#!/bin/bash

set SPARQLRANK = `pwd`

cd $BSBMROOT
mkdir tempStore
./generate -s ttl -pc 266 -fn tempStore/dataset100K
./generate -s ttl -pc 666 -fn tempStore/dataset250K
./generate -s ttl -pc 1332 -fn tempStore/dataset500K
./generate -s ttl -pc 2785 -fn tempStore/dataset1M
./generate -s ttl -pc 13925 -fn tempStore/dataset5M


cd $SPARQLRANK
mkdir Store

java -cp lib/tdb-0.8.11-SNAPSHOT.jar:lib/* tdb.tdbloader -loc Store/dataset100K $BSBMROOT/tempStore/dataset100K.ttl 

java -cp lib/tdb-0.8.11-SNAPSHOT.jar:lib/* tdb.tdbloader -loc Store/dataset250K $BSBMROOT/tempStore/dataset250K.ttl 

java -cp lib/tdb-0.8.11-SNAPSHOT.jar:lib/* tdb.tdbloader -loc Store/dataset500K $BSBMROOT/tempStore/dataset500K.ttl 

java -cp lib/tdb-0.8.11-SNAPSHOT.jar:lib/* tdb.tdbloader -loc Store/dataset1M $BSBMROOT/tempStore/dataset1M.ttl 

java -cp lib/tdb-0.8.11-SNAPSHOT.jar:lib/* tdb.tdbloader -loc Store/dataset5M $BSBMROOT/tempStore/dataset5M.ttl 

