package sparqlrank;

import java.util.ArrayList;

/**
 * Class containing the default execution parameters.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class DefaultTestParameters {
	
	// query default parameters - usually overridden
	public final static int DEFAULT_K = 10;
    public final static Boolean DEFAULT_MODIFIED_EXECUTION = true;
    public final static int DEFAULT_NUM_VARS = 3;
    
    // the dir containing the queries
    public final static String DEFAULT_QUERY_DIR = "queries/";
    
    // shall we use stdARQOptimization-> OpSequence and such
    public final static Boolean DEFAULT_ARQ_OPTIMIZATION = true;
    
    // which dataset size shall we use
    public final static String DEFAULT_DATASET = "250K";
	public static final String DEFAULT_DATASET_DIRECTORY = "Store";
    
    public final static String DEFAULT_QUERY_NAME = "phantom";
    public final static String DEFAULT_QUERY_FILENAME = "query1_1_0.txt";

    // use the BSBMDataModificator?
    public final static Boolean DEFAULT_CREATE_VALUES = false;
    
    // debugging flags
    public final static Boolean DEFAULT_DEBUG_MODEL = false;
    public final static  Boolean DEFAULT_DEBUG_VALUES = false;
    public final static Boolean DEFAULT_OUTPUT_RESULTS = false;
    
    // reporting flag
    public final static Boolean DEFAULT_REPORTING = false;
    public static final Boolean DEFAULT_TIMING = true;
      
    // default number of iterations on one query
    public final static int DEFAULT_REPEAT_QUERIES = 15;
    
    // each query is ran with different values of k
    // starting from 10^starting_power to 10^power
    public final static int DEFAULT_STARTING_POWER = 0;
    public final static int DEFAULT_POWER = 4;
    
    // which queries to run?
    public final static String DEFAULT_QUERY_MIX_FILENAME = "querymix.txt";
    
    // which datasets to run on?
    public final static String DEFAULT_POSSIBLE_DATASETS_FILENAME = "config.txt";

    public final static Boolean DEFAULT_CACHING = true;
    public final static Boolean DEFAULT_USESEQUENCEOPT = false;
    
    public  static ArrayList<String> DEFAULT_POSSIBLE_DATASETS = new ArrayList<String>();
    
    static{
        DEFAULT_POSSIBLE_DATASETS.add("100K");
        DEFAULT_POSSIBLE_DATASETS.add("250K");
        DEFAULT_POSSIBLE_DATASETS.add("500K");
        DEFAULT_POSSIBLE_DATASETS.add("750K");
        DEFAULT_POSSIBLE_DATASETS.add("1M");
        DEFAULT_POSSIBLE_DATASETS.add("5M");
        DEFAULT_POSSIBLE_DATASETS.add("10M");
    }


    public static final int HRJNSTAR_JOIN = 0;
    public static final int RANKSEQUENCE_JOIN = 1;
    public static final int HRJNRANDOM_JOIN = 2;
    public static final int COMBINED_JOIN = 3;

    public static final int DEFAULT_JOIN_TYPE = COMBINED_JOIN;
   
    public static final Boolean DEFAULT_TOPN_VALUE = true;
    
   public static final Boolean COMPARE_RANK_JOINS = true;

    
}
