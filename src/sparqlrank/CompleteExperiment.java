/*
 * @(#)CompleteExperiment.java   1.0   05/dic/2011
 *
 * Copyright 2011-2011 Politecnico di Milano. All Rights Reserved.
 *
 * This software is the proprietary information of Politecnico di Milano.
 * Use is subject to license terms.
 *
 * @(#) $Id$
 */
package sparqlrank;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import sparqlrank.datasetmodificator.BSBMDataModificator;
import sparqlrank.datasetmodificator.BSBMDataModificatorDistribution;

/**
 * Main class that performs a whole experiment.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */


public class CompleteExperiment {
   public static void main(final String[] args){

      // create an object TestParameters, that contains all the set parameters for each test
      // in case a parameter is not specified, TestParameter contains the default values
      final TestParameters param = new TestParameters(args);
      

      // first create the 100K, 250K, 500K, 1M, 5M datasets with the BSBM
      // put them into the Store directory each in one datasetXX dir
      
      final ArrayList<String> createdDatasets = new ArrayList<String>();
      final ArrayList<String> distributionDatasets = new ArrayList<String>();
      
      // modify them with BSBM Data Modifier
      //createdDatasets.add("100K");
      createdDatasets.add("250K");
      createdDatasets.add("500K");
      //createdDatasets.add("750K");
      createdDatasets.add("1M");
      createdDatasets.add("5M");
      param.setPossibleDatasets(createdDatasets);
      BSBMDataModificator.modifyPossibleDatasets(param);

      System.out.println("Modified the following datasets: " + createdDatasets);
      
      // modify just 5M with the distributions
      distributionDatasets.add("5M");
      param.setPossibleDatasets(distributionDatasets);
      BSBMDataModificatorDistribution.addDistributionToPossibleDatasets(param);
      
      System.out.println("Added syntectic values to the following datasets: " + distributionDatasets);


      TimingDriver.createMainExperimentsDirectories(param);
       
      final int numberOfRepetitions = 25;
     
      Experiment1 (param, distributionDatasets, numberOfRepetitions);
      Experiment2 (param, createdDatasets, numberOfRepetitions);

   }
   
   
   public static void Experiment1(final TestParameters param, final ArrayList<String> distributionDatasets, final int numberOfRepetitions){
       
      //
      // EXPERIMENT ON 1 QUERY (usually just on 5M)
      // (or any dataset that has been modified appropriately)
      //
      param.setPossibleDatasets(distributionDatasets);
      TimingDriver.createExperimentsDirectories(param, 1);
      param.setQueryDirName("queryExperiment2");
      param.setCompareRankJoins(true);
      param.setRepeatStandard(numberOfRepetitions);
      param.setRepeatModified(numberOfRepetitions);
      
      TimingDriver.setupTimingExperiments(param);
      ReportingDriver.setupReportingExperiments( param);
      writeQueryInfo(param);
         
   }
   
   public static void Experiment2(final TestParameters param, final ArrayList<String> createdDatasets, final int numberOfRepetitions){
      
      //
      // EXPERIMENT ON BENCHMARK - all datasets in createdDatasets
      //
      final File mainExperimentDir = param.getExperimentDirectory();
      param.setExperimentDirectory(mainExperimentDir);
      param.setPossibleDatasets(createdDatasets);
      TimingDriver.createExperimentsDirectories(param, 2);
      
      param.setQueryDirName("queries");
      param.setCompareRankJoins(false);
      param.setRepeatStandard(numberOfRepetitions);
      param.setRepeatModified(numberOfRepetitions);
      param.setReporting(false);
      param.setTiming(true);
      param.setCaching(true);
      
      TimingDriver.setupTimingExperiments(param);
      ReportingDriver.setupReportingExperiments( param);
      writeQueryInfo(param);
   }
   
   public static void writeQueryInfo(final TestParameters param){
      
      final File queryInfoDirectory = new File(param.getExperimentDirectory() + "/queries/");
      System.out.println("\t NEW Query Info Directory:" + queryInfoDirectory);
      queryInfoDirectory.mkdir();
      
      File queryDirectory;
      File[] listOfQueryFiles = {};
      
      
      try{
         queryDirectory = new File(param.getQueryDirName());
         listOfQueryFiles = queryDirectory.listFiles();
      }catch(final Exception e){
         // if the specified query directory does not exist 
         return;
      }
      

      Arrays.sort(listOfQueryFiles, new Comparator<File>(){
          @Override
         public int compare(final File f1, final File f2)
          {
              return f1.getName().compareTo(f2.getName());
          } });

      
      
      for (final File file: listOfQueryFiles){
          
         final String baseString = RankExecutionUtils.readQueryFromFile(file);
         
         try{
  
           final BufferedWriter bw = new BufferedWriter(new FileWriter(queryInfoDirectory +"/"+ file.getName()));
           bw.append(baseString);
           bw.flush();
           bw.close();
         } catch(final IOException e) { System.err.println("Could not create file query info."); System.exit(-1);}
         
         
      }
   }
}
