package sparqlrank;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.sparql.ARQConstants;
import com.hp.hpl.jena.sparql.util.Symbol;

/**
 * Class that contains all the relevant parameters in a test execution and the machinery to retrieve them from configuration files or command line arguments
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class TestParameters {

    
    public List<String> possibleDatasets = new ArrayList<String>();
    public List<Integer> possibleNumberOfVars = new ArrayList<Integer>();
    private final List<Integer> possibleQueries =  new ArrayList<Integer>();
    
    private int k = DefaultTestParameters.DEFAULT_K;
    private Boolean modifiedExecution = DefaultTestParameters.DEFAULT_MODIFIED_EXECUTION;
    private Boolean createValues = DefaultTestParameters.DEFAULT_CREATE_VALUES;
    
    private String queryDirName = DefaultTestParameters.DEFAULT_QUERY_DIR;    
//    private Boolean standardARQoptimization = DefaultTestParameters.DEFAULT_ARQ_OPTIMIZATION;
    private String dataset = DefaultTestParameters.DEFAULT_DATASET;
    private String datasetDirectory = DefaultTestParameters.DEFAULT_DATASET_DIRECTORY;
    private String queryName = DefaultTestParameters.DEFAULT_QUERY_NAME;
    private String queryFileName = DefaultTestParameters.DEFAULT_QUERY_FILENAME;
    private int numberOfOrderByVars = DefaultTestParameters.DEFAULT_NUM_VARS;
    private int planType = 0;
    
	 private Boolean debugModel = DefaultTestParameters.DEFAULT_DEBUG_MODEL;
    private Boolean debugValues = DefaultTestParameters.DEFAULT_DEBUG_VALUES;
    private Boolean outputresults = DefaultTestParameters.DEFAULT_OUTPUT_RESULTS;
    
    private Boolean compareRankJoins = DefaultTestParameters.COMPARE_RANK_JOINS;
    
//    private int repeat = DefaultTestParameters.DEFAULT_REPEAT_QUERIES;

    private int repeatModified = DefaultTestParameters.DEFAULT_REPEAT_QUERIES;
    private int repeatStandard = DefaultTestParameters.DEFAULT_REPEAT_QUERIES;
    
    private int power = DefaultTestParameters.DEFAULT_POWER;
    private int startingpower = DefaultTestParameters.DEFAULT_STARTING_POWER;
    //private int count = 0;
    
    private int countModified = 0;
    private  int countStandard = 0;
    
    private Boolean reporting = DefaultTestParameters.DEFAULT_REPORTING;
    private Boolean timing = DefaultTestParameters.DEFAULT_TIMING;
    
    private final String queryMixFilename = DefaultTestParameters.DEFAULT_QUERY_MIX_FILENAME;
    private final String possibleDatasetFilename = DefaultTestParameters.DEFAULT_POSSIBLE_DATASETS_FILENAME;
    
    private Boolean caching = DefaultTestParameters.DEFAULT_CACHING;
    //private Boolean useSequenceOptimization = DefaultTestParameters.DEFAULT_USESEQUENCEOPT;
    
    private File experimentDirectory = null;
    private File treeDirectory;
    
    private int joinType= DefaultTestParameters.DEFAULT_JOIN_TYPE;
    
    private Boolean optTopNSorting = DefaultTestParameters.DEFAULT_TOPN_VALUE;
    
    
    private final String[] args;
  
   
    final Symbol joinTypeSym = Symbol.create(ARQConstants.systemVarNS+"joinTypeSym") ;
    // set a symbol in the context to switch on/off caching
    final Symbol useCacheSym = Symbol.create(ARQConstants.systemVarNS+"useCache") ;

    
    public TestParameters(final String ...args){
    	
    	// read configuration files first
    	readConfig();
    	
    	// overwrite with parameters given on the command line
    	getParameters(args);
    	
    	//store parameters
    	this.args = args;
    	

		
    	if(caching == true){
			ARQ.getContext().set(useCacheSym, true);
    	}
    	else{
    		ARQ.getContext().set(useCacheSym, false);
    	}
    	
    	// set a symbol in the context to switch on/off sequence optimization
//    	final Symbol useSequenceOptimizationSym = Symbol.create(ARQConstants.systemVarNS+"useSequenceOptimization") ;
//    	
//    	if(useSequenceOptimization == true){
//			ARQ.getContext().set(useSequenceOptimizationSym, true);
//    	}
//    	else{
//    		ARQ.getContext().set(useSequenceOptimizationSym, false);
//    	}


      ARQ.getContext().set(joinTypeSym, joinType);
      

    	
    	
    }
    
    public void readConfig()
    {
    	// define the possible Datasets
    	// if there is not any file
    	if(possibleDatasetFilename == null){
			possibleDatasets = DefaultTestParameters.DEFAULT_POSSIBLE_DATASETS;
		}
		
    	// if the file is present
    	else{
    		System.out.println("Reading config file: " + possibleDatasetFilename);
    		final File file = new File(possibleDatasetFilename);

    		try {
    			final BufferedReader qmReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));

    			final StringBuffer data = new StringBuffer();
    			String line = null;
    			while((line=qmReader.readLine())!=null) {
    				
    				if(line.startsWith("config:")){
    				   System.out.println ("*** Configuration:" + line);
    					getParameters(line.split(":")[1].split(" "));
    					continue;
    				}
    				
    				if(!line.equals("")){
    					data.append(line);
    					data.append(" ");
    				}
    				

    			}
    				
    			final StringTokenizer st = new StringTokenizer(data.toString());
    			while(st.hasMoreTokens()) {
    				possibleDatasets.add(st.nextToken());
    			}
    			if (this.possibleDatasets.size()!=0)
    				this.dataset = this.possibleDatasets.get(0);
    			
    		} catch(final IOException e) {
    			System.out.println("Error processing dataset mix file: " + possibleDatasetFilename);
    		}
    		
			possibleNumberOfVars.add(2);
    	}
    	
    	if(queryMixFilename!=null){
    		//from BSBM testdriver
    		System.out.println("Reading query mix file: " + queryMixFilename);
    		final File file = new File(queryMixFilename);

    		try {
    			final BufferedReader qmReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));

    			final StringBuffer data = new StringBuffer();
    			String line = null;
    			while((line=qmReader.readLine())!=null) {
    				if(!line.equals("")){
    					data.append(line);
    					data.append(" ");
    				}
    			}
    				
    			final StringTokenizer st = new StringTokenizer(data.toString());
    			while(st.hasMoreTokens()) {
    				try{
    					possibleQueries.add(Integer.parseInt(st.nextToken()));
    				} catch(final Exception e){
    					break;
    				}
    			}

    		} catch(final IOException e) {
    			System.out.println("Error processing query mix file: " + queryMixFilename);
    		}
    	}
    	else{
			possibleNumberOfVars.add(2);
			possibleNumberOfVars.add(3);
    	}


    }
    
    
    
    private void getParameters(final String... args)
    {
	    int i = 0;
		while(i < args.length) {
			try {
				if(args[i].equals("-ds")) {
					dataset = args[i++ + 1];
				}
				else if(args[i].equals("-k")) {
					k = Integer.parseInt(args[i++ + 1]);
				}
				else if(args[i].equals("-create")) {
					createValues = true;
				}
				else if(args[i].equals("-stdexec")) {
					modifiedExecution = false;
				}
				else if(args[i].equals("-modexec")) {
					modifiedExecution = true;
				}
				else if(args[i].equals("-vars")) {
					setNumberOfOrderByVars(Integer.parseInt(args[i++ + 1]));
				}
				else if(args[i].equals("-dir")) {
					setQueryDirName(args[i++ + 1]);
				}
				else if(args[i].equals("-nodir")) {
					setQueryDirName(null);
				}
				else if(args[i].equals("-noopt")) {
				   setOptTopNSorting(false);
				}
				else if(args[i].equals("-opt")) {
					setOptTopNSorting(true);
				}
				else if(args[i].equals("-report")) {
					setReporting(true);
				}
				else if(args[i].equals("-noreport")) {
					setReporting(false);
				}
				else if(args[i].equals("-verbose")) {
					setOutputresults(true);
				}
				else if(args[i].equals("-q")) {
					setQueryFileName(args[i++ + 1]);
				}
				else if(args[i].equals("-caching")) {
					caching = true;
				}
				else if(args[i].equals("-nocaching")) {
               caching = false;
            }
//				else if(args[i].equals("-sequence")) {
//					useSequenceOptimization = true;
//				}
				else if(args[i].equals("-compareRJ")) {
                compareRankJoins = true;
             }
				else if(args[i].equals("-jointype")){
				   
				   final String join = args[i++ + 1];
				   
				   if(join.contains("hrjn") && join.contains("star")){
	               joinType = DefaultTestParameters.HRJNSTAR_JOIN;
				   }
				   else if(join.contains("hrjn") && join.contains("random")){
                  joinType = DefaultTestParameters.HRJNRANDOM_JOIN;
               } 
	            else if(join.contains("rank") || join.contains("sequence")){
	               joinType = DefaultTestParameters.RANKSEQUENCE_JOIN;
	            }
	            else if(join.contains("combined") ){
                  joinType = DefaultTestParameters.COMBINED_JOIN;
               }

	            else {
	               joinType = DefaultTestParameters.DEFAULT_JOIN_TYPE;
	            }
			
				}
				else if (args[i].equals("-repstd")){

                setRepeatStandard(Integer.parseInt(args[i++ + 1]));
				}
	         else if (args[i].equals("-repmod")){

	             setRepeatModified(Integer.parseInt(args[i++ + 1]));
	         }
				else {

    				printUsageInfos() ;
					//System.exit(-1);
				}
								
				i++;
				
				
							
			} catch(final Exception e) {
				printUsageInfos() ;
				System.err.println("Invalid arguments\n" + e);
				System.exit(-1);
			}
		}
    }
    
	/*
	 * print command line options
	 */
	public static void printUsageInfos() {
		final String output = "Usage: java sparqlrank.SPARQLRankDatasetGenerator <options>\n\n" +
						"Possible options are:\n" +
						"\t-ds <dataset size >\n" +
						"\t\twhere <dataset size >: 250K, 500K, 1M, 5M \n" +
						"\t\tdefault: '250K'\n" +
						"\t-k <limit size>\n" +
						"\t\tdefault: 10\n" +
						"\t-dir\tSpecify the directory of the queries\n" +
						"\t-create\tSwitch on creation of values in dataset which is by default off\n" +
						"\t-stdexec\tSwitch off modified execution\n" +
						"\t-modexec\tSwitch on modified execution, which should be the default\n" +
						//"\t-noopt\tSwitch off ARQ standard optimization, which is by default on\n" +
						"\t-verbose\tSwitch on the output of the results\n" +
						"\t-report\tSwitch on the reporting\n" +
						"\t-noreport\tSwitch off the reporting\n" +
                  "\t-caching\tSwitch on the caching\n" +
                  "\t-nocaching\tSwitch off the caching\n" +
                  "\t-jointype <join type>\n" +
                  "\t\twhere <join type>: hrjnstar, hrjnrandom, ranksequence \n" +
                  "\t-repstd <number of repetitions per standard execution>\n" +
                  "\t-repmod <number of repetitions per modified execution>\n" +
                  "\t-opt\tSwitch on Jena topk optimization, which should be the default\n" +
                  "\t-noopt\tSwitch off Jena topk optimization\n" +
                  "\t-compareRJ\tSwitch on RankJoin comparison\n" 
                 //"\t-sequence\tSwitch on the Sequence optimization\n" +
						//"\t-vars <number of ranking vars>\n" +
						//"\t\tThe number of variables used for ranking - 2 or 3\n" +
						//"\t\tdefault: 2\n"
						;
		System.out.print(output);
	}

	public void setModifiedExecution(final Boolean modifiedExecution) {
		this.modifiedExecution = modifiedExecution;
	}

	public Boolean getModifiedExecution() {
		return modifiedExecution;
	}

	public void setK(final int k) {
		this.k = k;
	}

	public int getK() {
		return k;
	}

	public void setCreateValues(final Boolean createValues) {
		this.createValues = createValues;
	}

	public Boolean getCreateValues() {
		return createValues;
	}

	public void setNumberOfOrderByVars(final int numberOfOrderByVars) {
		this.numberOfOrderByVars = numberOfOrderByVars;
	}

	public int getNumberOfOrderByVars() {
		return numberOfOrderByVars;
	}

	public void setQueryDirName(final String queryDirName) {
		this.queryDirName = queryDirName;
	}

	public String getQueryDirName() {
		return queryDirName;
	}

//	public void setStandardARQoptimization(final Boolean standardARQoptimization) {
//		this.standardARQoptimization = standardARQoptimization;
//	}
//
//	public Boolean getStandardARQoptimization() {
//		return standardARQoptimization;
//	}

	public void setDataset(final String dataset) {
		this.dataset = dataset;
	}

	public String getDataset() {
		return dataset;
	}

	public void setDebugModel(final Boolean debugModel) {
		this.debugModel = debugModel;
	}

	public Boolean getDebugModel() {
		return debugModel;
	}

	public void setDebugValues(final Boolean debugValues) {
		this.debugValues = debugValues;
	}

	public Boolean getDebugValues() {
		return debugValues;
	}

	public void setOutputresults(final Boolean outputresults) {
		this.outputresults = outputresults;
	}

	public Boolean getOutputresults() {
		return outputresults;
	}

//	public void setRepeat(final int repeat) {
//		this.repeat = repeat;
//	}
//
//	public int getRepeat() {
//		return repeat;
//	}

	public void setReporting(final Boolean reporting) {
		this.reporting = reporting;
	}

	public Boolean getReporting() {
		return reporting;
	}

	public void setPower(final int power) {
		this.power = power;
	}

	public int getPower() {
		return power;
	}
	
	public List<Integer> getPossibleQueries(){
		return possibleQueries;
	}
	
	 public String getQueryName() {
		return queryName;
	}

	public void setQueryName(final String queryName) {
		this.queryName = queryName;
		
		final String temp = queryName.substring(queryName.indexOf("_")+1, queryName.length());
		
		this.numberOfOrderByVars = Integer.parseInt(temp.substring(0, temp.indexOf("_")));
		this.planType = Integer.parseInt(temp.substring(temp.indexOf("_")+1, temp.length()));
	}

	public void setStartingpower(final int startingpower) {
		this.startingpower = startingpower;
	}

	public int getStartingpower() {
		return startingpower;
	}

   public  void setJoinType(final int joinType) {
      this.joinType=joinType;
      ARQ.getContext().set(joinTypeSym, joinType);
   }
   
	
	public void setExperimentDirectory(final File experimentDirectory) {
		this.experimentDirectory = experimentDirectory;
	}

	public File getExperimentDirectory() {
		return experimentDirectory;
	}

	public void setTreeDirectory(final File treeDirectory) {
		this.treeDirectory = treeDirectory;
	}

	public File getTreeDirectory() {
		return treeDirectory;
	}

//	public void setCount(final int count) {
//		this.count = count;
//	}
//
//	public int getCount() {
//		return count;
//	}

	public void setQueryFileName(final String queryFileName) {
		this.queryFileName = queryFileName;
	}

	public String getQueryFileName() {
		return queryFileName;
	}

	public void setCaching(final Boolean caching) {
		this.caching = caching;
	}

	public Boolean getCaching() {
		return caching;
	}

	public void setDatasetDirectory(final String datasetDirectory) {
		this.datasetDirectory = datasetDirectory;
	}

	public String getDatasetDirectory() {
		return datasetDirectory;
	}

   public  int getJoinType() {
      return joinType;
   }
   
   public TestParameters copy(){
      return new TestParameters(this.args);
   }

   public Boolean getTiming() {
      return this.timing;
   }

   public void setTiming(final Boolean timing) {
      this.timing = timing;
   }

   public int getRepeatModified() {
      return this.repeatModified;
   }

   public void setRepeatModified(final int repeatModified) {
      this.repeatModified = repeatModified;
   }

   public int getRepeatStandard() {
      return this.repeatStandard;
   }

   public void setRepeatStandard(final int repeatStandard) {
      this.repeatStandard = repeatStandard;
   }

   public int getCountModified() {
      return this.countModified;
   }

   public void setCountModified(final int countModified) {
      this.countModified = countModified;
   }

   public int getCountStandard() {
      return this.countStandard;
   }

   public int getCount(){
      if (this.getModifiedExecution()){
         return getCountModified();
         
      }
      else {
         return getCountStandard();
      }
   }
   
   public void setCountStandard(final int countStandard) {
      this.countStandard = countStandard;
   }

   public Boolean getOptTopNSorting() {
      return this.optTopNSorting;
   }

   public void setOptTopNSorting(final Boolean optTopNSorting) {
      this.optTopNSorting = optTopNSorting;
   }

   public int getPlanType() {
      return this.planType;
   }

   public void setPlanType(final int planType) {
      this.planType = planType;
   }

   public Boolean getCompareRankJoins() {
      return this.compareRankJoins;
   }
   
   public void setCompareRankJoins(final Boolean value) {
      compareRankJoins = value;
   }

   public void setPossibleDatasets(final List<String> value){
      this.possibleDatasets = value;
   }

}
