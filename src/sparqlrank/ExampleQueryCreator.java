package sparqlrank;


/**
 * Obsolete method to create query string programmatically - used in some obsolete sections of the BlackBoxTesting and TimingDriver
  * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

@Deprecated

public class ExampleQueryCreator {

    private static final String prefix = 
    	"prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> " +
    	"prefix xsd: <http://www.w3.org/2001/XMLSchema#>";
    
    
    private static final String multDefineScoreString2values = "((((?avgscore1+1)/2) * ((?normnumrevprod+1)/2)) AS ?score)";
    private static final String multDefineScoreString3values = "((((?avgscore1+1)/2) * ((?normnumrevprod+1)/2) * (((10000-xsd:double(xsd:string(?price)))/10000+1)/2)) AS ?score) ";
    
    private static final String sumDefineScoreString2values = "((?avgscore1 + ?normnumrevprod) AS ?score)";
    private static final String sumDefineScoreString3values = "((?avgscore1 + ?normnumrevprod + (10000-xsd:double(xsd:string(?price)))/10000) AS ?score) ";
    
    
    private static final String defaultScoreFunction = "sum";
    
    public static String createQueryString(int k, int values){
    	return createQueryString(k, values, defaultScoreFunction);
    }
    
	public static String createQueryString(int k, int values, String scorefunction){
        
    	// check parameters
    	String limitString;
    	
    	if (k<0) limitString = "";	
    	else if (k==0) return null;
    	else limitString = "LIMIT " + k;
    	
    	String defineScoreString2values = "", defineScoreString3values ="";

        if(scorefunction.equals("sum")){
        	defineScoreString2values = sumDefineScoreString2values;
        	defineScoreString3values = sumDefineScoreString3values;
        }
            	
        else if(scorefunction.equals("mult")){
        	
        	defineScoreString2values = multDefineScoreString2values;
        	defineScoreString3values = multDefineScoreString3values;
        }

	    
	    String standardOrderBy = "ORDER BY DESC(?score) " + limitString;

	    String selectClause2vars ="select ?offer ?product ?vendor ?producer" +
		"?normnumrevprod "+ "?avgscore1 ";
	    
	    String selectClause3vars ="select ?offer ?product ?vendor ?producer " +
		"?normnumrevprod "+ "?avgscore1 " + " ?price ";
	    
	    ///////////////////////////////
	    ///////////////////////////////
	    // Several Where clauses	//
	    ///////////////////////////////
	    ///////////////////////////////
		

	    String whereClauseSimple = "{" +
			"?product bsbm:producer  ?producer ." +
			"?producer bsbm:normnumreviews ?normnumrevprod ."+
			"?product bsbm:avgscore1 ?avgscore1 ." +
			"?offer bsbm:product ?product ." +
			"?offer bsbm:vendor ?vendor ."+
			"?offer bsbm:price ?price ." +
		"} ";
	    
	    String whereClauseJoins = "{" +
		"{?product bsbm:producer  ?producer ." +
		"?producer bsbm:normnumreviews ?normnumrevprod} ."+
		"{?product bsbm:avgscore1 ?avgscore1 .}" +
		"{?offer bsbm:product ?product} ." +
		"{?offer bsbm:vendor ?vendor} ."+
		"{?offer bsbm:price ?price }." +
		"} ";
	    
	    String whereClauseJoinsMED = "{" +
		"{?producer bsbm:normnumreviews ?normnumrevprod} ."+
		"{?product bsbm:avgscore1 ?avgscore1 .}" +
		"{?offer bsbm:product ?product ." +
		"?offer bsbm:vendor ?vendor ."+
		"?offer bsbm:price ?price ." +
		"?product bsbm:producer  ?producer} ." +
		"} ";
	    
	    String whereClauseJoinsBAD = "{" +
		"{?offer bsbm:product ?product} ." +
		"{?offer bsbm:vendor ?vendor} ."+
		"{?offer bsbm:price ?price }." +
		"{?product bsbm:producer  ?producer} ." +
		"{?producer bsbm:normnumreviews ?normnumrevprod} ."+
		"{?product bsbm:avgscore1 ?avgscore1 .}" +
		"} ";
	    
	    String whereClauseJoinsProducer13 = "{" +
			"{?product bsbm:producer  <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/dataFromProducer13/Producer13> ." +
			"<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/dataFromProducer13/Producer13> bsbm:normnumreviews ?normnumrevprod} ."+
			"{?product bsbm:avgscore1 ?avgscore1 .}" +
			"{?offer bsbm:product ?product} ." +
			"{?offer bsbm:vendor ?vendor} ."+
			"{?offer bsbm:price ?price }." +
		"} ";
	    
	    
	    // we need to put Producer13 to get unbound values for k = 200
	    // otherwise we will not get any unbound value in the top-k
	    String whereClauseOptional = "{" +
			"?offer bsbm:vendor ?vendor ."+
			"?offer bsbm:product ?product ." +
			"?offer bsbm:price ?price ." +
			"{?product bsbm:producer  ?producer ." +
			"?producer bsbm:normnumreviews ?normnumrevprod} ."+
			"OPTIONAL {?product bsbm:avgscore1 ?avgscore1} ." +
		"} ";

	    String whereClauseOptionalProducer13 = "{" +
		"?offer bsbm:vendor ?vendor ."+
		"?offer bsbm:product ?product ." +
		"?offer bsbm:price ?price ." +
		"{?product bsbm:producer  <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/dataFromProducer13/Producer13> ." +
		"<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/dataFromProducer13/Producer13> bsbm:normnumreviews ?normnumrevprod} ."+
		"OPTIONAL {?product bsbm:avgscore1 ?avgscore1} ." +
		"} ";
	    
	    
	    String targetString2valuesBAD = prefix+ selectClause2vars + defineScoreString2values + 
	    	whereClauseJoinsBAD + standardOrderBy;
	    
	    String targetString2valuesSimple = prefix+ selectClause2vars + defineScoreString2values + 
    	whereClauseSimple+ standardOrderBy;

	    String targetString2valuesMED = prefix + selectClause2vars + 
	    defineScoreString2values + 
	    whereClauseJoinsMED + standardOrderBy;
	    
	    String targetString3valuesMED = prefix + selectClause3vars + 
	    defineScoreString3values + 
	    whereClauseJoinsMED + standardOrderBy;
	    
	    
//	    String targetString1OPT = prefix +  selectClause2vars +
//		"((10000-xsd:float(xsd:string(?price)))/10000 AS ?normprice)" +
//		"((?normnumrevprod + ?avgscore1 + ?normprice) AS ?score)"+		
//		whereClauseOptional+ standardOrderBy;
	    
//	    String modifiedString3valuesOPT = prefix+ selectClause2vars + 
//		"((10000-xsd:float(xsd:string(?price)))/10000 AS ?normprice)" +
//		"((?normnumrevprod + ?avgscore1 + ?normprice) AS ?score)"+			
//			whereClauseOptional+ 
//			"ORDER BY DESC(?avgscore1) DESC(?normnumrevprod) DESC(?normprice) LIMIT " + k;
//	    
    
        
        String resultString = "";
        
        if(values == 2){
            resultString = prefix+ 
        	selectClause2vars + defineScoreString2values + 
        	whereClauseJoins+ standardOrderBy;
        }
        else if(values == 3){
            resultString = prefix+ 
        	selectClause3vars + defineScoreString3values + 
        	whereClauseJoins+ standardOrderBy;
        }
  
    
        System.out.println(resultString);    
        return resultString;
    }
	
}
