
package sparqlrank.rankcorrelation;

import java.util.ArrayList;
import java.util.List;

import com.hp.hpl.jena.sparql.algebra.OpVisitorBase;
import com.hp.hpl.jena.sparql.algebra.op.OpBGP;

/**
 * Class that visits a tree of Op objects and lists all Basic Graph Patterns.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class OpVisitorBGP extends OpVisitorBase{

		private List<OpBGP> listBGPs = new ArrayList<OpBGP>();
		
		public void visit(OpBGP op) {
			listBGPs.add(op);			
		}

		public List<OpBGP> getListBGPs() {return listBGPs;}
		
	}

