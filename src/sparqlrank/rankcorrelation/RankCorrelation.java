package sparqlrank.rankcorrelation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.SortCondition;
import com.hp.hpl.jena.query.Syntax;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.sparql.algebra.Algebra;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.OpVars;
import com.hp.hpl.jena.sparql.algebra.OpWalker;
import com.hp.hpl.jena.sparql.algebra.op.OpBGP;
import com.hp.hpl.jena.sparql.algebra.op.OpOrder;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.expr.E_Add;
import com.hp.hpl.jena.sparql.expr.E_Divide;
import com.hp.hpl.jena.sparql.expr.E_Function;
import com.hp.hpl.jena.sparql.expr.E_Subtract;
import com.hp.hpl.jena.sparql.expr.Expr;
import com.hp.hpl.jena.sparql.expr.ExprList;
import com.hp.hpl.jena.sparql.expr.ExprVar;
import com.hp.hpl.jena.sparql.expr.nodevalue.NodeValueInteger;

import org.apache.commons.math.stat.correlation.PearsonsCorrelation;

import sparqlrank.RankExecutionUtils;
import sparqlrank.TestParameters;
import sparqlrank.datasetmodificator.BSBMDataModificator;

/**
 * Class that computes the correlation between some hardcoded ranking predicates.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class RankCorrelation {
	
	private static ArrayList<SortCondition> rankingPredicates = new ArrayList<SortCondition>();
	
	private static LinkedHashMap<SortCondition,LinkedHashMap<Double,Integer>> countSameScores = new LinkedHashMap<SortCondition, LinkedHashMap<Double, Integer>>();

	public static void main(final String[] args){

			// create an object TestParameters, that contains all the set parameters for each test
			// in case a parameter is not specified, TestParameter contains the default values
			final TestParameters param = new TestParameters(args);
			
			param.setQueryDirName("queryExperiment2");
			
			createRankingPredicates();
			//String queryString = "prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/>  prefix xsd: <http://www.w3.org/2001/XMLSchema#> select * {    ?offer bsbm:price ?price } order by DESC(((10000-xsd:double(xsd:string(?price)))/10000))";
			//Query targetQuery = QueryFactory.create(queryString, Syntax.syntaxSPARQL_11);
			
						
			// create new experimentDirectory in the ARQ main dir
			final File rankCorrelationDir = new File("RankCorrelation");
			rankCorrelationDir.mkdir();
			System.out.println("\t NEW RANK CORRELATION DIR:" + rankCorrelationDir);
			

			// iterate on all possible datasets
			for (final String dataset: param.possibleDatasets){
				
				final File dsDirectory = new File(rankCorrelationDir + "/" + dataset + "/");
				System.out.println("\t NEW DATASET DIR:" + dsDirectory);
				dsDirectory.mkdir();
				param.setDataset(dataset);
				
				// a file for each dataset
				final Model model = BSBMDataModificator.createModel(param);
				


				File queryDirectory;
				File[] listOfQueryFiles = {};
						
				try{
					queryDirectory = new File(param.getQueryDirName());
					listOfQueryFiles = queryDirectory.listFiles();
				}catch(final Exception exception){
					// if the specified query directory does not exist 
					return;
				}
				    	
		
				Arrays.sort(listOfQueryFiles, new Comparator<File>(){
						    @Override
                     public int compare(final File f1, final File f2)
						    {
						        return f1.getName().compareTo(f2.getName());
						    } });

						
						
				for (final File file: listOfQueryFiles){
							
					String queryName = file.getName();
							
							
					if(queryName.startsWith("query"))
						queryName = queryName.split("query")[1];
					else continue;
							
					if(queryName.endsWith(".txt"))
						queryName = queryName.split(".txt")[0];
					else continue;					
							

							
				    param.setQueryName(queryName);
			    	System.out.println("--- Query name " + queryName);
						    	
					final String baseString = RankExecutionUtils.readQueryFromFile(file);
						   	
					 // is in the list but is null 
					if(baseString == null) 
						return;	
							
							
					// TODO: debug, sometimes one query gets repeated
								
					calculateRankCorrelationPerBGP( param, dsDirectory, model, baseString);
					}
				
						
				model.close();				

			
			}// end for dataset
			

	}
	
	


	// perform an experiment on one query, by adding increasing limits and repeating the execution
	public static void calculateRankCorrelationPerBGP(final TestParameters param, final File dsDirectory, final Model model, final String queryString){
		
		final BufferedWriter measurementFile = createMeasureWriter(dsDirectory, param);		
		
    	final Query targetQuery = QueryFactory.create(queryString, Syntax.syntaxSPARQL_11);
		final Op op = Algebra.compile(targetQuery) ;
				
		//get BGPs from each query
		final OpVisitorBGP v = new OpVisitorBGP();
		OpWalker.walk( op, v );
		final List<OpBGP> listBGPs = v.getListBGPs();
		
		final Var score = Var.alloc("score");
		
		

	
		// for each BGP of the query
		for (final OpBGP opBGP : listBGPs ){

			// results from the first input, basic ordering
			final List<Binding> originalResults = new ArrayList<Binding>();	
			
			//Op opExtend = OpExtend.extend(opBGP,score, new NodeValueInteger(1));
			
			final QueryIterator qIterOriginal = Algebra.exec(opBGP, model) ;
			for ( ; qIterOriginal.hasNext() ; )	 {   	
	        	originalResults.add(qIterOriginal.nextBinding()) ;
	        }
			
			final double[] originalPositions = new double[originalResults.size()];
			for (int i = 0; i<originalResults.size(); i++)
				originalPositions[i]=i;

			final LinkedHashMap<SortCondition, double[] > listOfPositions = new LinkedHashMap<SortCondition, double[]>();
			//HashMap<SortCondition, List<Binding> > listOfResults = new HashMap<SortCondition,List<Binding>>();
			
			listOfPositions.put(new SortCondition(new ExprVar("none"), 1), originalPositions);
			//listOfResults.put(new SortCondition(new ExprVar("none"), 1), originalResults);
			
			for (final SortCondition pred: rankingPredicates){
				
				Boolean skipPredicate = false;
								
				// can we apply predicate?
				final Set<Var> vars = OpVars.patternVars(opBGP) ;
				
				for (final Var v1 : pred.expression.getVarsMentioned()){
					if (!vars.contains(v1))
						skipPredicate = true;
				}
				
				if(skipPredicate)
					continue;
				
				
				// we can apply predicate 				
				final double[] positions = new double[originalResults.size()];
					
				final List<SortCondition> list = new ArrayList<SortCondition>();
				list.add(pred);
				final Op newOp = new OpOrder(opBGP, list);
				
//				Op op2 = OpExtend.extend(opBGP,score, pred.expression);
//				List<SortCondition> list = new ArrayList<SortCondition>();
//				list.add(new SortCondition(score,-1));
//				Op newOp = new OpOrder(op2, list);
				
				
				// execute BGP + order by predicate
		        final QueryIterator qIter = Algebra.exec(newOp, model) ;
				//System.out.println(newOp);
				
				try {
					measurementFile.append(newOp+"\n");
				} catch (final IOException e) { e.printStackTrace();}

				
//				List<Binding> sameScoreBindings = new ArrayList<Binding>();
//
//				// create the positions vector
//				Binding nextBinding = null;
//				Double scoreValue = null;
				
				//List<Binding> results = new ArrayList<Binding>();	
				int count = 0;
				countSameScores.remove(pred);
				final LinkedHashMap<Double, Integer> listOfSameScores = new LinkedHashMap<Double,Integer> ();
	
				
				for ( ; qIter.hasNext() ; )
		        		        	
		        {
		        	
					//take all binding with the same score
					final Binding binding = qIter.nextBinding();					
//					scoreValue = (Double) binding.get(score).getLiteralValue();					
//					if(scoreValue == null) break;
					
//					int sum = 0;
////					int size = 0;
//					
//					for( ; qIter.hasNext(); ){
//						Binding b = qIter.nextBinding();
//						Double scoreValue2 = (Double) b.get(score).getLiteralValue();					
//						if(scoreValue2 == null) break;
//						
//						
//						if (scoreValue.equals(scoreValue2)){
//							sum += calculatePosition (results, b, positions);
//							size++;
//						}
//							
//						else{
//							nextBinding = b;
//							break;
//						}
//							
//			
//					}
					


//					//positions[count] the count-h Binding
//					for(int i = 0; i<size; i++)
//					{
//						positions[sum/size] = count++;
//
//					}
				
					//results.add(binding);
					
					positions[calculatePosition (originalResults, binding, originalPositions)] = count++;
					

					final Double currentScore = pred.expression.eval(binding, null).getDouble();
					
					if(!listOfSameScores.containsKey(currentScore)){
						listOfSameScores.put(currentScore, 1);
					}
					else{
						final Integer countThisScore =  listOfSameScores.get(currentScore) + 1;
						listOfSameScores.put(currentScore, countThisScore);
					}
					
						


		        	
		        }
		        
				countSameScores.put(pred, listOfSameScores);
				
				//listOfResults.put(pred, results);
		        listOfPositions.put(pred, positions);
		        
			}
	        
			compareRankCorrelations(measurementFile, opBGP, listOfPositions);
			printCountSameScores(dsDirectory, param, listOfPositions);
			
		}
	    	
		try {
			measurementFile.flush();
			measurementFile.close();
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}






	private static void printCountSameScores( final File directory, final TestParameters param, final HashMap<SortCondition, double[]> listOfResults) {
		BufferedWriter measurementFile = null;
		
		final File newdirectory =  new File(directory + "/predicateDistribution/");
		newdirectory.mkdir();	
		
		for (final SortCondition s1: listOfResults.keySet()){
			try {
				final Expr e = s1.getExpression();
				String name = "";
				
				if (e.isVariable()){
					name = e.getVarName();
				}
				else if (e.isFunction()){
					if (e instanceof E_Add){
											
						for( final Var v: e.getVarsMentioned()){
							name += "&"+v.getVarName();
						}
					}
				}
				
				
				measurementFile =  new BufferedWriter(new FileWriter(newdirectory + "/" + name +"_"+ param.getQueryName() +".txt"));
				
				final Map<Double,Integer> list = countSameScores.get(s1);
				if(list!= null){
					measurementFile.append("\n ### "+ name+ " : "+ list.size()+ " different values \n ");
				
					for (final Double score: list.keySet()){
						measurementFile.append("( "+ score + ", "+ list.get(score)+") \n");
					}


				}
				
				measurementFile.flush(); measurementFile.close();
			} catch (final IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		

	}




	private static void compareRankCorrelations(final BufferedWriter measurementFile, final OpBGP opBGP,
			final HashMap<SortCondition, double[]> listOfResults) {

    	
    	final ArrayList<SortCondition> temp = new ArrayList<SortCondition>();
    	temp.addAll(listOfResults.keySet());
 	
    	for (final SortCondition s1: listOfResults.keySet()){
    		
    		for (final SortCondition s2: temp){
    			
    			if(s1.equals(s2)) continue;    	
    			
   			
    			//double distance = calculateSpearmanDistance(listOfResults.get(s1), listOfResults.get(s2));
    			
    			final double [] xArray = listOfResults.get(s1);
    			final double [] yArray = listOfResults.get(s2);
    			double distance = 0.0;
    			if (xArray != null && yArray!= null && xArray.length == yArray.length && xArray.length > 1) 
    				distance = new PearsonsCorrelation().correlation(xArray, yArray);

    			
    			if(distance!= 0.0){
    				
    				//System.out.println("Distance between "+s1 + " " + s2 + " - " + distance);
    				
    				try {
    					measurementFile.append("Distance between "+s1 + " " + s2 + " - " + distance +"\n");
    				} catch (final IOException e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				}

    			}
  
    			
    		}
			temp.remove(s1);
    		
    	}
    	
		
	}




	private static double calculateSpearmanDistance(final double[] array1,
			final double[] array2) {
		
		if (array2 == null)
			return 0;
		
		if(array1.length!= array2.length){
			System.out.println("Different length");
			return 0;
		}

		
		double footrule = 0;
		for (int i = 0; i < array1.length; i++){
			
		    footrule = footrule + Math.abs( array1[i] - array2[i]);
		}

		return footrule;
		
		
		
	}




	public static BufferedWriter createMeasureWriter(final File directory, final TestParameters param){
		try{
			System.out.println("\t\tNEW FILE:" + directory + "/" + "rankcorrelation"+"_"+ param.getQueryName() +".txt");			
			return new BufferedWriter(new FileWriter(directory + "/" + "rankcorrelation"+"_"+ param.getQueryName() +".txt"));
		} catch(final IOException e) { System.err.println("Could not create file rankcorrelation.txt."); System.exit(-1);}
		
		return null;
			
	}
	
	public static void writeMeasuresToFile(final BufferedWriter bw, final TestParameters param){
		
		
		try{
			bw.append("Modified :" + param.getModifiedExecution() + "&dataset:" + param.getDataset() 
						+ "&time:"+ RankExecutionUtils.getExecutionTime()  + "&ms\n");
			bw.flush();
		} catch(final IOException e) { e.printStackTrace(); System.exit(-1);}
		
	}
	

	public static void createRankingPredicates() {
		
		
		rankingPredicates.add(new SortCondition(new ExprVar("avgscore1"),-1));
		rankingPredicates.add(new SortCondition(new ExprVar("avgscore2"),-1));
		rankingPredicates.add(new SortCondition(new ExprVar("avgscore3"),-1));
		rankingPredicates.add(new SortCondition(new ExprVar("normnumrevprod"),-1));
		rankingPredicates.add(new SortCondition(new ExprVar("normnumrev"),-1));

	   rankingPredicates.add(new SortCondition(new ExprVar("p1"),-1));
	   rankingPredicates.add(new SortCondition(new ExprVar("p2"),-1));
		
		ExprList elist = new ExprList();
		elist.add(new ExprVar("price"));
		final Expr e = new E_Function("http://www.w3.org/2001/XMLSchema#string", elist );
		
		elist = new ExprList();
		elist.add(e);
		
			
		final Expr e2 = new E_Divide(new E_Subtract(new NodeValueInteger(10000), new E_Function("http://www.w3.org/2001/XMLSchema#double", elist )),new NodeValueInteger(10000));
		rankingPredicates.add(new SortCondition(e2,-1));
		
		final ArrayList<SortCondition> temp = (ArrayList<SortCondition>) rankingPredicates.clone();
		final ArrayList<SortCondition> temp2 = (ArrayList<SortCondition>) rankingPredicates.clone();
		
		// sum of two ranking predicates
		for( final SortCondition pred1: temp){
			for( final SortCondition pred2: temp2){
				if(pred1.equals(pred2))
					continue;
				final Expr ecomb = new E_Add(pred1.expression, pred2.expression);
				rankingPredicates.add(new SortCondition(ecomb,-1));
			}
			
			temp2.remove(pred1);
		}
		
		
		
	}
	
	
	private static int calculatePosition (final List<Binding> results, final Binding b, final double[] positions){
		if (results.contains(b)){
			
			// find all indexes of Binding b
			int i = 0;
			int sum = 0;
			int size = 0;
			
			for (final Binding b2: results){
				if (b2.equals(b)){
					sum+= i;
					size++;
				}
				i++;
					
			}
			
			if(size>=2)
				System.out.println("Duplicate");
						
			return sum;
	    	
		}
		
		return 0;
	}
	
	

}