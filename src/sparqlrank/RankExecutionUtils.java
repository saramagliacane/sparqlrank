package sparqlrank;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import arq.cmdline.ModTime;

import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.Syntax;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.sparql.ARQConstants;
import com.hp.hpl.jena.sparql.algebra.Algebra;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.optimize.Optimize;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.ResultSetStream;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.main.OpExecutorFactory;
import com.hp.hpl.jena.sparql.engine.main.QC;
import com.hp.hpl.jena.tdb.sys.TDBMaker;

import sparqlrank.algebra.RankRewriterFactory;
import sparqlrank.baseclasses.ExecutionModel;
import sparqlrank.datasetmodificator.BSBMDataModificator;
import sparqlrank.iterators.RankExecutorFactory;
import sparqlrank.iterators.SPARQLRankCache;

/**
 * Utility class for query execution, has two methods, one slower with timing - executeQuery -
 * one faster with results - executeQueryAndGetResults
  * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */


public class RankExecutionUtils {

        public static long time = 0;
        private final static Boolean DISABLE_CACHE = false;
        private final static Boolean PRINT_QUERY = false;
        private final static Boolean PRINT_TIME = false;

        
        public static Op executeQuery(final Model model, final String queryString){
        	return executeQuery(model, queryString, new TestParameters());
        }
        
        public static Op executeQuery(final Model model, final String queryString, final TestParameters param){
           return executeQuery(model, queryString, param, false);
              
        }
        
		public static Op executeQuery(final Model model, final String queryString, final TestParameters param, final Boolean ENABLE_LOGGING){
	        
         if(ENABLE_LOGGING)
            System.out.println("Query num: " + param.getQueryName() + 
                  " limit: "+ param.getK()+
                  " count: " + param.getCount() + 
                  " modified: " + param.getModifiedExecution() );
		   
		   
	      if(DISABLE_CACHE) TDBMaker.clearDatasetCache(); 
	        
	    	if (queryString == null) return null;
	    	
	      final Query targetQuery = QueryFactory.create(queryString, Syntax.syntaxSPARQL_11) ;
	      if (PRINT_QUERY) System.out.println(targetQuery);
	      	
			
//	      System.out.println("Before Compile: "); 
	      Op op = Algebra.compile(targetQuery) ;
//	      System.out.println("Compile: " +op); 

	      ExecutionModel.createExecutionModel();
		       
	    	//				Predicate predicateToSubstitute = new Predicate("avgscore1", -1);
	    	//				ExecutionModel.createExecutionModel(predicateToSubstitute, new Double(0.6));
	    	//	
	    	

			if(param.getModifiedExecution()){
			   
			   ARQ.getContext().setFalse(ARQ.optTopNSorting);
			   RankRewriterFactory.setSPARQLRankRewriter();				

			}
			else{
			   ARQ.getContext().set(ARQ.optTopNSorting, param.getOptTopNSorting());
				ARQ.getContext().set(ARQConstants.sysOptimizerFactory,Optimize.stdOptimizationFactory);
			}
	
	      	
	        //overrides only the execution of rank-aware ops
			RankExecutorFactory.setRankExecutor();
			try{
			   targetQueryExecution(model, op, param);
			}  catch (final java.lang.OutOfMemoryError e){
			   
            e.printStackTrace();
			   
			   if(param.getCaching() == true)
			      SPARQLRankCache.freeSPARQLRankCache();
			}
	      	
	      	
	      // to get the modified algebra tree, outside of the timing	
	    	op = Algebra.optimize(op);
		
         if(ENABLE_LOGGING)
            System.out.println(op);
	    	
         if (param.getModifiedExecution())
            ARQ.getContext().set(ARQ.optTopNSorting, param.getOptTopNSorting());
         
			return op;
	    }
	    
	  
	    private static void targetQueryExecution(final Model model, final Op op, final TestParameters param){ 
			// Timer in arq.cmdline;
			final ModTime modTime =     new ModTime() ;
	        modTime.startTimer() ;
	        
	        // Execute it.
	        final QueryIterator qIter = Algebra.exec(op, model) ;


            // Results
	        for ( ; qIter.hasNext() ; )
	        {
	            final Binding b = qIter.nextBinding() ;
	            if(param.getOutputresults()) 
	            	System.out.println("+++++Result:" + b) ;
	        }
	        
        	// from standard implementation
	        time = modTime.endTimer() ;
            
	        if (PRINT_TIME) 
            	System.out.println("Time: "+time+" ms") ;
            
	        qIter.close() ;
	    }
	    

	    
	    // fastest method that gets also results (but the two timings are not comparable)
	    public static ResultSet executeQueryAndGetResults(final Model model, final String queryString, final TestParameters param){
	    	
	    	final Query targetQuery = QueryFactory.create(queryString, Syntax.syntaxSPARQL_11);
	    	
	    	//ExecutionModel.createExecutionModel();
			
	        final OpExecutorFactory customExecutorFactory = new RankExecutorFactory() ;
			QC.setFactory(ARQ.getContext(), customExecutorFactory) ;
			
	    	
	    	if(param.getModifiedExecution()){
				
				RankRewriterFactory.setSPARQLRankRewriter();
				
				return resultsModifiedQuery(model, targetQuery, param.getDebugValues(), param.getDebugModel());
			
			}
			else{
				ARQ.getContext().set(ARQConstants.sysOptimizerFactory,Optimize.stdOptimizationFactory);
				final QueryExecution qExec = QueryExecutionFactory.create(targetQuery, model);
		  	    return qExec.execSelect();
			}
		
	    }
	    
	    // get Results in case of modified execution
	    private static ResultSet resultsModifiedQuery(final Model model, final Query targetQuery,  final Boolean debugValues, final Boolean debugModel){ 
	    	
	    	final Op op = Algebra.compile(targetQuery) ;
    	
	     	
	    	//standard
//      	System.out.println("Before Rewriter: " + op); 
//			RankRewriterFactory r = RankRewriterFactory.createRankRewriterFactory();
//	        Rewrite rw = r.create();
//	        op = rw.rewrite(op);
//	      	System.out.println("Rewriter: " + op); 
//	    	
	    	// resolved: doing twice the SPARQL-Rank optimization used to give several errors in execution
            //op = Algebra.optimize(op);
            
            
	    	if(PRINT_QUERY){
            	System.out.println("standard:"+op);
            }
	        
            
			final QueryIterator qIter = Algebra.exec(op, model) ;
 
			final ResultSetStream rStream = new ResultSetStream(targetQuery.getResultVars(), model, qIter) ;
						
	        return rStream ;

           
	    }
	    
	    public static long getExecutionTime(){
	    	return time;
	    }

	    // read a query from a file
	    public static String readQueryFromFile(final File queryFile){
    		// from BSBM Query code
    		try{
    			final BufferedReader queryReader = new BufferedReader(new InputStreamReader(new FileInputStream(queryFile)));
    			final StringBuffer sb = new StringBuffer();
    			
    			while(true) {
    				final String line = queryReader.readLine();
    				if(line==null)
    					break;
    				else {
    					sb.append(line);
    					sb.append("\n");
    				}
    			}
    			
    			queryReader.close();
    			
    			return sb.toString();
    			
    		} catch(final Exception e ){
    			return null;
    		}
    		
	    }
	    
	    public static void main(final String... args){
	    	
			// create an object TestParameters, that contains all the set parameters for each test
			// in case a parameter is not specified, TestParameter contains the default values
			final TestParameters param = new TestParameters(args);
			
			final Model model = BSBMDataModificator.createModel(param);
			
			final File queryFile = new File(param.getQueryDirName() +"/" + param.getQueryFileName());
			System.out.println("Filename: " + queryFile.getName());
			
			
			final String queryString = RankExecutionUtils.readQueryFromFile(queryFile);
			//System.out.println("Query:\n" +queryString);
	    	
			
			final int k = param.getK();

			String modString;
			
			if (k<=0) modString = queryString + " LIMIT 1";
			else modString =  queryString + " LIMIT " + k + "\n";
			
			param.setReporting(false);
	    	//param.setOutputresults(true);
			
			param.setModifiedExecution(false);	    	
			for( int count = 0; count <param.getRepeatStandard(); count++){
			   executeQuery(model,modString , param);
			}
			
         param.setModifiedExecution(true);        
         for( int count = 0; count <param.getRepeatModified(); count++){
            executeQuery(model,modString , param);
         }

	    }

}


