package sparqlrank.algebra;


import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.TransformCopy;
import com.hp.hpl.jena.sparql.algebra.op.OpJoin;

import sparqlrank.algebra.op.OpIncrementalRandom;
import sparqlrank.algebra.op.OpPipeJoin;
import sparqlrank.algebra.op.OpRank;
import sparqlrank.algebra.op.OpRankJoin;
import sparqlrank.baseclasses.PredicateSet;

/** OLD
 * Transformation which substitutes an OpSequence with a OpPipeJoin, the rank-aware
 * pipe implementation of a join. 
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

@Deprecated
public class TransformIntroducePipeJoin extends TransformCopy {

//   public Op transform(OpSequence op, List<Op> elts) 
//    {
//    	// the pipe join implementation up to now deals only with binary joins
//    	if (elts.size()!=2) return op;
//    	Op left = elts.get(0);
//    	Op right = elts.get(1);
//    	
//  		// it optimizes the execution when we have two OpRank ops
//   		if (!(left instanceof OpRank)||!(right instanceof OpRank))
//   			return super.transform(op, elts);
//    	
//    	return introducePipeJoin(op, left, right);
//    	
//    }
    
   private Op introducePipeJoin(final Op op, final Op left, final Op right) {
	   

		final OpRank op1 = (OpRank) left;
	   	final OpRank op2 = (OpRank) right;
	   	
	   	final PredicateSet leftpredicateset = new PredicateSet();
	   	leftpredicateset.addAll(op1.getSubPredicateset());
	   	leftpredicateset.add( op1.getPredicate());
	   	
	   	final PredicateSet rightpredicateset = new PredicateSet();
	   	rightpredicateset.addAll(op2.getSubPredicateset());
	   	rightpredicateset.add( op2.getPredicate());
	   	
	   	final Op op3 = new OpIncrementalRandom(op2.getSubOp(), op2.getPredicate(), 
	   			op2.getScoreFunction(), op2.getSubPredicateset(), op2.getVars());
	   	
	   	final OpPipeJoin subOp = new OpPipeJoin(op1, op3, op1.getScoreFunction(), leftpredicateset, rightpredicateset);
	   	
	   	return subOp;
   }

   @Override
   public Op transform(final OpJoin op, final Op left, final Op right) {
	   if (! (op instanceof OpRankJoin)) return super.transform(op, left, right);
	   
 		// it optimizes the execution when we have two OpRank ops
  		if (!(left instanceof OpRank)||!(right instanceof OpRank))
  			return super.transform(op, left, right);
  		
	  	return introducePipeJoin(op, left, right);
    }
}
