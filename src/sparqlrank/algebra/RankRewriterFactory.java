package sparqlrank.algebra;


import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.sparql.ARQConstants;
import com.hp.hpl.jena.sparql.algebra.optimize.Optimize;
import com.hp.hpl.jena.sparql.algebra.optimize.Rewrite;
import com.hp.hpl.jena.sparql.algebra.optimize.Optimize.RewriterFactory;
import com.hp.hpl.jena.sparql.util.Context;
import com.hp.hpl.jena.sparql.util.Symbol;


/**
 * Class implementing the Factory pattern to create a SPARQLRank RankRewriter
 * Includes some code to ensure that the Rewriter is a singleton
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class RankRewriterFactory implements RewriterFactory{

	// symbols used to store references to the singleton objects
	private final static Symbol rankRewriterFactorySym = Symbol.create(ARQConstants.systemVarNS+"RankRewriterFactory") ;
	private final static Symbol rankRewriterSym = Symbol.create(ARQConstants.systemVarNS+"RankRewriter") ;
	public final static Symbol standardRewriterSym = Symbol.create(ARQConstants.systemVarNS+"StdRewriter") ;

	public static void setSPARQLRankRewriter(){
		setSPARQLRankRewriter(ARQ.getContext());
	}
	
	
	/*
	 * Set the default Rewriter to the SPARQLRank version
	 */

	public static void setSPARQLRankRewriter(Context context){
		
		// create a RankRewriter
		RankRewriterFactory r = RankRewriterFactory.createRankRewriterFactory(context);	
	
		// store the reference to the standard Rewriter 
		context.set(standardRewriterSym, Optimize.stdOptimizationFactory) ;
		
		// set the RankRewriter as the default Rewriter
		context.set(ARQConstants.sysOptimizerFactory, r) ;
	}
	
	/*
	 * Ensure the RankRewriterFactory is a singleton, creating it only in case it isn't present in the context
	 */
	
	public static RankRewriterFactory createRankRewriterFactory( Context context ){
		
		RankRewriterFactory rankRewriterFactory = (RankRewriterFactory) context.get(rankRewriterFactorySym) ;
		
		if(rankRewriterFactory==null){
			rankRewriterFactory = new RankRewriterFactory();
			context.set(rankRewriterFactorySym, rankRewriterFactory) ;
		}
	
		return rankRewriterFactory;
	}

	public Rewrite create() {
		return create(ARQ.getContext());
	}
	
	@Override
	public Rewrite create(Context context) {
		
		RankRewriter rankRewriter = (RankRewriter) context.get(rankRewriterSym) ;
		
		if(rankRewriter==null){
			rankRewriter = new RankRewriter();
			context.set(rankRewriterSym, rankRewriter) ;
		}
		return rankRewriter;

	}
	

	
	public static RewriterFactory getStandardRewriterFactory(Context context){

		RewriterFactory f =  (RewriterFactory) context.get(standardRewriterSym);
		if (f == null){
			f =  Optimize.stdOptimizationFactory;
			context.set(standardRewriterSym, f) ;
		}
		
		return f;
			
		
	}
	
	/* Set the standard ARQ Rewriter as the default Rewriter */
	public static void setStandardRewriterFactory(){
		setStandardRewriterFactory(ARQ.getContext());
	}

	
	public static void setStandardRewriterFactory(Context context){
		
		RewriterFactory f1 = (RewriterFactory) context.get(ARQConstants.sysOptimizerFactory);
		RewriterFactory f2 = (RewriterFactory) context.get(standardRewriterSym);
		
		// set only if it was not previously set
		if (!f1.equals(f2)) 
			context.set(standardRewriterSym, f2) ;
	
		
	}
	
	
}
