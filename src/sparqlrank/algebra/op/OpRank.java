package sparqlrank.algebra.op;

import java.util.HashSet;
import java.util.Set;
import sparqlrank.baseclasses.Predicate;
import sparqlrank.baseclasses.PredicateSet;
import sparqlrank.baseclasses.ScoreFunction;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.OpVars;
import com.hp.hpl.jena.sparql.algebra.op.OpBGP;
import com.hp.hpl.jena.sparql.algebra.op.OpOrder;
import com.hp.hpl.jena.sparql.core.BasicPattern;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.util.NodeIsomorphismMap;

/** This class implements the Rank operator of the rank-aware extension of the 
 * SPARQL algebra, called SPARQL-Rank algebra.
 * The aim of the Rank operator is to represent an ordering operator, which performs
 * an ordering on a single predicate function.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class OpRank extends OpOrder
{ 
  // the predicate field is the actual parameter of the Rank operator
  // the scorefunction represents the global ordering function that combines the predicates
  // the subpredicateset represents the set of predicates of the child operators
  // and it's needed in the physical implementation (QueryIterRank)
  private Predicate predicate;
  private ScoreFunction scorefunction;
  private PredicateSet subpredicateset;
  private Set<Var> vars = new HashSet<Var>();
  private BasicPattern subpattern = null;
 

  
  public OpRank(Op subOp, Predicate p, ScoreFunction s, PredicateSet subpredicateset, Set<Var> vars)
  { 
	super(subOp, p.getListOfSortConditions());
	
    this.predicate = p;
    this.scorefunction = s;
    this.subpredicateset = subpredicateset;
    this.vars.addAll(vars);
    
    if(subOp instanceof OpBGP){
    	subpattern = ((OpBGP) subOp).getPattern();
    }
    //debug();
   }
  
  
  
  public Predicate getPredicate() { return predicate;}
  public ScoreFunction getScoreFunction() { return scorefunction;}
  public PredicateSet getSubPredicateset() { return subpredicateset;}
  
  public BasicPattern getSubPattern() { return subpattern;} 


@Override
public boolean equalTo(Op other, NodeIsomorphismMap labelMap) {
   if ( ! (other instanceof OpRank) ) return false ;
	    OpRank opRank = (OpRank)other ;
	    if (!opRank.predicate.equals(this.predicate)) return false;
	    return getSubOp().equalTo(opRank.getSubOp(), labelMap) ;
}
@Override
public int hashCode() {
	return predicate.hashCode() ^ getSubOp().hashCode() ;
}

@Override
public Op copy(Op subOp) {
	return new OpRank(subOp, predicate, scorefunction, subpredicateset, vars) ;
}

@Override
public String getName() { 	return "rank"; }


public void debug(){
	System.out.println ("OpRank - " +predicate.toString()+" " + scorefunction.toString()+ " sub:"+ subpredicateset);
	System.out.println( "vars:" + OpVars.patternVars(getSubOp()));
	
}

public void close(){
	predicate = null;
	scorefunction = null;
	subpredicateset = null;
}

public void setVars(Set<Var> vars) {
	this.vars = vars;
}

public Set<Var> getVars() {
	return vars;
}


}

////////////////////////////////////////////////
// Methods to calculate dynamically the variables of the sub operators.
//Set<Var> leftVars = OpVars.patternVars(left) ;
//       VarFinder vf = new VarFinder(right) ;
//Set<Var> optRight = vf.getOpt() ;
//Set<Var> fixedRight = vf.getFixed() ;
//Set<Var> filterVarsRight = vf.getFilter() ; 
////////////////////////////////////////////////
