/*
 * @(#)OpRankJoinRandom.java   1.0   04/dic/2011
 *
 * Copyright 2011-2011 Politecnico di Milano. All Rights Reserved.
 *
 * This software is the proprietary information of Politecnico di Milano.
 * Use is subject to license terms.
 *
 * @(#) $Id$
 */
package sparqlrank.algebra.op;

import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.op.Op2;
import com.hp.hpl.jena.sparql.util.NodeIsomorphismMap;

import sparqlrank.baseclasses.PredicateSet;
import sparqlrank.baseclasses.ScoreFunction;

/** This class implements an alternative operator for the RankJoin in the rank-aware extension of the 
 * SPARQL algebra, called SPARQL-Rank algebra.
 * This operator can be used only in case we have random access (a triple pattern) in both child inputs.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */


public class OpRankJoinRandom extends OpBaseRankJoin {

   public OpRankJoinRandom(final Op newLeft, final Op newRight, final ScoreFunction scorefunction,
         final PredicateSet leftpredicateset, final PredicateSet rightpredicateset) {
      super(newLeft,  newRight,  scorefunction,  leftpredicateset,  rightpredicateset);
   }


   @Override
    public String getName() {    
       
       return "rankjoinrandom ( [" + leftpredicateset  + "] , ["+ rightpredicateset + "])"; 
   }

        
    @Override
    public Op copy(final Op newLeft, final Op newRight)
    { return new OpRankJoinRandom(newLeft, newRight, scorefunction, leftpredicateset, rightpredicateset) ; }
       
    @Override
    public boolean equalTo(final Op op2, final NodeIsomorphismMap labelMap)
    {
           if ( ! ( op2 instanceof OpRankJoinRandom) ) return false ;
           return super.sameArgumentsAs((Op2)op2, labelMap) ;
    }
       
    @Override
   public void debug(){
         System.out.println ("OpRankJOINrandom - " + scorefunction.toString()+ " "+ leftpredicateset  + rightpredicateset);
    }
}
