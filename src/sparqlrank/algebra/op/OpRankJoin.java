package sparqlrank.algebra.op;

import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.op.Op2;
import com.hp.hpl.jena.sparql.util.NodeIsomorphismMap;

import sparqlrank.baseclasses.PredicateSet;
import sparqlrank.baseclasses.ScoreFunction;

/** This class implements the RankJoin operator of the rank-aware extension of the 
 * SPARQL algebra, called SPARQL-Rank algebra.
 * The aim of the RankJoin operator is to enhance the Join operator with rank-awareness
 * in order to exploit the partial orderings of its operands
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */


//IMPORTANT: need to change OpJoin's constructor in ARQ from private to protected


public class OpRankJoin extends OpBaseRankJoin {
	
	 
	 public OpRankJoin(final Op newLeft, final Op newRight, final ScoreFunction scorefunction,
         final PredicateSet leftpredicateset, final PredicateSet rightpredicateset) {
      super(newLeft,  newRight,  scorefunction,  leftpredicateset,  rightpredicateset);
   }


   @Override
	 public String getName() { 	
		 
		 return "rankjoin ( [" + leftpredicateset  + "] , ["+ rightpredicateset + "])"; 
	}

        
	 @Override
	 public Op copy(final Op newLeft, final Op newRight)
	 { return new OpRankJoin(newLeft, newRight, scorefunction, leftpredicateset, rightpredicateset) ; }
	    
	 @Override
	 public boolean equalTo(final Op op2, final NodeIsomorphismMap labelMap)
	 {
	        if ( ! ( op2 instanceof OpRankJoin) ) return false ;
	        return super.sameArgumentsAs((Op2)op2, labelMap) ;
	 }
	    
	 public void debug(){
	    	System.out.println ("OpRankJOIN - " + scorefunction.toString()+ " "+ leftpredicateset  + rightpredicateset);
	 }


}
