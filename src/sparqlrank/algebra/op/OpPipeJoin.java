package sparqlrank.algebra.op;

import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.op.Op2;
import com.hp.hpl.jena.sparql.algebra.op.OpJoin;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.util.NodeIsomorphismMap;

import sparqlrank.baseclasses.PredicateSet;
import sparqlrank.baseclasses.ScoreFunction;

/** This class implements the PipeJoin operator, which is a rank-aware join operator 
 * that can take advantage of the sorted random access in its right child.
 * Thus it should be more performing than the standard OpRankJoin, but not always
 * applicable.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

// IMPORTANT: need to change OpJoin's constructor in ARQ from private to protected

@Deprecated
public class OpPipeJoin extends OpJoin{

	 private ScoreFunction scorefunction;
	 private PredicateSet leftpredicateset;
	 private PredicateSet rightpredicateset;
	 
	  //TODO: what about a set of joinvariables?
	  private Var joinVariable;
	
	 public ScoreFunction getScoreFunction() { return scorefunction;}
	 public PredicateSet getLeftPredicateset() { return leftpredicateset;}
	 public PredicateSet getRightPredicateset() { return rightpredicateset;}
	 
	public OpPipeJoin(final Op left, final Op right, final ScoreFunction s, final PredicateSet leftpredicateset, final PredicateSet rightpredicateset) { 
		 //need to change OpJoin's constructor to protected
		 super(left, right);
		 this.scorefunction = s;
		 this.leftpredicateset = leftpredicateset;
		 this.rightpredicateset = rightpredicateset;
	 }
	

	 @Override
   public String getName() { 	
		 
		 return "pipejoin ( [" + leftpredicateset  + "] , ["+ rightpredicateset + "])"; 
	}

        

	 @Override
   public Op copy(final Op newLeft, final Op newRight)
	 { return new OpPipeJoin(newLeft, newRight, scorefunction, leftpredicateset, rightpredicateset) ; }
	    

	 @Override
   public boolean equalTo(final Op op2, final NodeIsomorphismMap labelMap)
	 {
	        if ( ! ( op2 instanceof OpPipeJoin) ) return false ;
	        return super.sameArgumentsAs((Op2)op2, labelMap) ;
	 }
	    
	 public void debug(){
	    	System.out.println ("OpPipeJOIN - " + scorefunction.toString()+ " "+ leftpredicateset  + rightpredicateset);
	 }

	 public void close(){
		scorefunction = null;
		leftpredicateset = null;
		rightpredicateset = null;
	 }
}
