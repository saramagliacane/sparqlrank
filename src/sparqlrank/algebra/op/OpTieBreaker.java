/*
 * @(#)OpTieBreaker.java   1.0   16/dic/2011
 *
 * Copyright 2011-2011 Politecnico di Milano. All Rights Reserved.
 *
 * This software is the proprietary information of Politecnico di Milano.
 * Use is subject to license terms.
 *
 * @(#) $Id$
 */
package sparqlrank.algebra.op;

import java.util.List;

import com.hp.hpl.jena.query.SortCondition;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.op.OpOrder;
import com.hp.hpl.jena.sparql.util.NodeIsomorphismMap;

/** 
 * Operator that handles additional order
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */


public class OpTieBreaker extends OpOrder{

   
   
   public OpTieBreaker(final Op subOp, final List<SortCondition> conditions)
   { 
       super(subOp, conditions );
   }
   
   @Override
   public boolean equalTo(final Op other, final NodeIsomorphismMap labelMap)
   {
       if ( ! (other instanceof OpTieBreaker) ) return false ;
       final OpTieBreaker opOrder = (OpTieBreaker)other ;
       
       if ( ! opOrder.getConditions().equals(this.getConditions()) )
           return false ;
       
       //
       return getSubOp().equalTo(opOrder.getSubOp(), labelMap) ;
   }
   
   @Override
   public String getName() {  return "tiebreaker"; }
   
   
   
   @Override
   public Op copy(final Op subOp) {
   
      // need it in order to use standard optimizer
      return new OpTieBreaker(subOp, this.getConditions()) ;
   }
   
   
}
