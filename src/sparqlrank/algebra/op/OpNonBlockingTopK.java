package sparqlrank.algebra.op;

import sparqlrank.baseclasses.PredicateSet;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.op.OpDistinct;
import com.hp.hpl.jena.sparql.util.NodeIsomorphismMap;

/** Class used in the NonBlockingTopK ExecutionModel. Not really used.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */


public class OpNonBlockingTopK extends OpDistinct{
	
	private PredicateSet predicates;
	
    private OpNonBlockingTopK(Op subOp, PredicateSet predicates)
    { 
    	super(subOp) ; 
    	this.predicates = predicates;
    }
    
    public static Op create(Op op, PredicateSet predicates)
    {
        if ( op instanceof OpNonBlockingTopK)
            return op ;
        return new OpNonBlockingTopK(op, predicates) ;
    }
    
    @Override
    public String getName()                 { return "nonBlockingTopK" ; }
    
    public PredicateSet getPredicates() {return predicates;}

    @Override
    public Op copy(Op subOp)                { return new OpNonBlockingTopK(subOp, predicates) ; }
	
    @Override
    public boolean equalTo(Op other, NodeIsomorphismMap labelMap)
    {
        if ( ! (other instanceof OpNonBlockingTopK) ) return false ;
        return getSubOp().equalTo(((OpNonBlockingTopK)other).getSubOp(), labelMap) ;
    }

	public void close() {
		predicates = null;
		
	}
    
}
