/*
 * @(#)OpBaseRankJoin.java   1.0   04/dic/2011
 *
 * Copyright 2011-2011 Politecnico di Milano. All Rights Reserved.
 *
 * This software is the proprietary information of Politecnico di Milano.
 * Use is subject to license terms.
 *
 * @(#) $Id$
 */
package sparqlrank.algebra.op;


import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.op.OpJoin;
import com.hp.hpl.jena.sparql.util.NodeIsomorphismMap;

import sparqlrank.baseclasses.PredicateSet;
import sparqlrank.baseclasses.ScoreFunction;

   /** This abstract class represents the RankJoin operator of the rank-aware extension of the 
    * SPARQL algebra, called SPARQL-Rank algebra.
    * The aim of the RankJoin operator is to enhance the Join operator with rank-awareness
    * in order to exploit the partial orderings of its operands
    * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
    */


   //IMPORTANT: need to change OpJoin's constructor in ARQ from private to protected


   public abstract class OpBaseRankJoin extends OpJoin {
      
       // the scorefunction represents the global ordering function that combines the predicates
       // the leftpredicateset represents the set of predicates of the left child operator
       // while the rightpredicateset represents the set of the right child operator
       // TODO: perform here the intersections, etc. used in QueryIterRankJoin
      
       protected ScoreFunction scorefunction;
       protected PredicateSet leftpredicateset;
       protected PredicateSet rightpredicateset;
       
       public ScoreFunction getScoreFunction() { return scorefunction;}
       public PredicateSet getLeftPredicateset() { return leftpredicateset;}
       public PredicateSet getRightPredicateset() { return rightpredicateset;}    
       
      
       public OpBaseRankJoin(final Op left, final Op right, final ScoreFunction s, final PredicateSet leftpredicateset, final PredicateSet rightpredicateset) { 
          //need to change OpJoin's constructor to protected
          super(left, right);
          this.scorefunction = s;
          this.leftpredicateset = leftpredicateset;
          this.rightpredicateset = rightpredicateset;
          //debug();
       }
       

       public void close(){
         scorefunction = null;
         leftpredicateset = null;
         rightpredicateset = null;
       }
       
       

      @Override
      public abstract String getName();

      @Override
      public abstract Op copy(final Op newLeft, final Op newRight);
       

      @Override
      public abstract boolean equalTo(final Op op2, final NodeIsomorphismMap labelMap);
          
      public abstract void debug();
       
   }

