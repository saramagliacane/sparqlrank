package sparqlrank.algebra.op;

import java.util.HashSet;
import java.util.Set;

import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.OpVars;
import com.hp.hpl.jena.sparql.algebra.op.OpOrder;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.util.NodeIsomorphismMap;

import sparqlrank.baseclasses.Predicate;
import sparqlrank.baseclasses.PredicateSet;
import sparqlrank.baseclasses.ScoreFunction;

/** Class used for the PipeJoin optimization.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

@Deprecated

public class OpIncrementalRandom extends OpOrder {

	
	  private Predicate predicate;
	  private ScoreFunction scorefunction;
	  private PredicateSet subpredicateset;
	  private Set<Var> vars = new HashSet<Var>();
	  
	  //TODO: what about a set of joinvariables?
	  private Var joinVariable;

	  
	  public OpIncrementalRandom(final Op subOp, final Predicate p, final ScoreFunction s, final PredicateSet subpredicateset, final Set<Var> vars)
	  { 
		super(subOp, p.getListOfSortConditions());
		
	    this.predicate = p;
	    this.scorefunction = s;
	    this.subpredicateset = subpredicateset;
	    this.vars.addAll(vars);
	    
	    //debug();
	   }
	  
	  
	  
	  public Predicate getPredicate() { return predicate;}
	  public ScoreFunction getScoreFunction() { return scorefunction;}
	  public PredicateSet getSubPredicateset() { return subpredicateset;}


	@Override
	public boolean equalTo(final Op other, final NodeIsomorphismMap labelMap) {
	   if ( ! (other instanceof OpIncrementalRandom) ) return false ;
	   final OpIncrementalRandom opRank = (OpIncrementalRandom) other ;
	   if (!opRank.predicate.equals(this.predicate)) return false;
	   return getSubOp().equalTo(opRank.getSubOp(), labelMap) ;
	}
	@Override
	public int hashCode() {
		return predicate.hashCode() ^ getSubOp().hashCode() ;
	}

	@Override
	public Op copy(final Op subOp) {
		return new OpIncrementalRandom(subOp, predicate, scorefunction, subpredicateset, vars) ;
	}

	@Override
	public String getName() { 	return "incrementalRandom"; }


	public void debug(){
		System.out.println ("OpIncrementalRandom - " +predicate.toString()+" " + scorefunction.toString()+ " sub:"+ subpredicateset);
		System.out.println( "vars:" + OpVars.patternVars(getSubOp()));
		
	}

	public void close(){
		predicate = null;
		scorefunction = null;
		subpredicateset = null;
	}

	public void setVars(final Set<Var> vars) {
		this.vars = vars;
	}

	public Set<Var> getVars() {
		return vars;
	}
}
