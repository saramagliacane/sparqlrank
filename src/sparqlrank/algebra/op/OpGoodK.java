package sparqlrank.algebra.op;


import sparqlrank.baseclasses.Predicate;
import sparqlrank.baseclasses.ScoreFunction;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.op.OpDistinct;
import com.hp.hpl.jena.sparql.util.NodeIsomorphismMap;

/** Class used in the GoodK ExecutionModel. Not really used.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */



public class OpGoodK extends OpDistinct{
	
	private Double defaultValue;
	private Predicate predicate;
	private ScoreFunction scoreFunction;
	
	private OpGoodK(Op subOp, Predicate predicate, Double value, ScoreFunction scoreFunction){
		super(subOp);
		this.predicate = predicate;
		defaultValue = value;
		this.scoreFunction = scoreFunction;
		
		
	}
	
	public static OpGoodK create(Op subOp, Predicate predicate, Double value, ScoreFunction scoreFunction) {
		return new OpGoodK(subOp, predicate, value, scoreFunction);
	}
	
	public Predicate getPredicate() { return predicate;	}
	public Double getDefaultValue() { return defaultValue;	}
	public ScoreFunction getScoreFunction() { return scoreFunction;	}
	
	@Override
    public String getName() { return "goodK ["+ predicate.getName() + " " + defaultValue + "] "; }


	public void close() {
		predicate = null;		
	}
	
    @Override
    public Op copy(Op subOp)     { return new OpGoodK(subOp, predicate, defaultValue, scoreFunction) ; }
	
    @Override
    public boolean equalTo(Op other, NodeIsomorphismMap labelMap)
    {
        if ( ! (other instanceof OpGoodK) ) return false ;
        if (! ((OpGoodK) other).predicate.equals(predicate)) return false;
        if (! (((OpGoodK) other).defaultValue == defaultValue)) return false;
        return getSubOp().equalTo(((OpGoodK)other).getSubOp(), labelMap) ;
    }





}
