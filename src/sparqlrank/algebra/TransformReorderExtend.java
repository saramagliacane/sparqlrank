package sparqlrank.algebra;

import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.TransformCopy;
import com.hp.hpl.jena.sparql.algebra.op.OpExtend;
import com.hp.hpl.jena.sparql.core.VarExprList;

/**
 * A Transform which reorders OpExtend. NOT USED.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class TransformReorderExtend extends TransformCopy {
	
    public Op transform(OpExtend opExtend1, Op subOp) 
    {
    	if (!(subOp instanceof OpExtend)) return opExtend1;	
    	
    	OpExtend opExtend2 = (OpExtend) subOp;
    	Op subOp2 = opExtend2.getSubOp();
    	VarExprList exprs1 = opExtend1.getVarExprList();
    	VarExprList exprs2 = opExtend2.getVarExprList();
    	OpExtend subExtend = OpExtend.extendDirect(subOp2, exprs1);
    	OpExtend result = OpExtend.extendDirect(subExtend, exprs2);
    	return result;
    	
    }
}
