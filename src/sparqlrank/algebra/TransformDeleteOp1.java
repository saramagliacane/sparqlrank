package sparqlrank.algebra;


import java.util.List;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.TransformCopy;
import com.hp.hpl.jena.sparql.algebra.op.Op1;


/**
 * A Transform which deletes from the Op tree objects from a given list of operators.
 * (used by TransformExtendOrder)
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class TransformDeleteOp1 extends TransformCopy {
	
	// FOR NOW it deletes just Op1 which are children of another Op1
	// TODO: extend to other ops
	
	private List<Op1> opsToDelete;
	
	public TransformDeleteOp1(List<Op1> opsToDelete){
		this.opsToDelete = opsToDelete;

	}
	

	// changed visibility of TransformCopy?
	protected Op xform(Op1 mainOp, Op subOp){
	
		if (!(subOp instanceof Op1)) return super.xform(mainOp, subOp);
		
		//Op1 op1 = (Op1) mainOp;
		
		for (Op1 op: opsToDelete){
			if (subOp.equals(op)){
					return xform((Op1) mainOp.copy(((Op1) subOp).getSubOp()), ((Op1) subOp).getSubOp());
			}
				
		}
		return super.xform(mainOp, subOp);
	}

}
