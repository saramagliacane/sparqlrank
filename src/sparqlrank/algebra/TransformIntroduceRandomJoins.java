package sparqlrank.algebra;


import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.TransformCopy;
import com.hp.hpl.jena.sparql.algebra.op.OpJoin;
import com.hp.hpl.jena.sparql.core.BasicPattern;

import sparqlrank.DefaultTestParameters;
import sparqlrank.algebra.op.OpBaseRankJoin;
import sparqlrank.algebra.op.OpRank;
import sparqlrank.algebra.op.OpRankJoinRandom;
import sparqlrank.algebra.op.OpRankSequence;

/**
 * Transformation which substitutes an OpRankJoin with an alternative version, based on the availability of random access.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */


public class TransformIntroduceRandomJoins extends TransformCopy {


   private final int joinType;
   
   public TransformIntroduceRandomJoins(final int joinType){
      this.joinType = joinType;
   }
    
   private Op introduceRandomJoins(final OpBaseRankJoin op, final Op left, final Op right) {
	   
      

     

      
      if (joinType == DefaultTestParameters.HRJNRANDOM_JOIN){
         
         final OpRank op1 = (OpRank) left;
         final OpRank op2 = (OpRank) right;
         
         final BasicPattern bp1 = op1.getSubPattern();
         final BasicPattern bp2 = op2.getSubPattern();
         
         //if (bp2!= null && bp2.size() == 1 && bp1!= null && bp1.size() == 1){
         if (bp2!= null && bp1!= null ){     
            return new OpRankJoinRandom(op1, op2, op.getScoreFunction(), op.getLeftPredicateset(), op.getRightPredicateset());
            

         }
      }
      
      else if (joinType == DefaultTestParameters.RANKSEQUENCE_JOIN){
         
         final OpRank op2 = (OpRank) right;
         
         final BasicPattern bp2 = op2.getSubPattern();
      
         // random access on the right
         if (bp2!= null && bp2.size() == 1){
         //if (bp2!= null ){   
            
            ///TODO: take away OpRank
            return new OpRankSequence(left, op2, op.getScoreFunction(), op.getLeftPredicateset(), op.getRightPredicateset());
            
            
         }

      }
      
      else if (joinType == DefaultTestParameters.COMBINED_JOIN){
         
 
         final OpRank op2 = (OpRank) right;
         
         final BasicPattern bp2 = op2.getSubPattern();
         
         if(left instanceof OpRank){
            final OpRank op1 = (OpRank) left;
            final BasicPattern bp1 = op1.getSubPattern();
            
            // random access on the both
            //if (bp2!= null && bp2.size() == 1 && bp1!= null && bp1.size() == 1){
            if (bp2!= null && bp1!= null){
               
               return new OpRankJoinRandom(op1, op2, op.getScoreFunction(), op.getLeftPredicateset(), op.getRightPredicateset());
               

            }
         }
         
         // random access on the right
         //if (bp2!= null && bp2.size() == 1){
         if (bp2!= null ){    
            ///TODO: take away OpRank
            return new OpRankSequence(left, op2, op.getScoreFunction(), op.getLeftPredicateset(), op.getRightPredicateset());
            
            
         }
            
            
            

         
         
         
      }
      
      Op newleft= left;
      Op newright= right;
      
      if (left instanceof OpBaseRankJoin){
         newleft = transform(((OpJoin) left), ((OpJoin) left).getLeft(), ((OpJoin) left).getRight());
      }
      
      if (right instanceof OpBaseRankJoin){
         newright = transform(((OpJoin) right), ((OpJoin) right).getLeft(), ((OpJoin) right).getRight());
      }

      return op.copy(newleft, newright);
      
   }

   @Override
   public Op transform(final OpJoin op, final Op left, final Op right) {
	   if (! (op instanceof OpBaseRankJoin)) return super.transform(op, left, right);
	   
 		// it optimizes the execution when we have two OpRank ops - ranked on both sides
  		if (!(right instanceof OpRank)){
  		 
  		   if( !(left instanceof OpRank) && !(left instanceof OpBaseRankJoin))
  		      return super.transform(op, left, right);
       
  		}
  			
	  	return introduceRandomJoins((OpBaseRankJoin) op, left, right);
    }
}
