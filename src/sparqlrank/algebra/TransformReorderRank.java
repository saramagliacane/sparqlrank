package sparqlrank.algebra;

import sparqlrank.algebra.op.OpRank;
import sparqlrank.baseclasses.PredicateSet;

import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.TransformCopy;
import com.hp.hpl.jena.sparql.algebra.op.OpOrder;

/**
 * A Transform that reorders OpRanks, not used.
 * -> PROPOSITION 4 in the SPARQL-Rank algebra.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

@Deprecated
public class TransformReorderRank  extends TransformCopy {
	

	// Changing the order in a chain of OpRank operators
    public Op transform(OpOrder op, Op subOp) 
    {
    	
    	if (!(op instanceof OpRank)) return super.transform(op, subOp);    
    	OpRank opRank1 = (OpRank) op;
    	
    	if (!(subOp instanceof OpRank)) return opRank1;
    	
    	OpRank opRank2 = (OpRank) subOp;
    	Op subOp2 = opRank2.getSubOp();
    	OpRank subRank = new OpRank (subOp2, opRank1.getPredicate(), opRank1.getScoreFunction(), opRank2.getSubPredicateset(), opRank1.getVars());
    	
    	PredicateSet temp = new PredicateSet();
    	temp.addAll(opRank2.getSubPredicateset());
    	temp.add(opRank1.getPredicate());
    	
    	OpRank result = new OpRank( subRank, opRank2.getPredicate(), opRank2.getScoreFunction(), temp, opRank2.getVars());
    	
    	op = null;
    	subOp = null;
    	
//    	System.out.println("in transormreorderrank");
//    	System.out.println(" opRANK1: " +opRank1.getPredicate() + " " +  opRank1.getSubPredicateset());
//    	System.out.println(" opRANK2: " +opRank2.getPredicate() + " " +  opRank2.getSubPredicateset());
//    	
    	return result;
    }
    

    
}
