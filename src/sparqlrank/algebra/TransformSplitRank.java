package sparqlrank.algebra;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.query.SortCondition;
import com.hp.hpl.jena.sparql.ARQConstants;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.OpVars;
import com.hp.hpl.jena.sparql.algebra.TransformCopy;
import com.hp.hpl.jena.sparql.algebra.op.OpOrder;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.expr.E_Add;
import com.hp.hpl.jena.sparql.expr.Expr;
import com.hp.hpl.jena.sparql.util.Context;
import com.hp.hpl.jena.sparql.util.Symbol;

import sparqlrank.algebra.op.OpGoodK;
import sparqlrank.algebra.op.OpNonBlockingTopK;
import sparqlrank.algebra.op.OpRank;
import sparqlrank.algebra.op.OpTieBreaker;
import sparqlrank.baseclasses.ExecutionModel;
import sparqlrank.baseclasses.GenericScoreFunction;
import sparqlrank.baseclasses.Predicate;
import sparqlrank.baseclasses.PredicateSet;
import sparqlrank.baseclasses.ScoreFunction;
import sparqlrank.baseclasses.SumOfPredicates;

/**
 * A Transform which introduces OpRank operator (representing the ordering with a 
 * single predicate) by splitting the OpOrder operator, 
 * so that each condition becomes a single OpRank operator.
 * -> PROPOSITION 1 in the SPARQL-Rank algebra
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class TransformSplitRank  extends TransformCopy {
	
	
	private final Boolean alwaysSum = false;
	private final int timeout = 0;
	
	private ExecutionModel exModel = null;
	private final Symbol executionModelSym = Symbol.create(ARQConstants.systemVarNS+"executionModel") ;
	
	public TransformSplitRank(){
		this(ARQ.getContext());
	}

  
	public TransformSplitRank(final Context context){
		super();
		
		if(context.isDefined(executionModelSym))
			exModel = (ExecutionModel) context.get(executionModelSym);
	}
   
	
    @Override
   public Op transform(OpOrder opOrder, Op subOp) 
    {
    	  // one rank means it's already expanded (hopefully)
    	  if (opOrder instanceof OpRank)	
    		  return opOrder;
    	  
    	  if(opOrder instanceof OpTieBreaker)
    	     return opOrder;
    	     
    	  
	      final List<SortCondition> conditions = opOrder.getConditions();
	      
	      // TODO: now we consider only the first sortCondition
	      final SortCondition mainSortCondition = conditions.get(0);
	      final Expr mainExpr = mainSortCondition.getExpression();
	      final int direction = mainSortCondition.getDirection();
	      final List<Expr> exprs = getExpressions( mainExpr );
	      
	      PredicateSet predicates;
	      
	      if (exModel!=null && exModel.isBlockingTopK()){
	    	  //forward exModel, because we need to stop as soon as we find an unbound
	    	  predicates = PredicateSet.create(exprs, direction, exModel);
	      }
	      else{
	    	  //don't forward
	    	  predicates = PredicateSet.create(exprs, direction);
	      }
	      
	      ScoreFunction scorefunction;
	      
	      if ((alwaysSum) || mainExpr.getClass().equals(E_Add.class)){
	    	  scorefunction = new SumOfPredicates(predicates);
	      }
	
	      else scorefunction = new GenericScoreFunction(predicates, mainExpr, timeout);
	      
	      final PredicateSet temp = new PredicateSet();
	      
	      final Set<Var> vars = OpVars.patternVars(subOp);

	      
	      for ( final Predicate p : predicates )
	      {
	    	  //needed to have different PredicateSets for each OpRank
	    	  //System.out.println(" Predicate " + p.getExpr());
	    	  final PredicateSet temp2 = (PredicateSet) temp.clone();
	    	  final OpRank op2 = new OpRank( subOp, p, scorefunction, temp2, vars);
	    	  subOp = op2;
	    	  temp.add(p);    	 
	    	 
	      }
	      opOrder = null;

	      
	      // backward compatibility
	      if(exModel!=null){
	    	  exModel.setScoreFunction(scorefunction);
	    	  
	    	  if (exModel.isNonBlockingTopK()) 
		    	  subOp = OpNonBlockingTopK.create(subOp, predicates);
		      
		      else if (exModel.isGoodK()) 
		    	  subOp = OpGoodK.create(subOp, exModel.getPredicate(), exModel.getDefaultValue(), scorefunction);
		      
	      }
	    	
	      
	      if(conditions.size()>1){
	         final List<SortCondition> newconditions = new ArrayList<SortCondition>();
	         newconditions.addAll(conditions);
	         newconditions.remove(0);
	         subOp = new OpTieBreaker(subOp, conditions);
	      }
	         

	      
	      return subOp;
    }
    
    
    private List<Expr> getExpressions( final Expr mainExpr ){
        final List<Expr> exprs = new ArrayList<Expr>();
        List<Expr> tempexprs = new ArrayList<Expr>();
        
      
        
        if (mainExpr.isFunction()){
        	tempexprs = mainExpr.getFunction().getArgs();
          	for (final Expr e : tempexprs){
          		
          		if (e.isFunction() && (e.getClass().equals(mainExpr.getClass()))){
          			exprs.addAll(getExpressions(e));
          		}
          		else if (e.isConstant()){
          			
          			exprs.removeAll(tempexprs);
          			exprs.add(mainExpr);

          		}
          		else {
          			 exprs.add(e);
          		}
         	}
          		  
    	}
    	else if (mainExpr.isVariable()){
    		//TODO: check if is numeric?
    		// Pros: can leave an OpSort
    		// Cons: should do a query before to understand the type of data
    		exprs.add(mainExpr);
    	}
        tempexprs = null;        
        return exprs;
    }

    

//	//old way - use separate conditions
//    public Op transform2(OpOrder opOrder, Op subOp) 
//    {
//      List<SortCondition> conditions = opOrder.getConditions();
//      
//      PredicateSet predicates;
//      
//      if (exModel.isBlockingTopK()){
//    	  //forward exModel, because we need to stop as soon as we find an unbound
//    	  predicates = PredicateSet.create(conditions, exModel);
//      }
//      else{
//    	  //don't forward
//    	  predicates = PredicateSet.create(conditions);
//      }
//      
//      ScoreFunction scorefunction = new SumOfPredicates(predicates);
//            
//      PredicateSet temp = new PredicateSet();
//      
//      Set<Var> vars = OpVars.patternVars(subOp);
//      //TODO: change
//      int limit = 0;
//      
//      for ( Predicate p : predicates )
//      {
//    	  //needed to have different PredicateSets for each OpRank
//    	  PredicateSet temp2 = (PredicateSet) temp.clone();
//    	  OpRank op2 = new OpRank( subOp, p, scorefunction, temp2, vars, limit);
//    	  subOp = op2;
//    	  temp.add(p);    	 
//    	 
//      }
//      opOrder = null;
//      
//      exModel.setScoreFunction(scorefunction);
//      
//      if (exModel.isNonBlockingTopK()) 
//    	  subOp = OpNonBlockingTopK.create(subOp, predicates);
//      
//      else if (exModel.isGoodK()) 
//    	  subOp = OpGoodK.create(subOp, exModel.getPredicate(), exModel.getDefaultValue(), scorefunction);
//      
//      return subOp;
//    }
//  
  
}