package sparqlrank.algebra;

import java.util.HashSet;
import java.util.Set;
import sparqlrank.algebra.op.OpRank;
import sparqlrank.algebra.op.OpRankJoin;
import sparqlrank.baseclasses.PredicateSet;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.OpVars;
import com.hp.hpl.jena.sparql.algebra.TransformCopy;
import com.hp.hpl.jena.sparql.algebra.op.OpExtend;
import com.hp.hpl.jena.sparql.algebra.op.OpJoin;
import com.hp.hpl.jena.sparql.algebra.op.OpOrder;
import com.hp.hpl.jena.sparql.core.Var;

/**
 * A Transform which pushes each OpRank to the deepest possible point in the algebraic tree.
 * -> Proposition 6 of the SPARQL-Rank algebra
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class TransformPushBinary  extends TransformCopy{
	
	
    public Op transform(OpOrder op, Op subOp) 
    {
    	if (!(op instanceof OpRank)) return super.transform(op, subOp);
    	OpRank opRank = (OpRank) op;    	
    	
    	if (( subOp instanceof OpJoin)||( subOp instanceof OpRankJoin)){
    		OpJoin op2 = (OpJoin) subOp;
    		return transformPushBinaryWorker (opRank, op2);
    	}
    	
    	if (subOp instanceof OpExtend){
    		OpExtend op2 = (OpExtend) subOp;
    		return transformPushBinaryWorker (opRank, op2);
    	}
    	
     	return opRank;
    }
	
    public Op transformPushBinaryWorker (OpRank opRank, OpExtend opExtend){


    	
    	Op subOp = opExtend.getSubOp();    	
    	Set<Var> vars = OpVars.patternVars(subOp) ;	
    	
    	HashSet<Var> predicatevars = opRank.getPredicate().getVars();
    	if(checkIfPush (predicatevars, vars)){
    		OpRank op = (OpRank) opRank.copy(opExtend.getSubOp());
    		
			 //recursive
			 return opExtend.copy(transform((OpRank) op, op.getSubOp()));
			 
    	}
    	
    	return opRank;
    	
    	
    }
	 
	public Op transformPushBinaryWorker (OpRank opRank, OpJoin opJoin){
		//OpJoin in order to catch also OpRankJoin
		
		// the getVars() method allows to calculate the vars of the OpRank just on its creation
		 Set<Var> predicatevars = opRank.getPredicate().getVars();
		 
		 Op leftOp = opJoin.getLeft();
		 Set<Var> leftvars = OpVars.patternVars(leftOp) ;	
		 
		 Op rightOp = opJoin.getRight();
		 Set<Var> rightvars = OpVars.patternVars(rightOp) ;

		 Boolean pushLeft = checkIfPush (predicatevars, leftvars);
		 Boolean pushRight = checkIfPush (predicatevars, rightvars);
		 
		 if( !pushLeft && !pushRight) return opRank;
		 
		 PredicateSet leftpredicateset = new PredicateSet();
		 PredicateSet rightpredicateset = new PredicateSet();
		 
		 if(opJoin instanceof OpRankJoin){
			 leftpredicateset.addAll(((OpRankJoin) opJoin).getLeftPredicateset());
			 rightpredicateset.addAll(((OpRankJoin) opJoin).getRightPredicateset());
		 }
		 
		 if(pushLeft){
			
			 PredicateSet subpredset = opRank.getSubPredicateset();
			 
			 if(leftOp instanceof OpRank){
				 subpredset.addAll(((OpRank) leftOp).getSubPredicateset());
				 subpredset.add(((OpRank) leftOp).getPredicate());
			 }
			 else if (leftOp instanceof OpRankJoin){
				 subpredset.addAll(((OpRankJoin) leftOp).getLeftPredicateset());
				 subpredset.addAll(((OpRankJoin) leftOp).getRightPredicateset());
			 }
			 else{
				 subpredset.clear();
			 }
			 leftOp = new OpRank(leftOp, opRank.getPredicate(), opRank.getScoreFunction(), subpredset, leftvars);
			 
		 
			 leftpredicateset.add(((OpRank) leftOp).getPredicate());
			 //recursive
			 leftOp = transform((OpRank) leftOp, ((OpRank) leftOp).getSubOp());
		 }
		 
		 if(pushRight){
			 PredicateSet subpredset = opRank.getSubPredicateset();
			 
			 if(rightOp instanceof OpRank){
				 subpredset.addAll(((OpRank) rightOp).getSubPredicateset());
				 subpredset.add(((OpRank) rightOp).getPredicate());
			 }
			 else if (rightOp instanceof OpRankJoin){
				 subpredset.addAll(((OpRankJoin) rightOp).getLeftPredicateset());
				 subpredset.addAll(((OpRankJoin) rightOp).getRightPredicateset());
			 }
			 else{
				 subpredset.clear();
			 }
			 rightOp = new OpRank(rightOp, opRank.getPredicate(), opRank.getScoreFunction(), subpredset, rightvars);
			 
			 rightpredicateset.add(((OpRank) rightOp).getPredicate());
			 
			 //recursive
			 rightOp = transform((OpRank) rightOp, ((OpRank) rightOp).getSubOp());
		 }	 
		 
		 return new OpRankJoin( leftOp ,rightOp, opRank.getScoreFunction() , leftpredicateset, rightpredicateset);

	    }
	 
	 
	 private Boolean checkIfPush (Set<Var> predicatevars, Set<Var> vars){
		 
		 //check if it is possible to push the OpRank operator inward the expression
		 Boolean push = true;
		 
		 for (Var p : predicatevars){
			 Boolean found = false; 
			 for (Var r: vars){
				 // need to use equals because it is true also for different object with same SortCondition
				 if (p.equals(r)){
					 found = true;
					 break;
				 }

					 
			 }
			 // if a variable of the predicate is not in the variables of the left operand
			 if (found == false) 
				 push = false;
		 }
		 return push;
	 }

}
