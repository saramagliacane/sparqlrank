package sparqlrank.algebra;


import arq.cmdline.ModTime;

import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.sparql.ARQConstants;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.Transformer;
import com.hp.hpl.jena.sparql.algebra.optimize.Optimize.RewriterFactory;
import com.hp.hpl.jena.sparql.algebra.optimize.Rewrite;
import com.hp.hpl.jena.sparql.util.Symbol;

/**
* Main class for Algebraic Optimizations
* It calls in sequence all implemented Transforms
* @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
*/


public class RankRewriter implements Rewrite {

   // called twice to get the logging done
   
	private Boolean debugAlgebraTree = false;
	private final Boolean debugResult = false;
	private long time = 0;
   private final static Boolean PRINT_TIME = false;
	
	public RankRewriter(){
		super();
	}
	
	public RankRewriter(final Boolean debugAlgebraTree){
		this.debugAlgebraTree = debugAlgebraTree;
	}
	

	  @Override
   public Op rewrite(Op op)
	    {
	     
	     final ModTime modTime =     new ModTime() ;
	     
        if (PRINT_TIME) {
          // Timer in arq.cmdline;
          modTime.startTimer() ;
          
        }
		  
		    if(debugAlgebraTree) System.out.println ("Initial ops:");
	        if(debugAlgebraTree) System.out.println(op) ;	    
	        
	        if(debugAlgebraTree) System.out.println ("TransformSplitExtend:");
	        op = Transformer.transform(new TransformSplitExtend(), op);
	        if(debugAlgebraTree) System.out.println(op) ;
	        	        
	        if(debugAlgebraTree) System.out.println ("TransformExtendOrder:");
	        op = Transformer.transform(new TransformExtendOrder(), op);
	        if(debugAlgebraTree) System.out.println(op) ;
	        
	        if(debugAlgebraTree) System.out.println ("TransformPushExtend:");
	        op = (new TransformPushExtend()).repeatedApply(op);
	        if(debugAlgebraTree) System.out.println(op) ;
	        
        
	        if(debugAlgebraTree) System.out.println ("TransformSplitRank:");
	        op = Transformer.transform(new TransformSplitRank(), op);
	        if(debugAlgebraTree) System.out.println(op) ;
	        
// 			useless	   
//	        if(debugAlgebraTree) System.out.println ("TransformReorderRank:");
//	        op = Transformer.transform(new TransformReorderRank(), op);
//	        if(debugAlgebraTree) System.out.println(op) ;

	        if(debugAlgebraTree) System.out.println ("TransformPushBinary:");
	        op = Transformer.transform(new TransformPushBinary(), op);
	        if(debugAlgebraTree || debugResult) System.out.println(op) ;
	        
//	    	Symbol useSequenceOptimizationSym = Symbol.create(ARQConstants.systemVarNS+"useSequenceOptimization") ;
//			Boolean useSequenceOptimization = ( ARQ.getContext().isTrue(useSequenceOptimizationSym));
//			
//			if (useSequenceOptimization){
//	        //pipeJoin optimization
//		        if(debugAlgebraTree) System.out.println ("TransformIntroducePipeJoin:");
//		        op = Transformer.transform(new TransformIntroducePipeJoin(), op);
//		        if(debugAlgebraTree || debugResult) System.out.println(op) ;
//			}
	      

	        // standard optimization
	        if(debugAlgebraTree) System.out.println ("Standard Optimization:");
	        final RewriterFactory f = RankRewriterFactory.getStandardRewriterFactory(ARQ.getContext());
	        if (f!=null){
	        	final Rewrite opt = f.create(ARQ.getContext());
	        	op = opt.rewrite(op) ;
	        }
	        
	        if(debugAlgebraTree || debugResult) System.out.println(op) ;
           
	        
	         final Symbol joinTypeSym = Symbol.create(ARQConstants.systemVarNS+"joinTypeSym") ;  
	         final int joinType = (Integer) ( ARQ.getContext().get(joinTypeSym));
	         
	         if(debugAlgebraTree) System.out.println ("TransformIntroduceRandomJoins:");
	         op = Transformer.transform(new TransformIntroduceRandomJoins(joinType), op);
	         if(debugAlgebraTree || debugResult) System.out.println(op) ;
	        
        
	        if(debugAlgebraTree || debugResult) System.out.println("final: " + op) ;
	        
	        
           
      
            
           if (PRINT_TIME) {
              // from standard implementation
              time = modTime.endTimer() ;
              if (time>0)
                 System.out.println("Algebraic Transformation Time: "+time+" ms") ;
              if (time>1)
                 System.out.println("final: " + op) ;

               
           }    
		  return op;
	   } 
}
