package sparqlrank.algebra;

import java.util.ArrayList;
import java.util.List;

import com.hp.hpl.jena.query.SortCondition;
import com.hp.hpl.jena.sparql.ARQInternalErrorException;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.TransformCopy;
import com.hp.hpl.jena.sparql.algebra.Transformer;
import com.hp.hpl.jena.sparql.algebra.op.Op1;
import com.hp.hpl.jena.sparql.algebra.op.OpExtend;
import com.hp.hpl.jena.sparql.algebra.op.OpOrder;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.core.VarExprList;
import com.hp.hpl.jena.sparql.expr.Expr;

/**
 * A Transform that handles the projection expression by substituing the expression in
 * the OpExtend into the OpOrder.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class TransformExtendOrder extends TransformCopy {
	
	private final List<Op1> opsToDelete = new ArrayList<Op1>();
	

	 // extend the expression in OpOrder
	 @Override
   public Op transform(final OpOrder opOrder, final Op subOp) 
	    {
		 	if(!(subOp instanceof Op1)) { return opOrder;	}
		 	
		 	Op result = substitute(opOrder, opOrder,  subOp);
		 	result = Transformer.transform(new TransformDeleteOp1(opsToDelete), result);
	        return result;
	    }
	
		private Op substitute(final OpOrder opOrder,  final Op parent, final Op subOp)	 {
			
			if (! (subOp instanceof Op1)) {
				return opOrder;
			}
			
			if (subOp instanceof OpExtend)
				return substitute( opOrder, parent, ((OpExtend) subOp));
			else 
				return substitute (opOrder, subOp, ((Op1) subOp).getSubOp());
		}
	 
		private Op substitute(final OpOrder opOrder, final Op parent, final OpExtend opExtend)	 {

		 	final VarExprList exprlist = opExtend.getVarExprList();
			
			// it should first run TransformSplitExtend, thus each expression has its own Extend op
			if(exprlist.size()!=1) 
				throw new ARQInternalErrorException();
		 
			// if used in an OpOrder, it should substitute the first level

			final Var var = exprlist.getExprs().keySet().iterator().next();
			final Expr e = exprlist.getExpr(var);
			final Op1 op1parent = opExtend; 

			
	
			final List<SortCondition> conditions = new ArrayList<SortCondition>();
			conditions.addAll(opOrder.getConditions());
			
			final List<SortCondition> temp = new ArrayList<SortCondition>();
			temp.addAll(conditions);
			
			//TODO: need to check when more Extends
			
			for ( final SortCondition c: conditions){
				final Expr sortExpr = c.getExpression();
				final int direction = c.getDirection();
				if (sortExpr.isVariable() && sortExpr.asVar().equals(var)){
				   final int i = temp.indexOf(c);
					temp.remove(c);
					temp.add(i, new SortCondition(e, direction));
					
					// delete OpExtend
					opsToDelete.add(opExtend);
						
				}
			}
			
			
			return substitute (new OpOrder(opOrder.getSubOp(), temp), op1parent, op1parent.getSubOp());
		 
	    }
}
