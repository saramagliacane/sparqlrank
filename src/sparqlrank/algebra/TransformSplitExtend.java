package sparqlrank.algebra;


import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.TransformCopy;
import com.hp.hpl.jena.sparql.algebra.op.OpExtend;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.core.VarExprList;
import com.hp.hpl.jena.sparql.expr.Expr;

/**
 * A Transform which deletes splits a composite assignment in OpExtend into several OpExtends
 * one for each assignment. Quite rare as a case.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class TransformSplitExtend  extends TransformCopy {

	// We split an OpExtend that performs several assignments into several OpExtends with single
	// assignment. This is needed in order to push down the single OpExtends and to interleave them
	// with the other rank-aware operators, as for example OpRank.
	
	public Op transform(OpExtend opExtend, Op subOp) 
    {
		VarExprList exprlist = opExtend.getVarExprList();
		
		// for each assigned variable
		for ( Var var : exprlist.getExprs().keySet()){
			// get the related Expr object
			Expr e = exprlist.getExpr(var);
			// create a new VarExprList containing the variable and expression
			VarExprList tempexpr = new VarExprList();
			tempexpr.add(var, e);
			// the only public method to create distinct OpExtends
			subOp = OpExtend.extendDirect(subOp, tempexpr);
			
		}
		
		return subOp;
    }
	
}
