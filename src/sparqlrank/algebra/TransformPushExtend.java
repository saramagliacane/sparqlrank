package sparqlrank.algebra;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import com.hp.hpl.jena.sparql.ARQInternalErrorException;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.OpVars;
import com.hp.hpl.jena.sparql.algebra.TransformCopy;
import com.hp.hpl.jena.sparql.algebra.Transformer;
import com.hp.hpl.jena.sparql.algebra.op.Op1;
import com.hp.hpl.jena.sparql.algebra.op.Op2;
import com.hp.hpl.jena.sparql.algebra.op.OpExtend;
import com.hp.hpl.jena.sparql.algebra.op.OpN;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.core.VarExprList;
import com.hp.hpl.jena.sparql.expr.Expr;

/**
 * A Transform which pushes the OpExtend inside the algebraic tree.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

// TODO: quick&dirty implementation, consider better opt.

public class TransformPushExtend extends TransformCopy {
	
	
	 public Op transform(OpExtend opExtend, Op subOp) 
	 {
		// need to push the extend to the first place in which all of its vars are defined
		// example: normalized price, it would be nice to evaluate as soon as price is defined
		 Set<Var> patternVars = OpVars.patternVars(subOp) ;		 
		 VarExprList exprlist = opExtend.getVarExprList();
		 
		 // it should first run TransformSplitExtend, thus each expression has its own Extend op
		 if(exprlist.size()!=1) 
			throw new ARQInternalErrorException();
		 
		 Op result = opExtend;
		 if( checkIfPush (exprlist, patternVars)){
			 if(subOp instanceof Op1){
				Op1 op1 = (Op1) subOp;
				result = op1.copy(transform(opExtend, op1.getSubOp()));
			}
			else if (subOp instanceof Op2){
				 Op2 op2 = (Op2) subOp;
				 Op left = op2.getLeft();
				 Op right = op2.getRight();
				 Set<Var> leftvars = OpVars.patternVars(left) ;
				 Set<Var> rightvars = OpVars.patternVars(right) ;
				 
				 Boolean pushLeft = checkIfPush (exprlist, leftvars);
				 Boolean pushRight = checkIfPush (exprlist, rightvars);

				 
				 if (pushLeft){
					 left = opExtend.copy(left);
					 left = transform((OpExtend) left, ((OpExtend) left).getSubOp());
				 }
				 
				 if (pushRight){
					 right = opExtend.copy(right);
					 right = transform((OpExtend) right, ((OpExtend) right).getSubOp());
				 }
					
				 if( !pushLeft && !pushRight) 
					 result = opExtend.copy(op2);
				 else
					 result = op2.copy(left, right);
			 }
			 else if (subOp instanceof OpN){
				 OpN opn = (OpN) subOp;

				 List<Op> ops = new ArrayList<Op>();
				 ops.addAll(opn.getElements());
				 
				 for (Op op: opn.getElements()){
					 Set<Var> opvars = OpVars.patternVars(op) ;
					 if(checkIfPush (exprlist, opvars)){
						 Op newOp = opExtend.copy(op);
						 newOp = transform((OpExtend) newOp, ((OpExtend) newOp).getSubOp());
						 ops.add(newOp);
						 ops.remove(op);
					 }
				 }
				 
				 result = opn.copy(ops);

			 	}
			 //BGPs
			 else ;

		    }
		 
		 	return result;
		 }
	 
	 
	 private Boolean checkIfPush (VarExprList exprlist, Set<Var> patternVars){
		 Var varToBeExtended = exprlist.getExprs().keySet().iterator().next();
		 Expr e = exprlist.getExpr(varToBeExtended);
		 Set<Var> exprVars = new HashSet<Var>();
		 exprVars.addAll(e.getVarsMentioned());
		 exprVars.remove(varToBeExtended);
		 
		 Boolean push = true;
		 
		 for (Var var: exprVars){
			 Boolean found = false;
			 if(patternVars.contains(var)) 
				 found = true;
			 push = found && push;
				 
		 }	
		 
		 return push;
	 }
	 
	 

	 public Op repeatedApply(Op op){
		Op oldOp = null;
		while(!sameStructure(oldOp, op)){
			oldOp = op;
			op = Transformer.transform(this, op);
		}
		return op;
	 }
	 
	
	public Boolean sameStructure(Op oldOp, Op tempOp){
			if (oldOp == null || tempOp == null) return false;
			if (oldOp.getClass() != tempOp.getClass()) return false;
			if (oldOp instanceof Op1){
				return sameStructure(((Op1)oldOp).getSubOp(), ((Op1)tempOp).getSubOp() );	
			}
			else if (oldOp instanceof Op2){
				return sameStructure(((Op2)oldOp).getLeft(), ((Op2)tempOp).getLeft() ) 
					&& sameStructure(((Op2)oldOp).getRight(), ((Op2)tempOp).getRight() );	
			}
			else if (oldOp instanceof OpN){
				 OpN opn = (OpN) oldOp;
				 OpN opn2 = (OpN) tempOp;

				 List<Op> ops = new ArrayList<Op>();
				 ops.addAll(opn.getElements());
				 
				 List<Op> ops2 = new ArrayList<Op>();
				 ops2.addAll(opn2.getElements());
				 
				 if(! ops.containsAll(ops2) || !ops2.containsAll(ops)) return false;
				 
				 Boolean same = true;
				 
				 for (Op op1: ops){
					 Boolean found = false;
					 for(Op op2 : ops2){
						 found = found || sameStructure(op1, op2);
					 }
					 same = same && found;
				 }

				 for (Op op1: ops2){
					 Boolean found = false;
					 for(Op op2 : ops){
						 found = found || sameStructure(op1, op2);
					 }
					 same = same && found;
				 }
				 
				 return same;
			 	}
			else{
				return oldOp.equals(tempOp);
			}
			
		}
}
