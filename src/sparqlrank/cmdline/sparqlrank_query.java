package sparqlrank.cmdline;

import sparqlrank.TestParameters;
import sparqlrank.algebra.RankRewriterFactory;
import sparqlrank.iterators.RankExecutorFactory;
import arq.query;

/**
 * Class to call in order to execute queries from the shell.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 *
 */

public class sparqlrank_query {

    public static void main (String... argv)
    {
    	System.out.println("SPARQL-Rank execution");
    	new TestParameters(argv);
    	RankRewriterFactory.setSPARQLRankRewriter();
    	RankExecutorFactory.setRankExecutor();
        query.main(argv) ;
    }
}
