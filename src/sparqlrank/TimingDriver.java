package sparqlrank;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;

import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.sparql.ARQConstants;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.util.Symbol;

import sparqlrank.datasetmodificator.BSBMDataModificator;


/**
 * The class which does the timing experiments - benchmarking
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */
public class TimingDriver {
	
	private static Boolean firstDataset = true;
	private final static Boolean ENABLE_LOGGING = false;
	private final static Boolean DO_WARM_UP = true;

	// hard-coded warm-up query: queries_8_1_0.txt
	private final static String phantomQuery = "prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> select ?offer ?product  ((?avgscore1) AS ?score) { ?product bsbm:avgscore1 ?avgscore1 . ?product bsbm:avgscore2 ?avgscore2 .    ?product bsbm:avgscore3 ?avgscore3 .	?offer bsbm:product ?product .    ?offer bsbm:price ?price } order by ?score";
	
	private final static Symbol notInCacheSym = Symbol.create(ARQConstants.systemVarNS+"notInCache") ;
	
	public static void main(final String[] args){

			// create an object TestParameters, that contains all the set parameters for each test
			// in case a parameter is not specified, TestParameter contains the default values
			final TestParameters param = new TestParameters(args);
	      BSBMDataModificator.modifyPossibleDatasets(param);
			
	      createMainExperimentsDirectories(param);
			
         createExperimentsDirectories(param, 1);

			setupTimingExperiments2(param);
			
	      ReportingDriver.setupReportingExperiments( param);
			
	}
	
	public static void createExperimentsDirectories(final TestParameters param){

      if(ENABLE_LOGGING)
         System.out.println("NEW EXPERIMENTS DIR:" + param.getExperimentDirectory());
       
      // if the reporting (saving execution trees and counting bindings) is enabled
      // create a new dir called "treeDirectory" in the experiments dir
      final File treeDirectory = new File(param.getExperimentDirectory() + "/treeDirectory/");
      treeDirectory.mkdir();
      param.setTreeDirectory(treeDirectory);
      if(ENABLE_LOGGING)
         System.out.println("\t NEW TREE DIR:" + param.getTreeDirectory());
      

	}
	
	  public static void createMainExperimentsDirectories(final TestParameters param){
	      // create new experimentDirectory in the ARQ main dir
	      final File experimentDirectory = new File("Experiments_"  +  System.currentTimeMillis() +"/");
	      experimentDirectory.mkdir();
	      param.setExperimentDirectory(experimentDirectory);
	      
	      if(ENABLE_LOGGING)
	         System.out.println("NEW MAIN DIR:" + param.getExperimentDirectory());
	      
	   }
	
	  public static void createExperimentsDirectories(final TestParameters param, final int n){

	     final File experimentDirectory = new File( param.getExperimentDirectory() +"/Experiment_"  +  n +"/");
        experimentDirectory.mkdir();
        param.setExperimentDirectory(experimentDirectory);
        
        createExperimentsDirectories(param);

	   }
	
   
	public static void setupTimingExperiments(final TestParameters param){
			
			
			// iterate on all possible datasets
			for (final String dataset: param.possibleDatasets){
				
			   final File dsDirectory = new File(param.getExperimentDirectory() + "/" + dataset + "/");
            final File compareRJDirectory = new File(dsDirectory +"/RankJoinComparison/");
          	
				if(param.getTiming()){
				   if(ENABLE_LOGGING)
				      System.out.println("\t NEW DATASET DIR:" + dsDirectory);
	            dsDirectory.mkdir();
	 
				}
				
				param.setDataset(dataset);
				
				if(param.getReporting()){
					final File treeDsDirectory = new File(param.getTreeDirectory() + "/" + dataset + "/");
					if(ENABLE_LOGGING)
						System.out.println("\t NEW TREE DATASET DIR:" + treeDsDirectory);
					treeDsDirectory.mkdir();
				}

				if(param.getCompareRankJoins()){
              compareRJDirectory.mkdir();
				}
				
				// a file for each dataset
				final Model model = BSBMDataModificator.createModel(param);
				

				// auto-generate queries
				if(param.getQueryDirName() == null){
					for(final Integer numberOfOrderByVars : param.possibleNumberOfVars ){
						param.setNumberOfOrderByVars(numberOfOrderByVars);			 			
						performExperiment( param, dsDirectory,  model, "");
						
					}// end for numberOfVars
				}
				// take queries from directory
				// variable limit, repeat "repeat" times
				else {
//						int i = 1;
						File queryDirectory;
						File[] listOfQueryFiles = {};
						
						try{
							queryDirectory = new File(param.getQueryDirName());
							listOfQueryFiles = queryDirectory.listFiles();
						}catch(final Exception e){
							// if the specified query directory does not exist 
							return;
						}
				    	
						// warm-up experiment
						if (DO_WARM_UP) {
						   final TestParameters p = new TestParameters();
						   p.setRepeatStandard(1);
						   p.setRepeatModified(1);
						   p.setCaching(false);
						   p.setReporting(false);
						   p.setPower(1);
							performExperiment( p, dsDirectory, model, phantomQuery);
						
						}

						Arrays.sort(listOfQueryFiles, new Comparator<File>(){
						    @Override
                     public int compare(final File f1, final File f2)
						    {
						        return f1.getName().compareTo(f2.getName());
						    } });

						
						
						for (final File file: listOfQueryFiles){
							
							// while(true){
							// the query is not in the list 
//							if(! possibleQueries.contains(i)){i++; continue;} else{
							
							String queryName = file.getName();
							
							
							if(queryName.startsWith("query"))
								queryName = queryName.split("query")[1];
							else continue;
							
							if(queryName.endsWith(".txt"))
								queryName = queryName.split(".txt")[0];
							else continue;					
							

							
						   param.setQueryName(queryName);
					    	System.out.println("--- Query name " + queryName);
						    	
							final String baseString = RankExecutionUtils.readQueryFromFile(file);
						   	
						    // is in the list but is null 
							if(baseString == null) 
								return;	
							
							performExperiment( param, dsDirectory, model, baseString);
	                  
							
							if (param.getPlanType()==2 && param.getCompareRankJoins()){
							   
							   
				            
							   final String temp = queryName.substring(0,queryName.indexOf("_"));
							      
	                     param.setQueryName(temp+"_"+param.getNumberOfOrderByVars()+"_3");
							   param.setJoinType(DefaultTestParameters.HRJNSTAR_JOIN);
	                     performExperiment( param, compareRJDirectory, model, baseString);
							   
	                     param.setQueryName(temp+"_"+param.getNumberOfOrderByVars()+"_4");
	                     param.setJoinType(DefaultTestParameters.RANKSEQUENCE_JOIN);
	                     try{
	                        performExperiment( param, compareRJDirectory, model, baseString);
	                     }catch (final java.lang.ClassCastException e){
	                        System.out.println("Could not apply RSEQ to query " + queryName);
	                     }
	                     
	                     param.setQueryName(temp+"_"+param.getNumberOfOrderByVars()+"_5");
	                     param.setJoinType(DefaultTestParameters.HRJNRANDOM_JOIN);
	                     try{
                           performExperiment( param, compareRJDirectory, model, baseString);
                        }catch (final  java.lang.ClassCastException e){
                           System.out.println("Could not apply RA-HRJN to query " + queryName);
                        }
	                     
	                     param.setJoinType(DefaultTestParameters.COMBINED_JOIN);
							   
							}

						}
						
						
						
				}// else
				
						
				model.close();				
				firstDataset = false;

			
			}// end for dataset
			

			firstDataset = true;
			
	}
	
	public static void setupTimingExperiments2(final TestParameters param){
      
      final int repeatStd = param.getRepeatStandard();
	   
      // iterate on all possible datasets
      for (final String dataset: param.possibleDatasets){
         
         final File dsDirectory = new File(param.getExperimentDirectory() + "/" + dataset + "/");
         final File compareRJDirectory = new File(dsDirectory +"/RankJoinComparison/");
         
         if(param.getTiming()){
            if(ENABLE_LOGGING)
               System.out.println("\t NEW DATASET DIR:" + dsDirectory);
            dsDirectory.mkdir();
 
         }
         
         param.setDataset(dataset);
         
         if(param.getReporting()){
            final File treeDsDirectory = new File(param.getTreeDirectory() + "/" + dataset + "/");
            if(ENABLE_LOGGING)
               System.out.println("\t NEW TREE DATASET DIR:" + treeDsDirectory);
            treeDsDirectory.mkdir();
         }

         if(param.getCompareRankJoins()){
           compareRJDirectory.mkdir();
         }
         
         // a file for each dataset
         final Model model = BSBMDataModificator.createModel(param);
         

         
         param.setRepeatStandard(1);
         param.setRepeatModified(1);

         File queryDirectory;
         File[] listOfQueryFiles = {};
               
         try{
            queryDirectory = new File(param.getQueryDirName());
            listOfQueryFiles = queryDirectory.listFiles();
         }catch(final Exception e){
            // if the specified query directory does not exist 
            return;
         }
               

         Arrays.sort(listOfQueryFiles, new Comparator<File>(){
                   @Override
                  public int compare(final File f1, final File f2)
                   {
                       return f1.getName().compareTo(f2.getName());
                   } });

          for (int i = 0; i < repeatStd; i++){
               
               for (final File file: listOfQueryFiles){
                  
                  String queryName = file.getName();
                  
                  
                  if(queryName.startsWith("query"))
                     queryName = queryName.split("query")[1];
                  else continue;
                  
                  if(queryName.endsWith(".txt"))
                     queryName = queryName.split(".txt")[0];
                  else continue;             
                  

                  
                  param.setQueryName(queryName);
                  param.setCountStandard(i);
                  param.setCountModified(i);
                  System.out.println("--- Query name " + queryName);
                     
                  final String baseString = RankExecutionUtils.readQueryFromFile(file);
                     
                   // is in the list but is null 
                  if(baseString == null) 
                     return;  
                  
                  performExperiment( param, dsDirectory, model, baseString);
                  
                  
                  if (param.getPlanType()==2 && param.getCompareRankJoins()){
                     
                     
                     
                     final String temp = queryName.substring(0,queryName.indexOf("_"));
                        
                     param.setQueryName(temp+"_"+param.getNumberOfOrderByVars()+"_3");
                     param.setJoinType(DefaultTestParameters.HRJNSTAR_JOIN);
                     performExperiment( param, compareRJDirectory, model, baseString);
                     
                     param.setQueryName(temp+"_"+param.getNumberOfOrderByVars()+"_4");
                     param.setJoinType(DefaultTestParameters.RANKSEQUENCE_JOIN);
                     try{
                        performExperiment( param, compareRJDirectory, model, baseString);
                     }catch (final java.lang.ClassCastException e){

                        System.out.println("Could not apply RSEQ to query " + queryName);
                     }
                     
                     param.setQueryName(temp+"_"+param.getNumberOfOrderByVars()+"_5");
                     param.setJoinType(DefaultTestParameters.HRJNRANDOM_JOIN);
                     try{
                        performExperiment( param, compareRJDirectory, model, baseString);
                     }catch (final java.lang.ClassCastException e){

                        System.out.println("Could not apply RA-HRJN to query " + queryName);
                     }
                     
                     param.setJoinType(DefaultTestParameters.COMBINED_JOIN);
                     
                  }

               }
               
      
          }
               
         model.close();          
         firstDataset = false;

      
      }// end for dataset
      

      firstDataset = true;
      
}




	// perform an experiment on one query, by adding increasing limits and repeating the execution
	public static void performExperiment(final TestParameters param, final File dsDirectory, final Model model, final String baseString){
		
	   BufferedWriter measurementFile = null;
	   
	   if(param.getTiming()){
	      measurementFile = createMeasureWriter(dsDirectory, param);		
	      insertHeadLine(measurementFile,param);
	   }
	   
        
	   
    	for (int j = param.getStartingpower(); j< param.getPower(); j++){
    		
			// iterate on the value of K
			param.setK((int) ( Math.pow(10.0, j)));
			
						
			final int k = param.getK();		
			String queryString;

			if(param.getQueryDirName() == null)
				queryString = ExampleQueryCreator.createQueryString(k, param.getNumberOfOrderByVars());
			else{
				if (k<=0) queryString = baseString + " LIMIT 1";
				else queryString =  baseString + " LIMIT " + k + "\n";
			}
			
	
    	
			//writeQueryToFile(measurementFile, queryString);
			Op op = null, standardOp = null;
    	
	      //SPARQLRankCache.freeSPARQLRankCache();
			
	      // STANDARD EXECUTION
	      param.setModifiedExecution(false);
			for(int count = 0; count<param.getRepeatStandard(); count++){
				
			   param.setCountStandard(count);
   			
				op = RankExecutionUtils.executeQuery(model, queryString, param, ENABLE_LOGGING);

	         if(param.getTiming()){
	            writeMeasuresToFile(measurementFile, param);
	         }
	         
			}
			
			param.setCountStandard(0);
		
			standardOp = op;	
			
			// MODIFIED EXECUTION
			param.setModifiedExecution(true);
			
			for(int count = 0; count<param.getRepeatModified(); count++){
            
            param.setCountModified(count);
            op = RankExecutionUtils.executeQuery(model, queryString, param, ENABLE_LOGGING);
            
            if(param.getTiming()){
               writeMeasuresToFile(measurementFile, param);
            }
            
         }
			
			
			
			if(j==param.getStartingpower() && firstDataset && param.getReporting()){

				writeOpsToFile( param, op, standardOp);
			}

			

//         if(param.getTiming()){
//            insertEndOfLine(measurementFile);
//         }
		}//for
    	

      if(param.getTiming()){
         try {
            measurementFile.close();
         } catch (final IOException e) {
            e.printStackTrace();
         }
      }


		
	}


	public static BufferedWriter createMeasureWriter(final File directory, final TestParameters param){
		try{
			if(ENABLE_LOGGING)
				System.out.println("\t\tNEW FILE:" + directory + "/" + "measures"+"_"+ param.getQueryName() +".txt");			
			return new BufferedWriter(new FileWriter(directory + "/" + "measures"+"_"+ param.getQueryName() + "_"+ param.getCountStandard()+".txt"));
		} catch(final IOException e) { System.err.println("Could not create file measures.dat."); System.exit(-1);}
		
		return null;
			
	}
	
	public static void writeMeasuresToFile(final BufferedWriter bw, final TestParameters param){
		
		
		try{
			if (param.getQueryDirName() == null)
				bw.append("\n Modified :" + param.getModifiedExecution() + "&dataset:" + param.getDataset() + "&k:" + param.getK() + 
						"&"+ "number of vars:"+ param.getNumberOfOrderByVars() + "&time:"+ RankExecutionUtils.getExecutionTime()  + "&ms\n");
			else{
			   
			   String joinStrategy = "";
			   String planType = "";
			   
			   switch (param.getJoinType()){
			      case DefaultTestParameters.HRJNSTAR_JOIN:
			         joinStrategy = "HRJN"; break;
			      case DefaultTestParameters.RANKSEQUENCE_JOIN:
			         joinStrategy = "RSEQ"; break;
			      case DefaultTestParameters.HRJNRANDOM_JOIN:
			         joinStrategy = "RA-HRJN"; break;
			      case DefaultTestParameters.COMBINED_JOIN:
			         joinStrategy = "COMBINED"; break;
			       
			   }
			   
	         switch (param.getPlanType()){
               case 0:
                  planType = "ROB"; break;
               case 1:
                  planType = "INTER"; break;
               case 2:
                  planType = "RJ"; break;
                
            }

			   

			   bw.append( param.getDataset() + "&" + param.getQueryName()+ "&"+ param.getNumberOfOrderByVars()+ "&"+ planType+ "&"+ param.getK()+"&"+ param.getModifiedExecution()+ "&" +joinStrategy+ "&"+param.getCaching()+ "&"+
                     param.getCount() + "&"+ RankExecutionUtils.getExecutionTime()  + "\n");
            

			}

			bw.flush();
		} catch(final IOException e) { e.printStackTrace(); System.exit(-1);}
		

		
		//TODO: remove, find another way?
		ARQ.getContext().set(notInCacheSym, false);
	}
	
	public static void insertHeadLine(final BufferedWriter bw, final TestParameters param){
		try{bw.append("dataset&query_num&numvars&plan&limit&modified&joinType&caching&count&extime");bw.append("\n");} catch(final IOException e) { e.printStackTrace(); System.exit(-1);}
	}
	
	public static void insertEndOfLine(final BufferedWriter bw){
		try{bw.append("\n");} catch(final IOException e) { e.printStackTrace(); System.exit(-1);}
	}
	
	public static void writeQueryToFile(final BufferedWriter bw, final String queryString){
		try{bw.append(queryString); bw.append("\n");} catch(final IOException e) { e.printStackTrace(); System.exit(-1);}
	}
	
	
	public static void writeOpsToFile( final TestParameters param, final Op op, final Op standardOp){
		
		BufferedWriter bw = null;
		try{
			if(ENABLE_LOGGING)
				System.out.println("\t\tNEW FILE:" + param.getTreeDirectory() + "/" + "tree"+"_"+ param.getQueryName() +".txt");
			bw = new BufferedWriter(new FileWriter(param.getTreeDirectory() + "/" + "tree"+"_"+ param.getQueryName() +".txt"));
		} catch(final IOException e) { System.err.println("Could not create file tree."); System.exit(-1);}
		
		try{
			bw.append("standardOp: \n" + standardOp + "\n"+"modifiedOp: \n" + op );
			bw.close();
		} catch(final IOException e) { e.printStackTrace(); System.exit(-1);}
		
	}
	
	
	
	public static BufferedWriter createTreeStatsWriter(final TestParameters param){
		BufferedWriter bw = null;
		String modified;
		
		if (param.getModifiedExecution())	modified = "mod";
		else modified = "std";
		
		final String filename = param.getTreeDirectory() +"/"+ param.getDataset()+"/" + "treestats"+"_"+ param.getQueryName() + "_" +modified+".txt";
		
		try{
			if(ENABLE_LOGGING)
				System.out.println("\t\tNEW FILE:" + filename);
			bw = new BufferedWriter(new FileWriter(filename, true));
		} catch(final IOException e) { System.err.println("Could not create file treestats."); System.exit(-1);}
		return bw;
	}
	
	  public static BufferedWriter createDistributionWriter(final TestParameters param){
	      BufferedWriter bw = null;
	      String modified;
	      
	      if (param.getModifiedExecution())   modified = "mod";
	      else modified = "std";
	      
	      final String filename = param.getTreeDirectory() +"/"+ param.getDataset()+"/" + "distribution"+"_"+ param.getQueryName() + "_" +modified+".txt";
	      
	      try{
	         if(ENABLE_LOGGING)
	            System.out.println("\t\tNEW FILE:" + filename);
	         bw = new BufferedWriter(new FileWriter(filename, true));
	      } catch(final IOException e) { System.err.println("Could not create file distribution."); System.exit(-1);}
	      return bw;
	   }

}



