package sparqlrank.baseclasses;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import com.hp.hpl.jena.query.SortCondition;
import com.hp.hpl.jena.sparql.expr.Expr;

/**
 * Set of {@link Predicate} objects with some additional methods.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class PredicateSet extends LinkedHashSet<Predicate>{
	
	private static final long serialVersionUID = -3123517272516313944L;
	private static final Boolean debug = false;

	
	// removes a Predicate from the given PredicateSet
	public PredicateSet removePredicate (Predicate p){
		
		// needed to use the iterator on this
		PredicateSet temppred = new PredicateSet();
		
		for (Predicate p2: this){
	    	  if(p2.equals(p)) temppred.add(p2);
	    }
		
		this.removeAll(temppred);
		
		return this ;
	}
	

	// removes a set of Predicates from the given PredicateSet
	public PredicateSet removeAllPredicates (PredicateSet predicatesToRemove){
		
		for (Predicate p : predicatesToRemove){
			removePredicate (p);
		}
		
		return this;
	}
	
	// returns an epurated PredicateSet without modifying the arguments
	public static PredicateSet removeAllPredicates (PredicateSet predicates, PredicateSet predicatesToRemove){
		
		PredicateSet temppred = new PredicateSet();
		temppred.addAll(predicates);
		
		for (Predicate p : predicatesToRemove){
			temppred.removePredicate (p);
		}
		
		return temppred;
	}
	
	
	public static PredicateSet intersection (PredicateSet left, PredicateSet right){
		
		PredicateSet temppred = new PredicateSet();
		for (Predicate p : left){
			
			for (Predicate p2: right){
		    	  if(p2.equals(p)) temppred.add(p2);
		      }
		}
		
		return temppred;
	}
	
	public static PredicateSet union (PredicateSet left, PredicateSet right){
		
		PredicateSet temppred = new PredicateSet();
		temppred.addAll(left);
		temppred.addAll(right);		
		return temppred;
	}

	public boolean containsSimilar ( Predicate p){
		if (this.contains(p)) return true;
		for (Predicate p1: this){
			if (p1.equals(p)) return true;
		}
		return false;
	}

	


	public static PredicateSet create(List<SortCondition> list){
		PredicateSet p = new PredicateSet();
		for (SortCondition c: list){
			p.add(new Predicate(c));
			if(debug) System.out.println("predicate created: "+ p);
		}
		return p;
	}

	public static PredicateSet create(List<SortCondition> list, ExecutionModel exModel){
		PredicateSet p = new PredicateSet();
		for (SortCondition c: list){
			p.add(new Predicate(c, exModel));
		}
	return p;
	}
	

//	public static PredicateSet create(List<SortCondition> list, ExecutionContext execCtx){
//		PredicateSet p = new PredicateSet();
//		for (SortCondition c: list){
//			p.add(new Predicate(c, execCtx));
//		}
//	return p;
//	}
	

	@Deprecated
	public static PredicateSet removePredicate (HashSet<Predicate> predicates, Predicate p){
		
		PredicateSet temppred = new PredicateSet();
		temppred.addAll(predicates);
		
		for (Predicate p2: predicates){
	    	  if(p2.equals(p)) temppred.remove(p2);
	      }
		
		return temppred;
	}


	public static PredicateSet create(List<Expr> exprs, int direction, ExecutionModel exModel) {

		List<SortCondition> list = new ArrayList<SortCondition>();
		for (Expr e: exprs)
			list.add(new SortCondition (e, direction));

		exprs = null;
		return create(list, exModel);
		
	}
	
//	public static PredicateSet create(List<Expr> exprs, int direction, ExecutionContext exCtx) {
//
//		List<SortCondition> list = new ArrayList<SortCondition>();
//		for (Expr e: exprs)
//			list.add(new SortCondition (e, direction));
//
//		exprs = null;
//		return create(list, exCtx);
//		
//	}
	
	
	public static PredicateSet create(List<Expr> exprs, int direction) {

		List<SortCondition> list = new ArrayList<SortCondition>();
		for (Expr e: exprs)
			list.add(new SortCondition (e, direction));

		exprs = null;
		return create(list);
		
	}
	
	public String toString(){
		String newString = "[";
		for (Predicate p: this){
			newString += p.getSortCondition().getExpression().toString() +"," ;
	    }
		newString +="]";
		
		return newString;
	}
	
	
	public boolean equals(Object other){
        
        if ( this == other ) return true ;
        if ( ! ( other instanceof PredicateSet) ) 
            return false ;
        
        PredicateSet predother = (PredicateSet) other;
		
        if (predother.size()!= this.size())
        	return false;
        
        for (Predicate p: predother){
			if(!this.containsSimilar(p))
				return false;
		}
        
        for (Predicate p: this){
			if(!predother.containsSimilar(p))
				return false;
		}
        
        return true;
	}
	
    public static int hashCode(PredicateSet other)
    {
        int hash = 0xC0 ;
       
        for (Predicate p : other){
        	hash ^= p.getExpr().hashCode();        
        }
       
        return hash ;
    }
	
}
