package sparqlrank.baseclasses;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.query.SortCondition;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.expr.Expr;
import com.hp.hpl.jena.sparql.expr.ExprVar;
import com.hp.hpl.jena.sparql.expr.NodeValue;
import com.hp.hpl.jena.sparql.expr.VariableNotBoundException;
import com.hp.hpl.jena.sparql.function.FunctionEnv;

/** 
 * This class represents a predicate function (find better name, ranking function?).
 * A predicate function is the basic block of the ranking and is currently implemented as 
 * a wrapper for a SortCondition.
 * For each query the global ordering is composed by a function (ScoreFunction) of a set of
 * Predicates.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class Predicate {
	
	private SortCondition condition;
	private HashSet<Var> variables;
	private String name;
	
	// not used feature related to the evaluation of predicates in case of GoodK or NonBlockingTopK execution models
	private ExecutionModel exModel = null;
	private Boolean useExecutionModel = false;
	
	// decide if to avoid exception, but add overhead
	private Boolean avoidVariableNotBoundException = false;
	
	public Predicate(SortCondition c){
		condition = c;
		variables = new HashSet<Var>();
		variables.addAll(c.getExpression().getVarsMentioned());
		String expr = c.getExpression().toString();
		expr = expr.replace("?", "");
		name = "predicate_"+ expr;

	}
	
	// in case we sort on a variable
	public Predicate(String varName, int direction){
	
	    this(new SortCondition(new ExprVar(varName), direction));
		
	}
	
//	public Predicate (SortCondition c, ExecutionContext execCtx){
//		this(c);
//		this.useExecutionModel = true;
//		this.exModel = (ExecutionModel) execCtx.getContext().get(Symbol.create(ARQConstants.systemVarNS+"executionModel"));
//	}
	
	public Predicate (SortCondition c, ExecutionModel exModel){
		this(c);
		this.useExecutionModel = true;
		this.exModel = exModel;
	}
	
	
	public SortCondition getSortCondition(){return condition;}
	public List<SortCondition> getListOfSortConditions(){
		  List<SortCondition> conditions = new ArrayList<SortCondition>();
		  conditions.add(getSortCondition());		
		  return conditions;}
	public Expr getExpr(){return condition.expression;}
	public HashSet<Var> getVars(){return variables;}
	public String getName(){return name;}
	

	public Score eval(Binding b, FunctionEnv execCtx){	
		
		// this method allows to compute a part of the maximal-possible score of each 
		// binding.
		// in particular, when evaluating a predicate function over a binding with unbound
		// values for the variables of the Predicate, we decide to assign the value 1.0 to
		// the evaluated Score (upperbound of the predicate)
		
		NodeValue n;
		Boolean foundUnbound = false;
		Expr expr = getExpr();
		
		
		if (avoidVariableNotBoundException){
			// we can check if we can avoid a not bound variable evaluation
			// but from the current experimental results it's faster not to
			for (Var v: expr.getVarsMentioned()){
				Node nv = b.get(v);
				if ( nv == null )
					// there is at least one unbound variable
					foundUnbound = true;

			}
		
		}
		
		
		try{	
			// if at least one variable is unbound, assign to the predicate its upperbound
			if (foundUnbound) n = NodeValue.makeDouble(1.0);
			// otherwise evaluate it as usual
			else   	    	  n = expr.eval(b, execCtx);
		}
		catch(VariableNotBoundException e){  
//			System.out.println("Evaluating predicate: "+ condition.getExpression().toString()+ " " +e);
			
			n = NodeValue.makeDouble(1.0);
			
			if(useExecutionModel){
				if(exModel.isBlockingTopK()) {
					System.out.println("ExecutionModel BlockingTopK: Query rejected - unbound predicates ");
					throw e;
				}
				else if (exModel.isGoodK()){
					n = NodeValue.makeDouble(exModel.getDefaultValue());
				}
				
			}
	
		
		}

		Double d = 0.0;
		


		if (n.isDouble()){
			d = n.getDouble();
		}
		else{
			throw new UncorrectPredicateValuesException();
		}
		
		if ((d<0)||(d>1)) 
			throw new UncorrectPredicateValuesException();
		
		
		return new Score(d);
	}
	

	
	
	
	public Boolean equals(Predicate p){
		Boolean value = (p.getSortCondition().equals(condition));
		
		//System.out.println("EQUALS "+ p + " "+ p.getExpr().getVarName() + " and " + this + " "+ getExpr().getVarName() + " "+ value);
		return value;
	}
	
	
	public void close(){
		condition = null;
		variables = null;
	}
	
	public Predicate copy(){
		return new Predicate(condition);
	}
	
	public String toString(){
		return condition.getExpression().toString();
	}

}
