package sparqlrank.baseclasses;

/**
 * Class representing a Score - right now a Double
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class Score {
	
	private Double score = null;
	public Score(){score = 0.0;}
	public Score(final Double d) {score = d;}
	public Score(final Integer i) {score = new Double(i);}
	
	public void add(final Double s){ score +=s;}
	public void add(final double d){ score += d;}
	public void add(final Integer i){score += i;}	
	public void add(final Score temp) { score += temp.getDouble();}
	
	public void set(final double d){ score = d;}
	public void set(final Integer d){ score = new Double(d);}
	public Double getDouble(){return score;}
	
	public static Boolean greaterThan (final Score s1, final Score s2){
		if ((s1 == null) || (s2 == null)) return false;
		return (s1.getDouble()>= s2.getDouble());
	}
	
	public static Boolean greater (final Score s1, final Score s2){
		if ((s1 == null) || (s2 == null)) return false;
		else return (s1.getDouble()> s2.getDouble());
	}
	
	
	public static int compare (final Score s1, final Score s2){
		return - Double.compare(s1.getDouble(), s2.getDouble());
	}
	
	public static Score max(final Score s1, final Score s2){
		if (greaterThan(s1, s2)){
			return s1;
		}
		else return s2;
	}
	
	public Boolean checkIfValidScore(){
		return ((score>=0) && (score<=1));
	}
	                                           

	/** optimization since there are many instantiations of zeroScore
	 */
	public static final Score immutableZeroScore = new Score(0.0);

	@Override
   public String toString(){
	   return this.getDouble().toString();
	}

	@Override
   public boolean equals(final Object s2){
	   if(!(s2 instanceof Score))
	      return false;
	   if(score == null)
	      return false;
	   final Double val = ((Score) s2).getDouble();
	   if (val == null)
	      return false;
	   return score.equals(val);
	}
	
}
