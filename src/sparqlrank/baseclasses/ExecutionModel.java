package sparqlrank.baseclasses;

import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.sparql.ARQConstants;
import com.hp.hpl.jena.sparql.util.Context;
import com.hp.hpl.jena.sparql.util.Symbol;

/**
 * Do not consider, abandoned direction 
 * Object containing the chosen Execution Model
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class ExecutionModel {
	
	private int type;
	private static final int NONE = 0;
	private static final int BLOCKING_TOP_K = 1;
	private static final int NON_BLOCKING_TOP_K = 2;
	private static final int GOOD_K = 3;
	
	private Predicate predicateToSubstitute;
	private Double defaultValue;
	private ScoreFunction scoreFunction;
	
	private static ExecutionModel singleton = null;
	
	
	public static ExecutionModel createExecutionModel(){
		return createExecutionModel( "", null, null, ARQ.getContext() );
	}

	public static ExecutionModel createExecutionModel(String exModel, Context context ){
		return createExecutionModel( exModel, null, null, context );
	}
	
	public static ExecutionModel createExecutionModel(Predicate predicate, Double defaultValue, Context context ){
		return createExecutionModel( "goodK", predicate, defaultValue, context );
	}
	
	
	public static ExecutionModel createExecutionModel(String exModel, Predicate predicate, Double defaultValue, Context context ){
		
		if(singleton==null){
			singleton = new ExecutionModel( exModel, predicate,  defaultValue );
			register(singleton, context);
		}
		return singleton;
	}
	
	private static void register(ExecutionModel exModel, Context context){
		Symbol executionModelSym = Symbol.create(ARQConstants.systemVarNS+"executionModel") ;
		context.set(executionModelSym, exModel) ;
	}
	
	
	private ExecutionModel(String exModel, Predicate predicate, Double defaultValue){
		
		if (exModel.startsWith("blocking")){
			type = BLOCKING_TOP_K;
		}
		else if (exModel.startsWith("non")){
			type = NON_BLOCKING_TOP_K;
		}
		else if (exModel.startsWith("good")){
			type = GOOD_K;
			this.predicateToSubstitute = predicate;
			this.defaultValue = defaultValue;
		}
		else type = NONE;

	}
	
	
	
	public boolean isBlockingTopK() {
		return (type == BLOCKING_TOP_K);
	}
	
	public boolean isNonBlockingTopK() {
		return (type == NON_BLOCKING_TOP_K);
	}
	
	public boolean isGoodK() {
		return (type == GOOD_K);
	}

	public void setDefaultValue(Double defaultValue) {
		this.defaultValue = defaultValue;
	}

	public Double getDefaultValue() {
		return defaultValue;
	}


	public Predicate getPredicate() {
		return predicateToSubstitute;
	}

	public void setScoreFunction(ScoreFunction scoreFunction) {
		this.scoreFunction = scoreFunction;
	}

	public ScoreFunction getScoreFunction() {
		
		return scoreFunction;
	}


}
