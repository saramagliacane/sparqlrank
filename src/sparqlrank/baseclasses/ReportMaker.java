package sparqlrank.baseclasses;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.sparql.ARQConstants;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIter1;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIter2;
import com.hp.hpl.jena.sparql.util.Context;
import com.hp.hpl.jena.sparql.util.Symbol;

@Deprecated
public class ReportMaker {

	private Boolean enableCountBindings;
	private Boolean enableMeasureTime;
	
	
	private HashMap<QueryIterator, ArrayList<Integer>> countBindings = new HashMap<QueryIterator,ArrayList<Integer>> ();
	//private HashMap<QueryIterator, Double>  measureTime = new HashMap<QueryIterator, Double>() ;
	
	private int INPUT = 2;
	private int LEFT = 2;
	private int RIGHT = 3;
	
	private static ReportMaker singleton = null;
	
	public static ReportMaker createReportMaker(Boolean countB, Boolean measureT, Context context ){
		if (singleton == null) {
			singleton = new ReportMaker( countB,  measureT );
			Symbol reportMakerSym = Symbol.create(ARQConstants.systemVarNS+"reportMaker") ;
			context.set(reportMakerSym, singleton) ;
		}
		return singleton;
	}
	
	public static ReportMaker createReportMaker(Boolean countB,  Boolean measureT, ExecutionContext execCtx ){
		return createReportMaker(countB, measureT,execCtx.getContext());
	}
	
	public static ReportMaker createReportMaker(){
		return createReportMaker(false, false, ARQ.getContext());
	}
	
	private ReportMaker(Boolean countB, Boolean measureT ){
		setEnableCountBindings(countB);
		setEnableMeasureTime(measureT);
	}	

	
	
	public void addCountBindings(QueryIterator qIter){
		
		addCountBindings(qIter, "");

	}
	
	public void addCountBindings(QueryIterator qIter, String position){
		
		int i = getPosition(position);
		
		if (getEnableCountBindings()){
			ArrayList<Integer> temp ;
			if(countBindings.containsKey(qIter)){
				temp = countBindings.get(qIter);
				Integer tempi = temp.get(i);
				temp.set(i, ++tempi);
				
			}
			else{
				temp = new ArrayList<Integer>();
				for (int j = 0; j<3; j++)
					temp.add(j, 0);
				temp.add(i, 1);
			}
			
			countBindings.put(qIter, temp);
		}

	}
	


	public void setEnableCountBindings(Boolean enableCountBindings) {
		this.enableCountBindings = enableCountBindings;
	}

	public Boolean getEnableCountBindings() {
		return enableCountBindings;
	}


	public void setEnableMeasureTime(Boolean enableMeasureTime) {
		this.enableMeasureTime = enableMeasureTime;
	}

	public Boolean getEnableMeasureTime() {
		return enableMeasureTime;
	}
	
	public HashMap<QueryIterator, ArrayList<Integer>> getCountBindings() {
		return countBindings;
	}
	
	public Integer getCountBindings(QueryIterator qIter){
		return getCountBindings(qIter, "");
	}
	
	public Integer getCountBindings(QueryIterator qIter, String position) {
		
		int i = getPosition(position);
		Integer result = 0;
		
		List<Integer> temp = countBindings.get(qIter);
		if( temp!= null && !temp.isEmpty()){
			result = temp.get(i);
			if (result == null) result = 0;
		}
		
		return result;
	}

	
	public void printCountBindings(QueryIterator qIter){
		printCountBindings(qIter, "");
	}
	
	public void printCountBindings(QueryIterator qIter, String position){
		if(getEnableCountBindings()){
			System.out.println( qIter  + " " + position + " - counted bindings: " + getCountBindings(qIter, position) );
		}
	}
	
	//UTILS

	public int getPosition (String position){
		if (position.equals("input")){	return INPUT;}
		else if (position.equals("left")){	return LEFT;}
		else if (position.equals("right")){	return RIGHT;}
		else return 1;
	}

	
	public void printReports(QueryIterator qIter){
			System.out.println(" ");
			System.out.println("In "+ qIter + ": ");
			printCountBindings(qIter);
			
			if(qIter instanceof QueryIter1){
				printCountBindings(qIter, "input");
			}
			else if (qIter instanceof QueryIter2){
				printCountBindings(qIter, "left");
				printCountBindings(qIter, "right");
			}
	}


}
