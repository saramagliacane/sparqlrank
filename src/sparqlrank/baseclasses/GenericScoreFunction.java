package sparqlrank.baseclasses;

import java.util.ArrayList;
import java.util.List;

import com.hp.hpl.jena.sparql.ARQNotImplemented;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.expr.Expr;
import com.hp.hpl.jena.sparql.expr.ExprFunction2;
import com.hp.hpl.jena.sparql.expr.ExprFunction3;
import com.hp.hpl.jena.sparql.expr.ExprVar;
import com.hp.hpl.jena.sparql.expr.NodeValue;
import com.hp.hpl.jena.sparql.function.FunctionEnv;

/**
 * Class modelling a generic score function with an arbitrary expression.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 *
 */
public class GenericScoreFunction extends ScoreFunctionBase implements
		ScoreFunction {

	// Expression evaluated by the ScoreFunction/
	private Expr expr;
	
	// Artificial delay introduced by each evaluation of the ScoreFunction - not yet used
	private static int delay;
	
	public GenericScoreFunction(PredicateSet predicates, Expr expr, int del) {
		super(predicates);
		this.expr = expr;
		delay = del;
	}
	
	public GenericScoreFunction(PredicateSet predicates, Expr expr) {
		this(predicates, expr, 0);
	}

	@Override
	public Score combine(Binding left, Binding right,
			PredicateSet leftpredicateset, PredicateSet rightpredicateset,
			FunctionEnv execCtx) {
		  PredicateSet leftpureset = PredicateSet.removeAllPredicates ( leftpredicateset, rightpredicateset);
		  PredicateSet rightpureset = PredicateSet.removeAllPredicates ( rightpredicateset, leftpredicateset);
		  PredicateSet intersectionset = PredicateSet.intersection ( leftpredicateset, rightpredicateset);
		  
		  PredicateSet restset = new PredicateSet();
		  restset.addAll(getPredicates());
		  restset.removeAllPredicates(leftpredicateset);
		  restset.removeAllPredicates(rightpredicateset);
		  
		  return combine(left, right, leftpureset, rightpureset, intersectionset, restset, execCtx);
	}

	@Override
	public Score combine(Binding left, Binding right, PredicateSet leftpureset,
			PredicateSet rightpureset, PredicateSet intersectionset,
			PredicateSet restset, FunctionEnv execCtx) {
		 
		delay();
		
		List<Score> scoreList = new ArrayList<Score>();
		NodeValue nv = NodeValue.makeDouble(0);
		
		  
		// for the left binding calculate its contribution to the score
		for (Predicate p: leftpureset){
			scoreList.add(p.eval(left,execCtx));
		}
		
		 // symmetrically for the right binding
		for (Predicate p: rightpureset){
			scoreList.add(p.eval(right,execCtx));
		}
		
		// in the intersection of predicates, get the maximum contribution
		for (Predicate p: intersectionset){
			Score leftScore  = p.eval(left,execCtx);
			Score rightScore = p.eval(right,execCtx);
			  
			scoreList.add( Score.max(leftScore, rightScore));

		}
		

		// if there are non evaluated predicates
		if(restset!= null){
			int x = restset.size();
			if(x!=0) scoreList.add(new Score(x));
		}
		
		
		// evaluate the ScoreFunction
		
		if (expr.isConstant()){
			nv = expr.getConstant();
		}		
		else if (expr instanceof ExprVar){
			nv = NodeValue.makeDouble(scoreList.get(0).getDouble());
		}		
		else if (expr instanceof ExprFunction2){
			for (int i = 0; i< scoreList.size(); i++)
				nv = ((ExprFunction2) expr).eval(NodeValue.makeDouble(scoreList.get(i).getDouble()), nv);
		}
			
		else if (expr instanceof ExprFunction3){
			for (int i = 0; i< scoreList.size(); i++)
				nv = ((ExprFunction3) expr).eval(NodeValue.makeDouble(scoreList.get(i).getDouble()), NodeValue.makeDouble(scoreList.get(++i).getDouble()),  nv);
		}
		else {
			throw new ARQNotImplemented() ;
		}
		
		Score score = new Score(nv.getDouble());
 
		return score;
	}



	@Override
	@Deprecated
	public Score evaluate(Binding binding, PredicateSet P, FunctionEnv execCtx) {
	    PredicateSet temppred = new PredicateSet();
	    temppred.addAll(getPredicates());
	    temppred.removeAllPredicates(P);
	    return evaluate(binding, P, temppred, execCtx);
	}

	@Override
	public Score evaluate(Binding binding, PredicateSet P,
			PredicateSet restset, FunctionEnv execCtx) {
		
		delay();
		
		List<Score> temp = new ArrayList<Score>();	
		NodeValue nv ;
		
	    //System.out.println("Eval " + binding);
		for (Predicate p: P){
			temp.add(p.eval(binding,execCtx));
			 //System.out.println("Predicate used in ordering: " + p + " " + p.getExpr().getVarName());
		}
		
		
//		for (Predicate p: restset){
//			temp.add(new Score(1.0));
//			System.out.println("Predicate not used in ordering: " + p + " " + p.getExpr().getVarName());
//		}
		
		if(restset!= null){
			int x = restset.size();
			if(x!=0) temp.add(new Score(x));
		}
				
		
		//EVALUATE THE EXPRESSION:
		if (expr.isConstant()){
			nv = expr.getConstant();
		}		
		else if (expr instanceof ExprVar){
			nv = NodeValue.makeDouble(temp.get(0).getDouble());
		}		
		else if (expr instanceof ExprFunction2){
			nv = NodeValue.makeDouble(temp.get(0).getDouble());
			for (int i = 1; i< temp.size(); i++)
				nv = ((ExprFunction2) expr).eval(NodeValue.makeDouble(temp.get(i).getDouble()), nv);
			
		}
			
		else if (expr instanceof ExprFunction3){
			nv = NodeValue.makeDouble(temp.get(0).getDouble());
			for (int i = 1; i< temp.size(); i++)
				nv = ((ExprFunction3) expr).eval(NodeValue.makeDouble(temp.get(i).getDouble()), NodeValue.makeDouble(temp.get(++i).getDouble()),  nv);
			
		}
		else {
			throw new ARQNotImplemented() ;
		}
		
		Score score = new Score(nv.getDouble());
		
	    return score;
	}
	
	@Override
	public ScoreFunction copy() {
		return new GenericScoreFunction(getPredicates(), expr);
	}

	
	private void delay(){
		if (delay>0)
			try {
				//System.out.println("sleeping");
				Thread.sleep(delay);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	}
}
