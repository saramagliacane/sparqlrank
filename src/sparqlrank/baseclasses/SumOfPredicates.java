package sparqlrank.baseclasses;

import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.function.FunctionEnv;

/**
 * Implementation of the ScoreFunction as sum of the evaluation of a number of {@link Predicate} objects.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class SumOfPredicates extends ScoreFunctionBase implements ScoreFunction{
	
	
	public SumOfPredicates(PredicateSet predicates){
		super(predicates);
	}
	
	 @Override
	 public Score evaluate(Binding binding, PredicateSet P, PredicateSet restset, FunctionEnv execCtx)
	  {
	    Score score = new Score();
	    // standard RankSQL approach

	    //System.out.println("Eval " + binding);
		for (Predicate p: P){
			score.add(p.eval(binding,execCtx));
			 //System.out.println("Predicate used in ordering: " + p + " " + p.getExpr().getVarName());
		}
		
		if(restset!= null)
			score.add(restset.size());
	
	    return score;
	  }
	
	 
	 
	@Deprecated
	// too many PredicateSet instantiated
	 public Score evaluate(Binding binding, PredicateSet P, FunctionEnv execCtx)
	  {
	    PredicateSet temppred = new PredicateSet();
	    temppred.addAll(getPredicates());
	    temppred.removeAllPredicates(P);
	    return evaluate(binding, P, temppred, execCtx);
	  }
	

	// used by QueryIterRankJoin
	@Override
	public Score combine(Binding left, Binding right, PredicateSet leftpureset, PredicateSet rightpureset, PredicateSet intersectionset, PredicateSet restset, FunctionEnv execCtx){
		  
		  Score score = new Score();
		  
		  // for the left binding calculate its contribution to the score
		  for (Predicate p: leftpureset){
			  score.add(p.eval(left,execCtx));
		  }
		  
		  // symmetrically for the right binding
		  for (Predicate p: rightpureset){
			  score.add(p.eval(right,execCtx));
		  }
		  

		  // in the intersection of predicates, get the maximum contribution
		  for (Predicate p: intersectionset){
			  Score leftScore  = p.eval(left,execCtx);
			  Score rightScore = p.eval(right,execCtx);
			  
			  score.add(Score.max(leftScore, rightScore));

		  }
		  
		  // the standard RankSQL approach - assign 1 for all unevaluated predicates
		  score.add(restset.size());
	    
		  return score;
	  }

	
	@Deprecated
	// too many PredicateSet instantiated
	public Score combine(Binding left, Binding right, PredicateSet leftpredicateset, PredicateSet rightpredicateset, FunctionEnv execCtx){

		  
		  PredicateSet leftpureset = PredicateSet.removeAllPredicates ( leftpredicateset, rightpredicateset);
		  PredicateSet rightpureset = PredicateSet.removeAllPredicates ( rightpredicateset, leftpredicateset);
		  PredicateSet intersectionset = PredicateSet.intersection ( leftpredicateset, rightpredicateset);
		  
		  PredicateSet restset = new PredicateSet();
		  restset.addAll(getPredicates());
		  restset.removeAllPredicates(leftpredicateset);
		  restset.removeAllPredicates(rightpredicateset);
		  
		  return combine(left, right, leftpureset, rightpureset, intersectionset, restset, execCtx);

	  }


	  public ScoreFunction copy(){
		  return new SumOfPredicates (this.getPredicates());
	  }
	  

}