package sparqlrank.baseclasses;

import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.sparql.expr.E_Add;
import com.hp.hpl.jena.sparql.expr.Expr;
import com.hp.hpl.jena.sparql.expr.ExprEvalTypeException;
import com.hp.hpl.jena.sparql.expr.NodeValue;
import com.hp.hpl.jena.sparql.expr.nodevalue.XSDFuncOp;

/**
 * Toy expression to check if the {@link GenericScoreFunction} works. Not really used.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class TestExpr extends E_Add {

	
	public TestExpr(Expr left, Expr right) {
		 super(left, right) ;
	}
	
    @Override
    public NodeValue eval(NodeValue x, NodeValue y)
    {
        if ( ARQ.isStrictMode() )
            return XSDFuncOp.add(x, y) ;

        if ( x.isString() && y.isString() )
            return NodeValue.makeString(x.asString()+y.asString()) ;
        if ( ! x.isNumber() ||  ! y.isNumber() )
            throw new ExprEvalTypeException("Operator '+' requires two numbers or two strings: got: "+x+" and "+y) ;
            
        return XSDFuncOp.sqrt(XSDFuncOp.add(x, y)) ;
    }

    @Override
    public Expr copy(Expr e1, Expr e2) {  return new TestExpr(e1 , e2 ) ; }

}
