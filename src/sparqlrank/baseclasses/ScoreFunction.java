package sparqlrank.baseclasses;

import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.function.FunctionEnv;

/**
 * Interface for the ScoreFunction - it contains the methods for evaluating a score of a specific Binding and to combine the scores
 * of two separate Bindings (useful for calculating the Thresholds in several algorithms).
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public interface ScoreFunction {

	@Deprecated
	public Score combine(Binding left, Binding right, PredicateSet leftpredicateset, PredicateSet rightpredicateset, FunctionEnv execCtx);
	
	public Score combine(Binding left, Binding right, PredicateSet leftpureset, PredicateSet rightpureset, PredicateSet intersectionset, PredicateSet restset, FunctionEnv execCtx);
	
	@Deprecated
	public Score evaluate(Binding binding, PredicateSet P, FunctionEnv env);
	
	public Score evaluate(Binding binding, PredicateSet P, PredicateSet restset, FunctionEnv execCtx);
	  
	
	public PredicateSet getPredicates();
	
	public void close ();
	
	public ScoreFunction copy();
	
	public ScoreFunction addPredicate (Predicate p);

	public ScoreFunction removePredicate (Predicate p);


}
