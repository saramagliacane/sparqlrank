package sparqlrank.baseclasses;

/**
 * Abstract class that contains the base of a {@link ScoreFunction}
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 *
 */


public abstract class ScoreFunctionBase implements ScoreFunction {
	
	private PredicateSet predicates = new PredicateSet();

	public ScoreFunctionBase(PredicateSet predicates){
		this.predicates = predicates;
	}
	
	@Override
	public PredicateSet getPredicates() {
		return predicates;
	}

	public ScoreFunction addPredicate (Predicate p){
		  predicates.add(p);
		  return this;
	}


	public ScoreFunction removePredicate (Predicate p){
		predicates.remove(p);
		return this;
	}
	
	@Override
	@Deprecated
	 public void close (){
		  predicates = null;
	  }

}
