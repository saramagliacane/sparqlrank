package sparqlrank.baseclasses;

import com.hp.hpl.jena.sparql.expr.ExprEvalException;

/**
 * Exception to be called in case the {@link Predicate} has a value outside the [0,1] interval.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 *
 */

public class UncorrectPredicateValuesException extends ExprEvalException {

	private static final long serialVersionUID = 1L;


}
