package sparqlrank;

import com.hp.hpl.jena.query.ARQ;

/**
 * Helper class to enable execution timing in Joseki, not debugged, possibly not working.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class EnableExecutionTiming {

static{	
	ARQ.getContext().set(ARQ.enableExecutionTimeLogging, true);
}
}