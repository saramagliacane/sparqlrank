/*
 * @(#)ReportingDriver.java   1.0   05/dic/2011
 *
 * Copyright 2011-2011 Politecnico di Milano. All Rights Reserved.
 *
 * This software is the proprietary information of Politecnico di Milano.
 * Use is subject to license terms.
 *
 * @(#) $Id$
 */
package sparqlrank;


/**
 * Class performing a reporting experiment: tree generation, binding counting ecc.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class ReportingDriver {

   
   public static void main(final String[] args){

      // create an object TestParameters, that contains all the set parameters for each test
      // in case a parameter is not specified, TestParameter contains the default values
      final TestParameters param = new TestParameters(args);
      
      TimingDriver.createExperimentsDirectories(param);
      
      setupReportingExperiments( param);
   }
   
   public static void setupReportingExperiments(final TestParameters param){
      param.setRepeatStandard(1);
      
      //first time is in case we have to full the cache
      param.setRepeatModified(2);
      param.setReporting(true);
      param.setCaching(false);
      param.setTiming(false);
      
      TimingDriver.setupTimingExperiments(param);
      
   }
}
