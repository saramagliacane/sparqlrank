/*
 * @(#)QueryIterRankJoinRandom.java   1.0   15/nov/2011
 *
 * Copyright 2011-2011 Politecnico di Milano. All Rights Reserved.
 *
 * This software is the proprietary information of Politecnico di Milano.
 * Use is subject to license terms.
 *
 * @(#) $Id$
 */
package sparqlrank.iterators;


   import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.graph.Graph;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.sparql.ARQInternalErrorException;
import com.hp.hpl.jena.sparql.core.BasicPattern;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.binding.BindingMap;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIter;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIter2;
import com.hp.hpl.jena.sparql.util.Utils;
import com.hp.hpl.jena.util.iterator.ClosableIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.util.iterator.NiceIterator;
import com.hp.hpl.jena.util.iterator.WrappedIterator;

import org.openjena.atlas.io.IndentedWriter;

import sparqlrank.baseclasses.PredicateSet;
import sparqlrank.baseclasses.Score;
import sparqlrank.baseclasses.ScoreFunction;

   /** 
    * Implementation of a ranked Sequence (RankJoin with also random access on BOTH sides)
    * iterator. Used with a single Triple Pattern + Rank on both right and left.
    * 
    * Extension: use with BGP +Rank on both left and right - ass. the last triple is the most restrictive (to enforce in algebraic tree generation)
    * @author saramagliacane
    *
    */
   

   public class QueryIterRankJoinRandom extends QueryIter2{
         
        private final Boolean enableLogging = false;
      
         // queue for outcoming results
         private RankedQueue queue;

         // used to calculate combine scores
         private PredicateSet leftpureset;
         private PredicateSet rightpureset;
         private PredicateSet intersectionset;
         private PredicateSet restset;
           
         private final String leftpredicateNames;
         private final String rightpredicateNames;
         
         private ScoreFunction scorefunction;
         private Score Threshold;
         
         // We need two hash tables to contain all the mappings we've seen till now
         // from both the inputs
         //private RankJoinHashTable leftHashTable = new RankJoinHashTable();
         //private RankJoinHashTable rightHashTable = new RankJoinHashTable();
         private Triple rightTriplePattern;
         private Triple leftTriplePattern;
         
         private BasicPattern rightPattern;
         private BasicPattern leftPattern;
         
         
         
         private Boolean isFirstMapping = true;
         private Boolean chooseLeftInput = true;
         private Boolean exhaustedInputs = false;
         
         // pre-fetched next Binding - needed to answer if the operator hasNext()
         private Binding nextBinding = null ;

         private Binding bottomLeftBinding, bottomRightBinding ;      
         
         
         private Set<Binding> lastSeenSameScoreBindings = new HashSet<Binding>();
         private Score lastSeenScore = new Score();
         

        
         

         public QueryIterRankJoinRandom(final QueryIterator left, final QueryIterator right,
               final ScoreFunction scoreFunction, final PredicateSet l, final PredicateSet r, final ExecutionContext execCtx) 
         {
            super(left, right, execCtx);
            this.scorefunction = scoreFunction;

            // TODO: move the calculation to OpRankJoin?
            // performed here so we don't need to calculate it in each call to ScoreFunction.combine(..)
            leftpureset = PredicateSet.removeAllPredicates ( l, r);
            rightpureset = PredicateSet.removeAllPredicates ( r, l);
            intersectionset = PredicateSet.intersection ( l, r);
            
            leftpredicateNames = l.toString();
            rightpredicateNames = r.toString();
            
            // union of the left and right PredicateSets
            final PredicateSet sumOfLeftAndRightPredicateSet = new PredicateSet();
            sumOfLeftAndRightPredicateSet.addAll(l);
            sumOfLeftAndRightPredicateSet.addAll(r);
            
            restset = new PredicateSet();
            restset.addAll(scorefunction.getPredicates());
            restset.removeAllPredicates(l);
            restset.removeAllPredicates(r);
            
            queue = RankedQueue.create(sumOfLeftAndRightPredicateSet, restset, scorefunction);
             
            try{ 
               rightTriplePattern = ((QueryIterRank) right).getSubpattern().getList().get(0);
               leftTriplePattern = ((QueryIterRank) left).getSubpattern().getList().get(0);
               rightPattern = ((QueryIterRank) right).getSubpattern();
               leftPattern = ((QueryIterRank) left).getSubpattern();
               

               
               
               
            } catch(final Exception e ){ 
               System.out.println("ERROR: Not supposed to have RankSequence here.");
               System.exit(-1);
            }
            
            
         }


         //TODO: check how to use
         protected void releaseResources() {
            
            
            getLeft().close();
            getRight().close();
            
//            leftHashTable.close();
//            leftHashTable = null;
//          rightHashTable.close();
//          rightHashTable = null;
            queue = null;

            leftpureset = null;
            rightpureset = null;
            intersectionset = null;
            restset = null;
            
            scorefunction = null;
            Threshold = null;
            nextBinding = null ;
            bottomLeftBinding = null; 
            bottomRightBinding = null;
            
            
         }

         
         
         @Override
         protected boolean hasNextBinding() {
            // if we don't have even the first result from each of the inputs
            if ( (!getLeft().hasNext()||!getRight().hasNext()) && isFirstMapping)
                   return false ;
            
            if(isFinished())
               return false;
            
            if ( nextBinding != null )
                 return true ;

             nextBinding = moveToNext() ;
             return ( nextBinding != null ) ;
            
         }

         
          @Override
          protected Binding moveToNextBinding()
          {
              if ( nextBinding == null )
                  throw new ARQInternalErrorException("moveToNextBinding: slot empty but hasNext was true)") ;
              
              final Binding b = nextBinding ;
              nextBinding = null ;
              return b ;
          }
         

          // do the real work
         protected Binding moveToNext() {
            
            Binding result = getQueueResults();
            if (result!= null) return result;
            
            while(true){
               Binding b, leftBinding, rightBinding;
                  
               if (isFirstMapping){
                  
                  // PHASE 0 - get Left
                  leftBinding = getLeft().next();
                  bottomLeftBinding = leftBinding;
                  

                  // we shouldn't need to check if it hasNext(), we already did in the hasNextBinding
                  matchBinding(leftBinding, true);


                  // PHASE 1- get Right
                  rightBinding = getRight().next();
                  bottomRightBinding = rightBinding;
                  
               
                  matchBinding(rightBinding, false);
                  

                  Threshold = scorefunction.combine(bottomLeftBinding, bottomRightBinding, leftpureset, rightpureset, intersectionset, restset, this.getExecContext());
                  
                  
                  isFirstMapping = false;
               }
               
                           
               if (! getInput().hasNext() && !exhaustedInputs){
                  
                  // if one input is exhausted, switch to the other and check if also the other has finished
                  forceSwitching();
                  
                  // both inputs exhausted
                  if (exhaustedInputs){
                     // let everything from queue
                     Threshold = Score.immutableZeroScore;
                  }
                  

               }
               else if (!exhaustedInputs){
            
                  // decide how to switch between left and right input
                  switchingPolicy();         
                  b = getInput().next();  

                  setBottomBinding(b);
                  
                  matchBinding(b, this.chooseLeftInput);
                  calculateThreshold();
                     

                     
               }
               else {
                  if(queue.isEmpty()) return null;
               }
         
               
               result = getQueueResults();
               if (result!= null) return result;
               

                  
            }

         }
        
         
         

         private void matchBinding(final Binding leftBinding, final Boolean isLeftBinding) {

            final BGPMapper BGPMapper;
            
            if(isLeftBinding){
               BGPMapper = new BGPMapper(rightPattern, leftBinding, this.getExecContext(), isFirstMapping);
               if(isFirstMapping){
                  rightPattern = BGPMapper.finalpattern;
               }
            }
            else {
               BGPMapper = new BGPMapper(leftPattern, leftBinding, this.getExecContext(), isFirstMapping);
               if(isFirstMapping){
                  leftPattern = BGPMapper.finalpattern;
               }
            }
            

               
            
            final List<Binding> templist = BGPMapper.findMatchingBinding(leftBinding);
            

            for(final Binding b: templist){
               //System.out.println("matchLeftBinding: " + rightBinding +   "-" + maxLeftScore.getDouble());
               queue.add(b, Threshold);
               
            }
            
         }
         


         
         


         private void matchBindingOriginal(final Binding leftBinding, final Boolean isLeftBinding) {

            final TripleMapper rightTripleMapper;
            
            if(isLeftBinding){
               rightTripleMapper = new TripleMapper(leftBinding, rightTriplePattern, this.getExecContext());
           }
            else {
               rightTripleMapper = new TripleMapper(leftBinding, leftTriplePattern, this.getExecContext());
            }
            
            Binding rightBinding;
            

            while(rightTripleMapper.hasNext()){
               rightBinding = rightTripleMapper.next();
               //System.out.println("matchLeftBinding: " + rightBinding +   "-" + maxLeftScore.getDouble());
               queue.add(rightBinding, Threshold);
               
            }
            
         }
         
         

         protected Binding getQueueResults(){
            if (!queue.isEmpty()){
               final Score topScore = queue.getTopScore(this.getExecContext());
               if(Score.greaterThan(topScore, Threshold)){
                  final Binding outputBinding = queue.remove();
                  Binding result = outputBinding;
                  // we have all the elements we need to define a final score
                  if (restset.isEmpty()){
                     final Var score = Var.alloc("score");
                     final Node nv = outputBinding.get(score);
                     if (nv == null){
                        final Score s = scorefunction.evaluate(outputBinding, scorefunction.getPredicates(), restset, this.getExecContext());
                        //TODO: remove ugly hack
                        final Node node = Node.createLiteral(Double.toString(s.getDouble()) + "e0", null, XSDDatatype.XSDdouble);
                        result = new BindingMap(outputBinding);
                        result.add(score, node);
                        //System.out.println(result);
                     }
                     
                  }
                  
                  
                  //eliminate duplicates
                  if(!lastSeenScore.equals(topScore)){
                     lastSeenScore = topScore;
                     lastSeenSameScoreBindings = new HashSet<Binding>();
                     
                  }
            
                  Boolean duplicate = false;  
                  
                  
                  for (final Binding b1: lastSeenSameScoreBindings){
                    
                     final Iterator<Var> vars = result.vars(); 
                     Boolean sameBinding = true;
                     
                     while (vars.hasNext()){
                        final Var v = vars.next();
                        
                        final Node nodeb = result.get(v);
                        final Node nodeb1 = b1.get(v);
                        
                        String valueb;
                        String valueb1;
                        
                        if(nodeb.isLiteral()){
                           // TODO: now we skip equality on literals
                           continue;
                        }
                        else{
                           valueb = nodeb.toString();
                           valueb1 = nodeb1.toString();
                        }
                      
                        
                        if(valueb!=valueb1){
                           
                           //System.out.println("#Different var: "+ v +" values: "+ valueb + "," + valueb1 );  
                           sameBinding = false;
                           break;
                        }
                        else{
                           //System.out.println("#Same var: "+ v +" values: "+ valueb + "," + valueb1 );  
                           
                        }
                        
                     }
                     
                     if (sameBinding){
                        
                        if (enableLogging) System.out.println("#Removed Duplicate: "+ result );                  
                        duplicate = true;
                        break;
                     }
                        
                    
     
                  }
                  
                  lastSeenSameScoreBindings.add(result);
                  
                  if(duplicate) return null;
                  
                  return result;
               }
                  
            }
         
            return null;
         }
         
          protected QueryIterator getInput()   { 
             if (chooseLeftInput) return getLeft();
             else return getRight();
         } 
         
         
         
         protected void setBottomBinding(final Binding b){
             if (chooseLeftInput)   bottomLeftBinding = b;
             else                bottomRightBinding = b;
            
         }
          
         
         
         protected void switchingPolicy(){

            roundRobin();
            //narrowTheGapHeuristic();
            
            // don't switch to empty
            if (chooseLeftInput && !getLeft().hasNext()) chooseLeftInput = false;
            else if (!chooseLeftInput && !getRight().hasNext()) chooseLeftInput = true;
            
         }
         
         protected void roundRobin(){
             // trivial switching policy: ROUND ROBIN     
            chooseLeftInput = !chooseLeftInput;
         }
         
//       protected void narrowTheGapHeuristic(){
//          // select the input with the lower threshold
//          
//          if(maxLeftScore.getDouble() > maxRightScore.getDouble()){
//             chooseLeftInput = false;
//          }
//          else 
//             chooseLeftInput = true;
//          
//       }
         
         protected void  forceSwitching(){
            // if we are here, it's because one input is exhausted
            chooseLeftInput = !chooseLeftInput;
            // the first two conditions mean that both inputs are exhausted
            if (chooseLeftInput && !getLeft().hasNext()) exhaustedInputs = true;
            else if (!chooseLeftInput && !getRight().hasNext()) exhaustedInputs = true;
            else exhaustedInputs = false;
            
         }
         
         protected void calculateThreshold(){

            //each bottomBinding changes just once, we can half the number of calls to combine
            Threshold = scorefunction.combine(bottomLeftBinding, bottomRightBinding, leftpureset, rightpureset, intersectionset, restset, this.getExecContext());
           
            
         }
         
         

         @Override
         public void output(final IndentedWriter out) {
              out.print("(") ;
              out.print(Utils.className(this)) ;
              out.print(" ");
              out.print("[" + leftpredicateNames + " ," + rightpredicateNames  + "]");
              out.print(")") ;
         }



         @Override
         protected void closeSubIterator() {
            // TODO Auto-generated method stub
            
         }



         @Override
         protected void requestSubCancel() {
            // TODO Auto-generated method stub
            
         }
         
         
         
         
         
         
         /////// From QueryIterTriplePattern.java
         
          public static class TripleMapper extends QueryIter
          {
              private final Node s ;
              private final Node p ;
              private final Node o ;
              private final Binding binding ;
              private ClosableIterator<Triple> graphIter ;
              private Binding slot = null ;
              private boolean finished = false ;
              private volatile boolean cancelled = false ;

              TripleMapper(final Binding binding, final Triple pattern, final ExecutionContext cxt)
              {
                  super(cxt) ;
                  this.s = substitute(pattern.getSubject(), binding) ;
                  this.p = substitute(pattern.getPredicate(), binding) ;
                  this.o = substitute(pattern.getObject(), binding) ;
                  this.binding = binding ;
                  final Node s2 = tripleNode(s) ;
                  final Node p2 = tripleNode(p) ;
                  final Node o2 = tripleNode(o) ;
                  final Graph graph = cxt.getActiveGraph() ;
                  
                  final ExtendedIterator<Triple> iter = graph.find(s2, p2, o2) ;
                  
                  if ( false )
                  {
                      // Materialize the results now. Debugging only.
                      final List<Triple> x = iter.toList() ;
                      this.graphIter = WrappedIterator.create(x.iterator()) ;
                      iter.close();
                  }
                  else
                      // Stream.
                      this.graphIter = iter ;
              }

              private static Node tripleNode(final Node node)
              {
                  if ( node.isVariable() )
                      return Node.ANY ;
                  return node ;
              }

              private static Node substitute(final Node node, final Binding binding)
              {
                  if ( Var.isVar(node) )
                  {
                      final Node x = binding.get(Var.alloc(node)) ;
                      if ( x != null )
                          return x ;
                  }
                  return node ;
              }

              private Binding mapper(final Triple r)
              {
                  final Binding results = new BindingMap(binding) ;

                  if ( ! insert(s, r.getSubject(), results) )
                      return null ; 
                  if ( ! insert(p, r.getPredicate(), results) )
                      return null ;
                  if ( ! insert(o, r.getObject(), results) )
                      return null ;
                  return results ;
              }

              private static boolean insert(final Node inputNode, final Node outputNode, final Binding results)
              {
                  if ( ! Var.isVar(inputNode) )
                      return true ;
                  
                  final Var v = Var.alloc(inputNode) ;
                  final Node x = results.get(v) ;
                  if ( x != null )
                      return outputNode.equals(x) ;
                  
                  results.add(v, outputNode) ;
                  return true ;
              }
              
              @Override
              protected boolean hasNextBinding()
              {
                  if ( finished ) return false ;
                  if ( slot != null ) return true ;
                  if ( cancelled )
                  {
                      graphIter.close() ;
                      finished = true ;
                      return false ;
                  }

                  while(graphIter.hasNext() && slot == null )
                  {
                      final Triple t = graphIter.next() ;
                      slot = mapper(t) ;
                  }
                  if ( slot == null )
                      finished = true ;
                  return slot != null ;
              }

              @Override
              protected Binding moveToNextBinding()
              {
                  if ( ! hasNextBinding() ) 
                      throw new ARQInternalErrorException() ;
                  final Binding r = slot ;
                  slot = null ;
                  return r ;
              }

              @Override
              protected void closeIterator()
              {
                  if ( graphIter != null )
                      NiceIterator.close(graphIter) ;
                  graphIter = null ;
              }
              
              @Override
              protected void requestCancel()
              {
                  // The QuryIteratorBase machinary will do the real work.
                  // but we cleanly kill the ExtendedIterator.
                  cancelled = true ;
              }
          }
          
          
          public static class BGPMapper 
          {

             BasicPattern finalpattern;
             ExecutionContext cxt;
             List<Binding> buffer;
            

            BGPMapper( final BasicPattern pattern, final Binding binding, final ExecutionContext cxt, final boolean firstTime)
              {
                  this.cxt = cxt;
                 finalpattern = new BasicPattern();

                 if(firstTime && pattern.getList().size()>1){
                    

                    //TODO reorder based on pattern
                    final HashMap<Integer, List<Triple>> triplePatternByRelevance = new HashMap<Integer, List<Triple>>();
                    
                    triplePatternByRelevance.put(0, new LinkedList<Triple>());
                    triplePatternByRelevance.put(1, new LinkedList<Triple>());
                    triplePatternByRelevance.put(2, new LinkedList<Triple>());
                    
                    for( final Triple t: pattern){
                       int key = 0;
                       final Triple temp = new Triple (substitute(t.getSubject(), binding), substitute(t.getPredicate(), binding), 
                             substitute(t.getObject(), binding)) ;
                       
                       if(Var.isVar(temp.getObject())){
                          key++;
                       }
                       if(Var.isVar(temp.getSubject())){
                          key++;
                       }
   //                    if(Var.isVar(temp.getPredicate())){
   //                       key++;
   //                    }
                       final List<Triple> triplelist = (triplePatternByRelevance.get(key));
                       triplelist.add(t);
                       triplePatternByRelevance.put(key, triplelist);
                       
                    }

                    
   
                    for(final Triple t: triplePatternByRelevance.get(0)){
                       finalpattern.add(t);
 
                    }

                    //TODO check for other bridge patterns in a greedy way
//                    for(final Triple t: triplePatternByRelevance.get(0)){
//                       for(final Triple t2: triplePatternByRelevance.get(1)){
//                          finalpattern.add(t);
//    
//                       }
// 
//                    }
                    
                    
                    for(final Triple t: triplePatternByRelevance.get(1)){
                       finalpattern.add(t);
                    }

                    for(final Triple t: triplePatternByRelevance.get(2)){
                       finalpattern.add(t);
                    }
                    
                    //System.out.println("is first time: reordered "+ pattern + " to " + finalpattern);
                    
                 }

                 else{
                    finalpattern = pattern;
                 }

                  
                  
                  

              }



              private static Node substitute(final Node node, final Binding binding)
              {
                  if ( Var.isVar(node) )
                  {
                      final Node x = binding.get(Var.alloc(node)) ;
                      if ( x != null )
                          return x ;
                  }
                  return node ;
              }

              public  List<Binding> findMatchingBinding(final Binding binding){
                 return  findMatchingBinding(binding, 0);
                 
              }

              public  List<Binding> findMatchingBinding(final Binding binding, final int counter){
                 
                 if(counter >= finalpattern.size()){
                    return null;
                 } 
                    
                 final TripleMapper tripleMapper = new TripleMapper(binding, finalpattern.getList().get( counter ), cxt);
                 Binding b;

                 final List<Binding> list = new LinkedList<Binding>();
                 
                 while(tripleMapper.hasNext()){
                    /// for each binding matching the first triple pattern
                    b = tripleMapper.next();
                    // get the list of mappings matching the other patterns
                    
                    final List<Binding> templist = findMatchingBinding(b, counter+1);
                    if (templist == null || templist.isEmpty()){
                       list.add(b);
                       continue;
                    }   
                    list.addAll(templist);
                    //merge them- should be merged
                    
                    
                 }
                 
                 return list;
                 
     
              }
              
          }
         
              

           
      }

