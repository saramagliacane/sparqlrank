package sparqlrank.iterators;


import java.util.List;
import java.util.PriorityQueue;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.query.SortCondition;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.binding.BindingComparator;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIter1;
import com.hp.hpl.jena.sparql.util.Utils;

import org.openjena.atlas.io.IndentedWriter;

/** 
 * Iterator that handles additional order
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class QueryIterTieBreaker extends QueryIter1{
	
   
   private final List<SortCondition> conditions = null;
   private final Var score = Var.alloc("score");
   private PriorityQueue<Binding> buffer = null;
   
   private final  Double bufferScore = null;
   private   Double lastSeenScore = null;
   private  Binding nextBinding = null;
	

	public QueryIterTieBreaker( final QueryIterator iter,  final List<SortCondition> conditions,  final ExecutionContext execCxt)
    {
        super(iter, execCxt) ;
        
        buffer = new PriorityQueue<Binding>(1, new BindingComparator(conditions, execCxt));
				
    }
    
   	

	@Override
   protected Binding moveToNextBinding() { 
		 
	   while(true){ 
   	   if(!buffer.isEmpty()){
   	      return buffer.remove();
   	   }
   	   
   	   if(nextBinding!= null){
   	      buffer.add(nextBinding);
   	      nextBinding = null;
   	   }
   	   
   	   //buffer is empty
   		while (getInput().hasNext()){
   	       
   		   
   		   final Binding input = getInput().nextBinding();
   		   final Node n = input.get(score);
   		   final Double temp = (Double) n.getLiteral().getValue();
   		   
   		   if(temp==lastSeenScore){
   		      buffer.add(input);
   		   }
   		   else{
   		      
   		      //the newly extracted binding has a different score
   		      nextBinding = input;
   		      lastSeenScore = temp;
   		      break;
   		   }
	
   		}//while has input
	   }

	 }
	    
	

	@Override
	protected void closeSubIterator() {
		
		
		getInput().close();

	}

	@Override
	protected boolean hasNextBinding() {
		  if ( ! getInput().hasNext() && buffer.isEmpty() && nextBinding == null)
	          return false ;
		  
		  
	      if ( isFinished() )
	          return false;
	      
		  return true;
	}

	
	@Override
	public void output(final IndentedWriter out) {
        out.print(Utils.className(this)) ;
        out.print(" ");
        out.print("(" + conditions  + " )");
	}



	@Override
   protected void requestSubCancel() {
		// TODO Auto-generated method stub
		
	}
	
}
