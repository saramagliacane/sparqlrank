package sparqlrank.iterators;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.function.FunctionEnv;

import sparqlrank.baseclasses.PredicateSet;
import sparqlrank.baseclasses.Score;
import sparqlrank.baseclasses.ScoreFunction;

/**
 * PriorityQueue which keeps the Bindings in order based on their maximal-possible score
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class RankedQueue extends PriorityQueue<Binding> {


	// two fields to reduce the number of evaluations of the top score
	private Score topScore;
	private Binding topBinding;

	
	// reference to the RankingComparator, so we can access its fields
	// TODO: what about duplicating the fields?
	private final RankingComparator rankingComparator;
	
	private static final long serialVersionUID = 1L;

	public RankedQueue(final Integer i, final RankingComparator c){
		super(i, c);
		rankingComparator = c;
		topScore = null;
		topBinding = null;
	}
	
	
	public static RankedQueue create(final PredicateSet p, final PredicateSet restset, final ScoreFunction s){
		
		return new RankedQueue(100,new RankingComparator(p, restset, s ));
		
	}
	
	public Score getTopScore(final FunctionEnv env){
		
		if (this.isEmpty()) 
			return Score.immutableZeroScore;
		
		if(topScore == null){
			topScore = rankingComparator.getScoreFunction().evaluate(
					this.peek(), rankingComparator.getPredicates(), rankingComparator.getRestSet(), env);
		}
		return topScore;
		
	}
	
	@Override
	public boolean add (final Binding b){
		final Boolean value = super.add(b);
		final Binding newTopBinding = this.peek();
		if ((topBinding == null)||(!topBinding.equals(newTopBinding))){
			topScore = null;
			topBinding = newTopBinding;
		}
		return value;
		
	}
	
	public boolean add (final Binding b, final Score Threshold){
		
		final Score thisScore = rankingComparator.getScoreFunction().evaluate(
				b, rankingComparator.getPredicates(), rankingComparator.getRestSet(), null);
		
	
		if(Score.greater(thisScore, Threshold)){
   		   //System.out.println("#Duplicate: "+ b + "- threshold: "+ Threshold);
   	       return false;
   		}
   	else{
   		 //if the Score is equal to Threshold 
   	    //could be a tie or a duplicate
   	    // we check later
   		 return this.add(b);
	   
   	}


		

		
	}
	
	@Override
	public Binding remove (){
		final Binding b = super.remove();
		final Binding newTopBinding = this.peek();
		if ((topBinding == null)||(!topBinding.equals(newTopBinding))){
			topScore = null;
			topBinding = newTopBinding;
		}
		return b;
		
	}
	
	
	public List<Binding> toList(){
		final List<Binding> list = new ArrayList<Binding>();
		for (final Binding b: this) {
			list.add(b);
		}
		return list;
	}
	
	  @Override
   public String toString(){
	     String temp = "*** Queue *** \n";
	      for (final Binding b: this) {
	         temp += b + "\n";
	      }
	      return temp;
	   }
	
	public Binding[] returnArray(final List<Binding> additionalBindings){



		final Binding[] array = this.toArray(new Binding[this.size() + additionalBindings.size()]);
		
		//System.out.println("<RankedQueue> Array length: "+ (this.size() + additionalBindings.size()) );
		
		int i = this.size();
		for (final Binding b: additionalBindings){
			array[i] = b;
			//System.out.println("Additional bindings" + b);
			i++;
		}
	
		//TODO: check if this is a useless overhead 
		Arrays.sort( array, rankingComparator);
		
//		System.out.println("Queue, predicates:" + rankingComparator.getPredicates() + "restset"+rankingComparator.getRestSet());
//		for (i = 0; i<100; i++){
//			if (i >= array.length -1) 
//				break;
//			System.out.println(i+" array value: "+ array[i]);
//		}
		
		

		return array;
	}
	
	public PredicateSet getPredicateSet(){
		return this.rankingComparator.getPredicates();
	}
	
//	public static void main (String args[]){
//		Predicate p1 = new Predicate(new SortCondition(Var.alloc("?p1"), 0));
//		Predicate p2 =  new Predicate(new SortCondition(Var.alloc("?p2"), 0));
//		HashSet<Predicate> p = new HashSet<Predicate>();
//		p.add(p1); p.add(p2);
//		ScoreFunction s = new SumOfPredicates(p);
//		RankedQueue queue = RankedQueue.create(p, s);
//		Binding b = new BindingMap();
//		b.add(Var.alloc("?p1"), Node.createLiteral("1.0"));
//		b.add(Var.alloc("?p2"), Node.createLiteral("0.2"));
//		queue.add(b);
//		
//	}
	
}
