package sparqlrank.iterators;


import org.openjena.atlas.io.IndentedWriter;
import sparqlrank.baseclasses.Predicate;
import sparqlrank.baseclasses.PredicateSet;
import sparqlrank.baseclasses.ScoreFunction;
import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.query.SortCondition;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.binding.BindingMap;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIter1;
import com.hp.hpl.jena.sparql.expr.NodeValue;
import com.hp.hpl.jena.sparql.util.Utils;

/** Do not consider, abandoned direction 
 * Iterator that assigns a defaultValue to Bindings with uncertain score
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class QueryIterGoodK extends QueryIter1{
	
	private Double defaultValue;
	private Predicate predicateToSubstitute;
	private ScoreFunction scorefunction;
	private Var var;
	
	
    public static QueryIterator create(QueryIterator iter, Predicate predicate, Double value, ScoreFunction scorefunction, ExecutionContext execCxt) 
    	{ 
    		QueryIterGoodK qGoodK = new QueryIterGoodK(iter, predicate, value, scorefunction, execCxt) ; 
    		QueryIterator qIter = qGoodK.initialize(execCxt);
    		
    		return qIter;
    	}
    
	private QueryIterator initialize(ExecutionContext execCxt) {
			
		// new execution model for each queryIterGoodK
		//ExecutionModel exModel = new ExecutionModel(predicateToSubstitute, defaultValue);
		
		SortCondition s = new SortCondition(getVar(), -1);
		Predicate p = new Predicate(s);
		
		// the predicate of the next QueryIterRank has the new exModel
		//should be useless because we modify already the unbound values here
		//Predicate p = new Predicate(s, exModel);
		
		ScoreFunction s1 = scorefunction.copy();
		s1.removePredicate(predicateToSubstitute);
		PredicateSet restOfPredicates = s1.getPredicates();
		s1.addPredicate(p);
		
		//TODO: change!
		QueryIterRank qRank = new QueryIterRank(this, p, s1, restOfPredicates,  null, execCxt);
		
		return qRank;
	}
	
    private Var getVar() { return var;	}

	private QueryIterGoodK(QueryIterator iter, Predicate predicate, Double value, ScoreFunction scorefunction, ExecutionContext execCxt)
    {
        super(iter, execCxt) ;
        this.defaultValue = value;
        this.scorefunction = scorefunction;

				
    }
    
   	
	@Override
	protected Binding moveToNextBinding() { 
		
		Binding input = getInput().nextBinding();
		
	
		Binding b = new BindingMap(input) ;
		
		Double d;
		
		try {
			
			NodeValue n = predicateToSubstitute.getExpr().eval(b, this.getExecContext());
			d = n.getDouble();
		
		} catch (Exception e)
		{
			//INUTILE?! se lo forwardo ai livelli più bassi...
			d = defaultValue;
		}
		
		Node node = Node.createLiteral(Double.toString(d), null, XSDDatatype.XSDdouble);
		b.add(var, node);
		
		
		return b;
	 }
	    
	

	@Override
	protected void closeSubIterator() {
		
		
		getInput().close();
		predicateToSubstitute = null;
		defaultValue = null;
		var = null;
		scorefunction = null;

	}

	@Override
	protected boolean hasNextBinding() {
		  if ( ! getInput().hasNext())
	          return false ;
		  
	      if ( isFinished() )
	          return false;
	      
		  return true;
	}

	
	@Override
	public void output(IndentedWriter out) {
        out.print(Utils.className(this)) ;
        out.print(" ");
        out.print("(" + predicateToSubstitute + " ," + defaultValue + " )");
	}


	protected void requestSubCancel() {
		// TODO Auto-generated method stub
		
	}
	
}
