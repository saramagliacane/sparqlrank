package sparqlrank.iterators;


import org.openjena.atlas.io.IndentedWriter;

import sparqlrank.baseclasses.PredicateSet;
import sparqlrank.baseclasses.Score;
import sparqlrank.baseclasses.ScoreFunction;
import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.sparql.ARQInternalErrorException;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.binding.BindingMap;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIter2;
import com.hp.hpl.jena.sparql.util.Utils;
/**
 * Physical implementation of the OpRankJoin
 * (modified HRJN algorithm)
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class QueryIterRankJoin extends QueryIter2{
	
	// queue for outcoming results
	private RankedQueue queue;

	// used to calculate combine scores
	private PredicateSet leftpureset;
	private PredicateSet rightpureset;
	private PredicateSet intersectionset;
	private PredicateSet restset;
	  
	private String leftpredicateNames;
	private String rightpredicateNames;
	
	private ScoreFunction scorefunction;
	private Score Threshold;
	
	// We need two hash tables to contain all the mappings we've seen till now
	// from both the inputs
	private RankJoinHashTable leftHashTable = new RankJoinHashTable();
	private RankJoinHashTable rightHashTable = new RankJoinHashTable();
	
	private Boolean isFirstMapping = true;
	private Boolean chooseLeftInput = true;
	private Boolean exhaustedInputs = false;
	
	// pre-fetched next Binding - needed to answer if the operator hasNext()
    private Binding nextBinding = null ;

    private Binding topLeftBinding, topRightBinding , bottomLeftBinding, bottomRightBinding ;
    // Wannabe optimization- remembering the Scores
    private Score maxLeftScore, maxRightScore;
        
    //private ReportMaker reportMaker;
    

	public QueryIterRankJoin(QueryIterator left, QueryIterator right,
			ScoreFunction scoreFunction, PredicateSet l, PredicateSet r, ExecutionContext execCtx) 
	{
		super(left, right, execCtx);
		this.scorefunction = scoreFunction;

		// TODO: move the calculation to OpRankJoin?
		// performed here so we don't need to calculate it in each call to ScoreFunction.combine(..)
		leftpureset = PredicateSet.removeAllPredicates ( l, r);
		rightpureset = PredicateSet.removeAllPredicates ( r, l);
		intersectionset = PredicateSet.intersection ( l, r);
		
		leftpredicateNames = l.toString();
		rightpredicateNames = r.toString();
		
		// union of the left and right PredicateSets
		PredicateSet sumOfLeftAndRightPredicateSet = new PredicateSet();
		sumOfLeftAndRightPredicateSet.addAll(l);
		sumOfLeftAndRightPredicateSet.addAll(r);
		
		restset = new PredicateSet();
		restset.addAll(scorefunction.getPredicates());
		restset.removeAllPredicates(l);
		restset.removeAllPredicates(r);
		
		queue = RankedQueue.create(sumOfLeftAndRightPredicateSet, restset, scorefunction);
		
		
	}


	//TODO: check how to use
	protected void releaseResources() {
		
		
		getLeft().close();
		getRight().close();
		
		leftHashTable.close();
		leftHashTable = null;
		rightHashTable.close();
		rightHashTable = null;
		queue = null;

		leftpureset = null;
		rightpureset = null;
		intersectionset = null;
		restset = null;
		
		scorefunction = null;
		Threshold = null;
		nextBinding = null ;
		topLeftBinding = null;
		topRightBinding = null;
		bottomLeftBinding = null; 
		bottomRightBinding = null;
		
		
	}

	
	
	@Override
	protected boolean hasNextBinding() {
		// if we don't have even the first result from each of the inputs
		if ( (!getLeft().hasNext()||!getRight().hasNext()) && isFirstMapping)
	          return false ;
		
		if(isFinished())
			return false;
		
		if ( nextBinding != null )
	        return true ;

	    nextBinding = moveToNext() ;
	    return ( nextBinding != null ) ;
		
	}

	
    @Override
    protected Binding moveToNextBinding()
    {
        if ( nextBinding == null )
            throw new ARQInternalErrorException("moveToNextBinding: slot empty but hasNext was true)") ;
        
        Binding b = nextBinding ;
        nextBinding = null ;
        return b ;
    }
	

    // do the real work
	protected Binding moveToNext() {
		
		Binding result = getQueueResults();
		if (result!= null) return result;
		
		while(true){
			Binding b;
				
			if (isFirstMapping){
				
				// we shouldn't need to check if it hasNext(), we already did in the hasNextBinding
				b = getLeft().next();
				topLeftBinding = bottomLeftBinding = b;
				
			
				//insert into left hash table
				leftHashTable.addBinding(b);
				
				// we shouldn't need to check if it hasNext(), we already did in the hasNextBinding
				b = getRight().next();
				topRightBinding = bottomRightBinding = b;
				
			
				// returns join results
				QueryIterator qIter = leftHashTable.matchRightLeft(b, false, false, null, this.getExecContext());
				
				// insert join result into queue
				for ( ; qIter.hasNext() ; ){
					queue.add(qIter.next());
				}
				
				//insert right Binding into right hash table
				rightHashTable.addBinding(b);
				
				maxLeftScore = scorefunction.combine(topLeftBinding, bottomRightBinding, leftpureset, rightpureset, intersectionset, restset, this.getExecContext());
				maxRightScore = scorefunction.combine(bottomLeftBinding, topRightBinding, leftpureset, rightpureset, intersectionset, restset, this.getExecContext());
		
				
				isFirstMapping = false;
			}
			
							
			if (! getInput().hasNext() && !exhaustedInputs){
				
				// if one input is exhausted, switch to the other and check if also the other has finished
				forceSwitching();
				
				// both inputs exhausted
				if (exhaustedInputs){
					// let everything from queue
					Threshold = Score.immutableZeroScore;
				}
			}
			else if (!exhaustedInputs){
		
				// decide how to switch between left and right input
				switchingPolicy();	
				b = getInput().next();	
//				System.out.println(b);
				setBottomBinding(b);
				
				//insert into its hash table
				getHashTable().addBinding(b);
					
				// returns join results with values in the other hash table
				QueryIterator qIter = matchOtherHashTable(b);
					
				// insert join result into queue
				for ( ; qIter.hasNext() ; ){
					queue.add(qIter.next());
				}
				
				calculateThreshold();
					
			}
			else {
				if(queue.isEmpty()) return null;
			}
	
			
			result = getQueueResults();
			if (result!= null) return result;
			
//			// if queue is not empty
//			if (!queue.isEmpty()){
//				if(Score.compare(queue.getTopScore(env), Threshold))
//					return queue.remove();
//			}
				
		}

	}
	
	protected Binding getQueueResults(){
		if (!queue.isEmpty()){
			if(Score.greaterThan(queue.getTopScore(this.getExecContext()), Threshold)){
				Binding outputBinding = queue.remove();
				Binding result = outputBinding;
				// we have all the elements we need to define a final score
				if (restset.isEmpty()){
					Var score = Var.alloc("score");
					Node nv = outputBinding.get(score);
					if (nv == null){
						Score s = scorefunction.evaluate(outputBinding, scorefunction.getPredicates(), restset, this.getExecContext());
						//TODO: remove ugly hack
						Node node = Node.createLiteral(Double.toString(s.getDouble()) + "e0", null, XSDDatatype.XSDdouble);
						result = new BindingMap(outputBinding);
						result.add(score, node);
					}
				}
				
//				if(reportMaker!=null)
//					reportMaker.addCountBindings(this);	
 				//System.out.println(outputBinding);
				return result;
			}
				
		}
	
		return null;
	}
	
	 protected QueryIterator getInput()   { 
		 if (chooseLeftInput) return getLeft();
		 else return getRight();
	} 
	
	
//	protected void setBottomBindingScore(Score s){ 
//		
//		 if (chooseLeftInput) 	bottomLeftScore = s;
//		 else 					bottomRightScore = s;
//
//	}
	
//	protected Score getBottomBindingScore(){ 
//		
//		 if (chooseLeftInput) 	return bottomLeftScore;
//		 else 					return bottomRightScore;
//
//	}
	
	protected void setBottomBinding(Binding b){
		 if (chooseLeftInput) 	bottomLeftBinding = b;
		 else 					bottomRightBinding = b;
		
	}
	 
//	protected PredicateSet getPredicateSet(){ 
//		
//		 if (chooseLeftInput) 	return leftpredicateset;
//		 else 					return rightpredicateset;
//
//	}
	
	protected RankJoinHashTable getHashTable(){
		
		 if (chooseLeftInput) 	return leftHashTable;
		 else 					return rightHashTable;
		

	}
	
	
	protected QueryIterator matchOtherHashTable(Binding b){
		 if (chooseLeftInput) 	return rightHashTable.matchRightLeft(b, false, true, null, this.getExecContext());
		 else 					return leftHashTable.matchRightLeft(b, false, false, null, this.getExecContext());
	}
	
	protected void switchingPolicy(){

		//roundRobin();
		narrowTheGapHeuristic();
		
		// don't switch to empty
		if (chooseLeftInput && !getLeft().hasNext()) chooseLeftInput = false;
		else if (!chooseLeftInput && !getRight().hasNext()) chooseLeftInput = true;
		
	}
	
	protected void roundRobin(){
		 // trivial switching policy: ROUND ROBIN		 
		chooseLeftInput = !chooseLeftInput;
	}
	
	protected void narrowTheGapHeuristic(){
		// select the input with the lower threshold
		
		if(maxLeftScore.getDouble() > maxRightScore.getDouble()){
			chooseLeftInput = false;
		}
		else 
			chooseLeftInput = true;
		
	}
	
	protected void  forceSwitching(){
		// if we are here, it's because one input is exhausted
		chooseLeftInput = !chooseLeftInput;
		// the first two conditions mean that both inputs are exhausted
		if (chooseLeftInput && !getLeft().hasNext()) exhaustedInputs = true;
		else if (!chooseLeftInput && !getRight().hasNext()) exhaustedInputs = true;
		else exhaustedInputs = false;
		
	}
	
	protected void calculateThreshold(){

		//each bottomBinding changes just once, we can half the number of calls to combine
		if (!chooseLeftInput) maxLeftScore = scorefunction.combine(topLeftBinding, bottomRightBinding, leftpureset, rightpureset, intersectionset, restset, this.getExecContext());
		else 				  maxRightScore = scorefunction.combine(bottomLeftBinding, topRightBinding, leftpureset, rightpureset, intersectionset, restset, this.getExecContext());

		Threshold = Score.max(maxLeftScore, maxRightScore);
	}
	
	

	@Override
	public void output(IndentedWriter out) {
        out.print("(") ;
        out.print(Utils.className(this)) ;
        out.print(" ");
        out.print("[" + leftpredicateNames + " ," + rightpredicateNames  + "]");
        out.print(")") ;
	}



	protected void closeSubIterator() {
		// TODO Auto-generated method stub
		
	}



	protected void requestSubCancel() {
		// TODO Auto-generated method stub
		
	}
	

}
