package sparqlrank.iterators;


import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import sparqlrank.baseclasses.PredicateSet;
import com.hp.hpl.jena.sparql.core.Var;

/**
 * Key for the {@link SPARQLRankCache}.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class SPARQLRankCacheKey implements Serializable{

	private static final long serialVersionUID = 1L;
	
	// is BasicPattern the type we need?
	private Set<Var> setofvars; 
	private PredicateSet listOfRankPredicates;
	private String dataset = null;
	
	public SPARQLRankCacheKey( Set<Var> setofvars, PredicateSet listOfRankPredicates, String dataset ){
		
		this.setofvars = new HashSet<Var>();
		
		
		// order vars by name (avoid permutations)
		Var[] vararray =  (setofvars).toArray(new Var[]{});
		
		Arrays.sort(vararray, new Comparator<Var>(){
		    public int compare(Var v1, Var v2)
		    {
		        return v1.getName().compareTo(v2.getName());
		    } });


		this.setofvars.addAll(Arrays.asList(vararray));	
		this.listOfRankPredicates = (PredicateSet) listOfRankPredicates.clone();
		this.setDataset(dataset);
	}
	
	public void setSetofvars(Set<Var> listofvars) {
		this.setofvars = listofvars;
	}
	public Set<Var> getSetofvars() {
		return setofvars;
	}

	public void setListOfRanks(PredicateSet listOfRankPredicates) {
		this.listOfRankPredicates = listOfRankPredicates;
	}
	public PredicateSet getListOfRanks() {
		return listOfRankPredicates;
	}
	

	public void setDataset(String dataset) {
		this.dataset = dataset;
	}

	public String getDataset() {
		return dataset;
	}

	public String toString(){
		
		String vars = this.getSetofvars().toString();

		String preds = this.getListOfRanks().toString().replace("?", "");		
		
		String key =  vars + "; " + preds + "; " + this.getDataset();
		
		//TODO: hashing of the strings (limited to 250 chars)
		//TODO: use StringBuffer
		if(key.length()>250){
			key = vars + ";"+ key.hashCode();
			System.out.println("Cache key limited to 250 chars");
		}
		
		return key;
	}
	


    public static int hashCode(SPARQLRankCacheKey key)
    {

        System.out.println("Hash called ");		
        int hash = 0xC0 ;
       
        for (Var v : key.getSetofvars()){
        	hash ^= v.getName().hashCode();        
        }
        
       	hash ^=key.getListOfRanks().hashCode();
       
       
        System.out.println("Hash: " + hash);
        
        return hash ;
    }
	

	public boolean equals (Object other){
		
        System.out.println("Equals called ");
        
        if ( this == other ) return true ;
        if ( ! ( other instanceof SPARQLRankCacheKey) ) 
            return false ;
       
        SPARQLRankCacheKey skey = (SPARQLRankCacheKey)other ;
        
        if(skey.toString().equals(this.toString()))      
             return true;
        else
        	return false;
	}


	
	
}
