package sparqlrank.iterators;


import sparqlrank.baseclasses.PredicateSet;
import sparqlrank.baseclasses.Score;
import sparqlrank.baseclasses.ScoreFunction;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.expr.Expr;
import com.hp.hpl.jena.sparql.function.FunctionEnv;
import com.hp.hpl.jena.sparql.function.FunctionEnvBase;

/**
 *  Class used by the {@link RankedQueue} to compare Bindings.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */
public class RankingComparator implements java.util.Comparator<Binding> {

	private PredicateSet predicates ;
    private ScoreFunction scorefunction;
    private FunctionEnv env ;
	private PredicateSet restset;
	
	public PredicateSet getPredicates(){	return predicates;}
	public ScoreFunction getScoreFunction(){ return scorefunction;}
	public PredicateSet getRestSet(){ return restset;}
    
    public RankingComparator(PredicateSet p, ScoreFunction s, ExecutionContext execCxt)
    {
    	predicates = p;
		scorefunction = s;
        env = execCxt ;
        
		// the rest of the predicates
		restset = new PredicateSet();
		restset.addAll(s.getPredicates());
		restset.removeAllPredicates(p);
        
    }
    
    public RankingComparator(PredicateSet p, ScoreFunction s){
		predicates = p;
		scorefunction = s;
		this.env = new FunctionEnvBase();
		
		// the rest of the predicates
		restset = new PredicateSet();
		restset.addAll(s.getPredicates());
		restset.removeAllPredicates(p);
    }

    public RankingComparator(PredicateSet p, PredicateSet r, ScoreFunction s)
    {
    	predicates = p;
		scorefunction = s;
        env =  new FunctionEnvBase();
    	restset = r;
        
    }
    
	@Override
	public int compare(Binding arg0, Binding arg1) {
		
		
		Score s1 = scorefunction.evaluate(arg0, predicates, restset, env);
		Score s2 = scorefunction.evaluate(arg1, predicates, restset, env);
		
		int x = Score.compare(s1, s2);
		
        if ( x < 0 ) return Expr.CMP_LESS ;
        if ( x > 0 ) return Expr.CMP_GREATER ;
        return Expr.CMP_EQUAL ;
		
		
		
		//very old debugging
//		if(!arg0.contains(Var.alloc("avgscore1"))) {
//			Predicate avgscorepred = new Predicate(new SortCondition(new ExprVar("avgscore1"), -1));
//			System.out.println("Predicates:"+ predicates +"avgscore1 unbound: " + arg0 + " " + s1.getDouble());
//			for (Predicate p: predicates){
//				if(avgscorepred.equals(p)){
//					System.out.println("avgscore1 unbound: " + arg0 + " " + s1.getDouble());
//					System.out.println(arg1 + " "+s2.getDouble() );
//					System.out.println("Comparison :"+x);
//				}
//					
//			}
//
//		}
		

	}
	
	public void close(){
    	predicates = null;
		scorefunction = null;
        env = null;
        restset = null;
	}
	
}
