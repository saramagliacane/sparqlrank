package sparqlrank.iterators;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.hp.hpl.jena.sparql.algebra.Algebra;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterNullIterator;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterPlainWrapper;
import com.hp.hpl.jena.sparql.expr.ExprList;

/**
 * Class used by the {@link QueryIterRankJoin} to store temporary results.
 * Very similar to TableN, slight change in the merge algorithm because of the switching policy.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 * @see com.hp.hpl.jena.sparql.algebra.table.TableN
 */

public class RankJoinHashTable {
	
	protected List<Binding> rows = new ArrayList<Binding>() ;
	
	public RankJoinHashTable() {}
	
    public void addBinding(Binding binding)
    {
        rows.add(binding) ;
    }
    
    public int size()           { return rows.size() ; }
    public boolean isEmpty()    { return rows.isEmpty() ; }
    
    public QueryIterator matchRightLeft(Binding bindingLeft, boolean includeOnNoMatch, boolean isLeftBinding,
	                                    ExprList conditions,
	                                    ExecutionContext execContext)
	{
	    List<Binding> out = new ArrayList<Binding>() ;
	    
    	
	    
	    for ( Iterator<Binding> iter = rows.iterator() ; iter.hasNext() ; )
	    {
	        Binding bindingRight = iter.next() ;
	        
	        Binding r;
	        
	        try{
	        	if(isLeftBinding)    	{
	        		r =  Algebra.merge(bindingLeft, bindingRight) ;
//	        		if(r!=null) 
//	        			{
//	        			System.out.println ("right" +bindingRight);
//	        			System.out.println("left2:"+ bindingLeft);
//	        			}
	        			
	        	}
	        	else 					{
	        		r =  Algebra.merge(bindingRight, bindingLeft) ;
//	        		if(r!=null){  
//	        			System.out.println("left:"+ bindingLeft);
//	        			System.out.println ("right2" +bindingRight);
//	        		}
	        	}
	        
	        }catch(Exception e){

	        	//System.out.println(e);
	        	r = null;
	        }
	        
	        if ( r == null )
	            continue ;
	        // This does the conditional part. Theta-join.
	        if ( conditions == null || conditions.isSatisfied(r, execContext) )
	            out.add(r) ;
	    }
	            
	    if ( out.size() == 0 && includeOnNoMatch )
	        out.add(bindingLeft) ;
	    
	    if ( out.size() == 0 )
	        return new QueryIterNullIterator(execContext) ;
	    return new QueryIterPlainWrapper(out.iterator(), execContext) ;
	}
 
    public QueryIterator iterator(ExecutionContext execCxt)
    {
        return new QueryIterPlainWrapper(rows.iterator(), execCxt) ;
    }
    
    public void close()  {  
    	rows = null ;    
    }

}
