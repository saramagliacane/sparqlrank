package sparqlrank.iterators;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.shared.PrefixMapping;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIter;
import com.hp.hpl.jena.sparql.serializer.SerializationContext;
import com.hp.hpl.jena.sparql.util.Utils;

import org.openjena.atlas.io.IndentedWriter;

import sparqlrank.baseclasses.Predicate;
import sparqlrank.baseclasses.PredicateSet;
import sparqlrank.baseclasses.ScoreFunction;


/**
 * Physical implementation to be used in a Pipe Join - not developed.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

@Deprecated
public class QueryIterIncrementalRandom extends QueryIter{
	
	private final QueryIterator qIter;
	private Predicate predicate;
	private final String predicateName;
	
	//TODO: how to define the Joinvariable
	private final Var joinVariable = Var.alloc("product");
	//private HashMap<Node, QueryIterator> inputs;	
	private final HashMap<Node, ArrayList<Binding>> inputs;	
	
	private final PredicateSet subpredicateset;
	private final ScoreFunction scorefunction;
	
	
	public QueryIterIncrementalRandom(final QueryIterator qIter, final Predicate predicate,
			final ScoreFunction scoreFunction, final PredicateSet subpredicateset, final ExecutionContext execCtx) {
		
		super(execCtx);
		
		this.qIter = qIter;
		this.predicate = predicate;
		this.predicateName = predicate.toString();
		this.subpredicateset = subpredicateset;
		this.scorefunction= scoreFunction;

		final PredicateSet restset = new PredicateSet();
		restset.addAll(scorefunction.getPredicates());
		restset.removePredicate(predicate);
		restset.removeAllPredicates(subpredicateset);
		
		final PredicateSet fullpredicateset = (PredicateSet) this.subpredicateset.clone();
		fullpredicateset.add(predicate);

		final RankingComparator rankingComparator = new RankingComparator (fullpredicateset, restset, scorefunction );
		
		//this.inputs = new HashMap<Node, QueryIterator>();
		this.inputs = new HashMap<Node, ArrayList<Binding>>();	
		
		//materialize
		while (qIter.hasNext()){
			final Binding b = qIter.next();
			final Node n = b.get(this.joinVariable);
			if (this.inputs.containsKey(n)){
				this.inputs.get(n).add(b);
			}
			else{
				this.inputs.put(n, new ArrayList<Binding>());
				this.inputs.get(n).add(b);
			}

		}
		
		//sort materialized - TODO: insert sorted...
		final Collection<Node> nodes = new ArrayList<Node>();
		nodes.addAll(this.inputs.keySet());
		for (final Node node: nodes ){
			final ArrayList<Binding> list = this.inputs.get(node);
			this.inputs.remove(node);
			final Binding[] array = list.toArray(new Binding[list.size()]);
			list.clear();
			
			Arrays.sort( array, rankingComparator);

			for (int i = 0; i<array.length; i++){
				list.add(array[i]);
			}
			this.inputs.put(node, list);
			
		}

	}


	protected boolean hasNext(final Node j) {
		
		if (!inputs.containsKey(j)){
			//instantiate a new iterator on the value of j
			
//			if (qIter instanceof QueryIterTDB){
//				QueryIterTDB qIterTDB = (QueryIterTDB) qIter;
//				//StageMatchTuple iter = qIterTDB.iterator;
//				
//				//  List<Triple> triples = pattern.getList() ;
//				//for (Triple triple: triples)
//				//Tuple<Node> tuple =  Tuple.create(triple.getSubject(), triple.getPredicate(), triple.getObject()) ;
//				//iter.patternTuple = tuple;
//				inputs.put(j, new QueryIterRank(qIter, predicate, scorefunction, subpredicateset, null, 0, execCtx));
//				
//				
//			}
			
			return false;
		}
		

		if ( inputs.get(j).isEmpty())
			return false ;
	  
		
		return true;
     }

	protected Binding next(final Node j) {
		
		return inputs.get(j).remove(0);
		
	}
	
	
	
    protected void details(final IndentedWriter out, final SerializationContext sCxt)
    {
        out.print("[ "+ predicateName +"]");
    }
	

	@Override
   public String toString(final PrefixMapping pmap) {

		return Utils.className(this)+" "+ predicateName + " - number of iterators "+ inputs.size() ;
	}


	@Override
   public void output(final IndentedWriter out) {
	     out.print("(") ;
	     out.print(Utils.className(this)) ;
	     out.print(" ") ;
	     out.print(predicateName) ;
	     out.print(" - number of iterators "+ inputs.size());
	     out.print(")") ;
	}
	
 

	protected void closeSubIterator() {
		
//		for (QueryIterator i: inputs.values()){
//			i.close();
//		}
		predicate = null;
		
	}


	@Override
	protected void closeIterator() {
		// TODO Auto-generated method stub
		
	}


	@Override
	protected boolean hasNextBinding() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	protected Binding moveToNextBinding() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
   protected void requestCancel() {
		// TODO Auto-generated method stub
		
	}

}
