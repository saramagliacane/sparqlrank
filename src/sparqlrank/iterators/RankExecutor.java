package sparqlrank.iterators;


import com.hp.hpl.jena.sparql.algebra.op.OpDistinct;
import com.hp.hpl.jena.sparql.algebra.op.OpJoin;
import com.hp.hpl.jena.sparql.algebra.op.OpOrder;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.main.OpExecutor;

import sparqlrank.algebra.op.OpGoodK;
import sparqlrank.algebra.op.OpIncrementalRandom;
import sparqlrank.algebra.op.OpNonBlockingTopK;
import sparqlrank.algebra.op.OpRank;
import sparqlrank.algebra.op.OpRankJoin;
import sparqlrank.algebra.op.OpRankJoinRandom;
import sparqlrank.algebra.op.OpRankSequence;
import sparqlrank.algebra.op.OpTieBreaker;

/**
 * SPARQLRank version of the OpExecutor in ARQ
 * Inserting the right QueryIterators for the new Ops
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */




public class RankExecutor extends OpExecutor {

  
   
	protected RankExecutor(final ExecutionContext execCxt) {
		super(execCxt);
	}
    
	@Override
   public QueryIterator execute(final OpOrder op, final QueryIterator input)
	{ 
        if(op instanceof OpRank){
            final OpRank opRank = (OpRank) op;
            QueryIterator qIter = executeOp(opRank.getSubOp(), input);

            qIter = new QueryIterRank(qIter, opRank.getPredicate(), opRank.getScoreFunction(), opRank.getSubPredicateset(), 
            		opRank.getVars(), opRank.getSubPattern(), execCxt);
            
            opRank.close();
            return qIter;
        }
        else if (op instanceof OpTieBreaker){
           final OpTieBreaker opRank = (OpTieBreaker) op;
           QueryIterator qIter = executeOp(opRank.getSubOp(), input);

           qIter = new QueryIterTieBreaker(qIter, opRank.getConditions(), execCxt);

           return qIter;
        }
        
        else if(op instanceof OpIncrementalRandom){
        	final OpIncrementalRandom opRank = (OpIncrementalRandom) op;
            QueryIterator qIter = executeOp(opRank.getSubOp(), input);

            qIter = new QueryIterIncrementalRandom(qIter, opRank.getPredicate(), opRank.getScoreFunction(), opRank.getSubPredicateset(), 
            		execCxt);
            
            opRank.close();
            return qIter;
        }
        else
        	return super.execute(op, input);

    }
	
	@Override
   public QueryIterator execute(final OpJoin op, final QueryIterator input)
    { 
		if(op instanceof OpRankJoin){
			final OpRankJoin opRankJoin = (OpRankJoin) op;
	        final QueryIterator left = executeOp(opRankJoin.getLeft(), input) ;
	        final QueryIterator right = executeOp(opRankJoin.getRight(), root()) ;
	        
	        final QueryIterator qIter = new QueryIterRankJoin(left, right, opRankJoin.getScoreFunction(), 
	               opRankJoin.getLeftPredicateset(), opRankJoin.getRightPredicateset(), execCxt);
	        
           opRankJoin.close();
           return qIter;
	        
		}
		else if (op instanceof OpRankSequence){
		   
		   final OpRankSequence opRankJoin = (OpRankSequence) op;
         final QueryIterator left = executeOp(opRankJoin.getLeft(), input) ;
         final QueryIterator right = executeOp(opRankJoin.getRight(), root()) ;
         
         final QueryIterator qIter = new QueryRankSequence(left, right, opRankJoin.getScoreFunction(), 
               opRankJoin.getLeftPredicateset(), opRankJoin.getRightPredicateset(), execCxt);
         
         opRankJoin.close();
         return qIter;
		}
	    else if (op instanceof OpRankJoinRandom){
	         
	         final OpRankJoinRandom opRankJoin = (OpRankJoinRandom) op;
	         final QueryIterator left = executeOp(opRankJoin.getLeft(), input) ;
	         final QueryIterator right = executeOp(opRankJoin.getRight(), root()) ;
	         
	         final QueryIterator qIter = new QueryIterRankJoinRandom(left, right, opRankJoin.getScoreFunction(), 
                  opRankJoin.getLeftPredicateset(), opRankJoin.getRightPredicateset(), execCxt);
            opRankJoin.close();
            return qIter;
	      }
		
	        
//   	else if (op instanceof OpPipeJoin){
//			final OpPipeJoin opRankJoin = (OpPipeJoin) op;
//	        final QueryIterator left = executeOp(opRankJoin.getLeft(), input) ;
//	        final QueryIterator right = executeOp(opRankJoin.getRight(), root()) ;
//	
//	        final QueryIterator qIter = new QueryIterPipeJoin(left, right, opRankJoin.getScoreFunction(), 
//	        		opRankJoin.getLeftPredicateset(), opRankJoin.getRightPredicateset(), execCxt);
//
//	        opRankJoin.close();
//	        return qIter;
//		}

		else
        	return super.execute(op, input);
    }
	
	
	@Override
   public QueryIterator execute(final OpDistinct op, final QueryIterator input)
	{ 
       
        
        if(op instanceof OpNonBlockingTopK){
        
	        final OpNonBlockingTopK opNonBlockingTopK = (OpNonBlockingTopK) op;
	        QueryIterator qIter = executeOp(opNonBlockingTopK.getSubOp(), input);
	        qIter = QueryIterNonBlockingTopK.create(qIter, opNonBlockingTopK.getPredicates(), execCxt);
	        opNonBlockingTopK.close();
	        return qIter;
        
        }
        
        else  if(op instanceof OpGoodK){
        	
        	final OpGoodK opGoodK = (OpGoodK) op;
            QueryIterator qIter = executeOp(opGoodK.getSubOp(), input);
            qIter = QueryIterGoodK.create(qIter, opGoodK.getPredicate(), opGoodK.getDefaultValue(), opGoodK.getScoreFunction(), execCxt);
            opGoodK.close();
            return qIter;
        
        }
        
        else  return super.execute(op, input);
    }
	

}
