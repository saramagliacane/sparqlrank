package sparqlrank.iterators;


import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.sparql.ARQConstants;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.main.OpExecutor;
import com.hp.hpl.jena.sparql.engine.main.OpExecutorFactory;
import com.hp.hpl.jena.sparql.util.Context;
import com.hp.hpl.jena.sparql.util.Symbol;

/**
 * Class implementing the Factory pattern to create a SPARQLRank OpExecutor
 * Includes some code to ensure that the RankExecutor is a singleton
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class RankExecutorFactory implements OpExecutorFactory {

	private final static Symbol rankExecutorFactorySym = Symbol.create(ARQConstants.systemVarNS+"RankExecutorFactory") ;
	
	public static void setRankExecutor(){
		setRankExecutor(ARQ.getContext());
	}
	
	public static void setRankExecutor( Context context ){
		
		RankExecutorFactory rankExecutorFactory = (RankExecutorFactory) context.get(rankExecutorFactorySym) ;
		
		if (rankExecutorFactory == null){
			rankExecutorFactory = new RankExecutorFactory() ;
			context.set(rankExecutorFactorySym, rankExecutorFactory) ;
		}

		context.set(ARQConstants.sysOpExecutorFactory, rankExecutorFactory) ;

		
	}
	
	@Override
	public OpExecutor create(ExecutionContext execCxt) {
		return new RankExecutor(execCxt);
	}

}
