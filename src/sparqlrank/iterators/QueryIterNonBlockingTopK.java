package sparqlrank.iterators;

import sparqlrank.baseclasses.Predicate;
import sparqlrank.baseclasses.PredicateSet;
import com.hp.hpl.jena.sparql.ARQInternalErrorException;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIteratorWrapper;
import com.hp.hpl.jena.sparql.expr.VariableNotBoundException;

/**
 * Do not consider, abandoned direction 
 * Iterator that stops when there are Bindings with uncertain score at the topmost level of the query execution tree
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class QueryIterNonBlockingTopK extends QueryIteratorWrapper {
	
	private PredicateSet predicates;
	private ExecutionContext execCxt;
	private Binding nextBinding = null;
	
	
	public static QueryIterNonBlockingTopK create(QueryIterator qIter,
			PredicateSet predicates, ExecutionContext execCxt) {
		return new QueryIterNonBlockingTopK(qIter, predicates,  execCxt);
	}

	public QueryIterNonBlockingTopK(QueryIterator qIter,
			PredicateSet predicates, ExecutionContext execCxt) {
		super(qIter) ;
		this.execCxt = execCxt;
		this.predicates = predicates;
		
	}
	
	
	@Override
	protected boolean hasNextBinding() { 
		if (isFinished()) return false;
		
		if ( nextBinding != null )
	        return true ;

	    nextBinding = moveToNext() ;
	    return ( nextBinding != null ) ;
	}
	
    @Override
    protected Binding moveToNextBinding()
    {
        if ( nextBinding == null )
            throw new ARQInternalErrorException("moveToNextBinding: slot empty but hasNext was true)") ;
        
        Binding b = nextBinding ;
        nextBinding = null ;
        return b ;
    }
	
	
	private Binding moveToNext() { 
		
		Binding b = super.iterator.nextBinding();
		
		try {
			
			for (Predicate p : predicates){
				p.getExpr().eval(b, execCxt);
			}
		
		} catch (VariableNotBoundException e)
		{
			System.out.println("ExecutionModel NonBlockingTopK: Query rejected - unbound predicates at the top level");
			b = null;
		}
	
		return b;
	 }
	
    @Override
    protected void closeIterator() {    super.closeIterator() ;   }
}
