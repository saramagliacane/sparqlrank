package sparqlrank.iterators;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.sparql.ARQConstants;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.util.Context;
import com.hp.hpl.jena.sparql.util.Symbol;

/**
 * Caching mechanism for both {@link sparqlrank.iterators.QueryIterRank} and {@link sparqlrank.iterators.QueryIterPipeJoin}. 
 * Naive implementation using a HashMap. Some dead useless code for trying to use memcached (first need to make Bindings Serializable). 
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */


public class SPARQLRankCache {

	private LinkedHashMap<String, Binding[]> cache = null;
	//private MemcachedClient c = null;
	
	private final LinkedHashMap<String, ArrayList> listOfJoinValues = new LinkedHashMap<String, ArrayList> ();
	
	private final int numberOfResultsPrinted = 1;
	
	private final Boolean USE_MEMCACHED = false;

	
	private final Symbol notInCacheSym = Symbol.create(ARQConstants.systemVarNS+"notInCache") ;
	
	private final static Symbol SPARQLRankCacheSym = Symbol.create(ARQConstants.systemVarNS+"SPARQLRankCache") ;
	
	public SPARQLRankCache(){
		if (USE_MEMCACHED){
			//try {
				//this.c = new MemcachedClient(new InetSocketAddress("localhost", 11211));
			//} catch (IOException e) {
			//	e.printStackTrace();
			//}
		}
		else{
			cache = new LinkedHashMap<String, Binding[]> ();
			
			//TODO: solo se si usa la ottimizzazione
			//listOfJoinValues = new LinkedHashMap<String, ArrayList> ();
		}
		
	}
	
	public static SPARQLRankCache createSPARQLRankCache(){
		return createSPARQLRankCache(ARQ.getContext());
	}
	
	public static SPARQLRankCache createSPARQLRankCache( final Context context ){
		
		SPARQLRankCache sparqlRankCache = (SPARQLRankCache) context.get(SPARQLRankCacheSym) ;
		
		if (sparqlRankCache == null){
			System.out.println("New cache created");
			sparqlRankCache = new SPARQLRankCache();
			context.set(SPARQLRankCacheSym, sparqlRankCache) ;
		}
		
		return sparqlRankCache;
	}

	public void put(final SPARQLRankCacheKey key , final Binding[] value){
		
	
		if (USE_MEMCACHED){
			if (value[0] instanceof Serializable)
				System.out.println("Yuppi! Bindings are serializable");
			//c.set(key.toString(), 1000000, value);
		}
		else{
			//System.out.println("**********************************************************");
			//System.out.println("Not in cache for [" +key+ "] ");
			
			ARQ.getContext().set(notInCacheSym, true);
			
			for (int i = 0; i<numberOfResultsPrinted; i++){
				if (i >= value.length) 
					break;
				System.out.println("put "+i+" value: "+ value[i]);
			}
			//System.out.println("**********************************************************");
			
	
			
	      try{
	         cache.put(key.toString(), value);
	      } catch (final Exception e){
	         System.out.println("*** Clear cache");
	         SPARQLRankCache.freeSPARQLRankCache();
	      }
		}

		

	}

	
	
	@SuppressWarnings("unchecked")
	public Binding[] get(final SPARQLRankCacheKey key){
		//TODO: if there is <BGP, list of ranks, limit> such that
		// limit is > new_limit, return this but trimmed to the new limit
		
				
//		if (USE_MEMCACHED){
//			return (Binding[] ) c.get(key.toString());
//		}
//		else{
			final Binding[] value = cache.get(key.toString());
			if (value != null){
				//System.out.println("**********************************************************");
				//System.out.println("In cache for [" +key+ "]: " + value.length + " entries");
				

				
				for (int i = 0; i<numberOfResultsPrinted; i++){
					if (i >= value.length -1) 
						break;
					//System.out.println("get "+ i+" value: "+ value[i]);
				}
	
				//System.out.println("**********************************************************");
				
			}
			return value;
//		}
		

		
	}
	
	
	@SuppressWarnings("unchecked")
	public ArrayList getListOfJoinValues(final SPARQLRankCacheKey key){
		//TODO: if there is <BGP, list of ranks, limit> such that
		// limit is > new_limit, return this but trimmed to the new limit
		
		final ArrayList value = listOfJoinValues.get(key.toString());
		
//		if (value != null)
//			System.out.println("In cache for list of values  [" +key+ "]: " + value.size() + " entries");
//			
		return value;

		
	}
	
	public void putListOfJoinValues(final SPARQLRankCacheKey key , final ArrayList value){
		//TODO: if list of ranks contains only one rank, then limit is useless
		//TODO: if there is <BGP, list of ranks, limit> such that
		// limit is > new_limit, don't put it
	
	   try{
	      listOfJoinValues.put(key.toString(), value);
	   } catch (final Exception e){
         System.out.println("*** Clear cache");
	      SPARQLRankCache.freeSPARQLRankCache();
	   }
	
	}
	
	//TODO: free cahce
	
	public void emptyCache(){
	   cache = null;
	}
	
	
	public static void freeSPARQLRankCache(){
	   
	   final SPARQLRankCache sparqlRankCache = (SPARQLRankCache) ARQ.getContext().get(SPARQLRankCacheSym) ;
     
	   if(sparqlRankCache!= null){
	      sparqlRankCache.emptyCache();
	      ARQ.getContext().set(SPARQLRankCacheSym, null) ;
	      
         System.out.println("*** Cleared cache");
	      
//	      try {
//	         Thread.sleep(5000);
//	      } catch (final InterruptedException e) {
//	         // TODO Auto-generated catch block
//	         e.printStackTrace();
//	      }
	   }
	   
	   

	}
	
}
