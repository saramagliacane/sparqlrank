package sparqlrank.iterators;

import java.util.List;

import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.sparql.ARQInternalErrorException;
import com.hp.hpl.jena.sparql.core.BasicPattern;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.binding.BindingMap;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIter2;
import com.hp.hpl.jena.sparql.util.Utils;

import org.openjena.atlas.io.IndentedWriter;

import sparqlrank.baseclasses.PredicateSet;
import sparqlrank.baseclasses.Score;
import sparqlrank.baseclasses.ScoreFunction;
import sparqlrank.iterators.QueryIterRankJoinRandom.BGPMapper;
import sparqlrank.iterators.QueryIterRankJoinRandom.TripleMapper;

/** 
 * ALTERNATIVE 1
 * Wannabe implementation of a ranked Sequence (RankJoin with also random access on the right)
 * iterator. Used with a single Triple Pattern + Rank on right.
 * @author saramagliacane
 *
 */

public class QueryRankSequence extends QueryIter2{
		
		// queue for outcoming results
		private RankedQueue queue;

		// used to calculate combine scores
		private PredicateSet leftpureset;
		private PredicateSet rightpureset;
		private PredicateSet intersectionset;
		private PredicateSet restset;
		private final PredicateSet thresholdpredicateset;
		
		private final PredicateSet nullpredicate= new PredicateSet();
		  
		private final String leftpredicateNames;
		private final String rightpredicateNames;
		
		private ScoreFunction scorefunction;
		private Score Threshold;
		
		// We need two hash tables to contain all the mappings we've seen till now
		// from both the inputs
		private Triple rightTriplePattern;
      private BasicPattern rightPattern;
		
		
		private Boolean isFirstMapping = true;
		private final Boolean chooseLeftInput = true;
		private Boolean exhaustedInputs = false;
		
		// pre-fetched next Binding - needed to answer if the operator hasNext()
	    private Binding nextBinding = null ;

	    private Binding topRightBinding, bottomLeftBinding;


	        


		public QueryRankSequence(final QueryIterator left, final QueryIterator right,
				final ScoreFunction scoreFunction, final PredicateSet l, final PredicateSet r, final ExecutionContext execCtx) 
		{
			super(left, right, execCtx);
			this.scorefunction = scoreFunction;

			// TODO: move the calculation to OpRankJoin?
			// performed here so we don't need to calculate it in each call to ScoreFunction.combine(..)
			leftpureset = PredicateSet.removeAllPredicates ( l, r);
			rightpureset = PredicateSet.removeAllPredicates ( r, l);
			intersectionset = PredicateSet.intersection ( l, r);
			
			leftpredicateNames = l.toString();
			rightpredicateNames = r.toString();
			
			// union of the left and right PredicateSets
			final PredicateSet sumOfLeftAndRightPredicateSet = new PredicateSet();
			sumOfLeftAndRightPredicateSet.addAll(l);
			sumOfLeftAndRightPredicateSet.addAll(r);
			
			restset = new PredicateSet();
			restset.addAll(scorefunction.getPredicates());
			restset.removeAllPredicates(l);
			restset.removeAllPredicates(r);
			
			thresholdpredicateset = new PredicateSet();
			thresholdpredicateset.addAll(r);
			thresholdpredicateset.addAll(restset);
			
			
			queue = RankedQueue.create(sumOfLeftAndRightPredicateSet, restset, scorefunction);
			 
			try{ 
				rightTriplePattern = ((QueryIterRank) right).getSubpattern().getList().get(0);
				rightPattern = ((QueryIterRank) right).getSubpattern();
			} catch(final Exception e ){ 
				System.out.println("ERROR: Not supposed to have RankSequence here.");
				System.exit(-1);
			}
			
			
		}


		//TODO: check how to use
		protected void releaseResources() {
			
			
			getLeft().close();
			getRight().close();

			queue = null;

			leftpureset = null;
			rightpureset = null;
			intersectionset = null;
			restset = null;
			
			scorefunction = null;
			Threshold = null;
			nextBinding = null ;
			bottomLeftBinding = null; 
			
			
		}

		
		
		@Override
		protected boolean hasNextBinding() {
			// if we don't have even the first result from each of the inputs
			if ( (!getLeft().hasNext()||!getRight().hasNext()) && isFirstMapping)
		          return false ;
			
			if(isFinished())
				return false;
			
			if ( nextBinding != null )
		        return true ;

		    nextBinding = moveToNext() ;
		    return ( nextBinding != null ) ;
			
		}

		
	    @Override
	    protected Binding moveToNextBinding()
	    {
	        if ( nextBinding == null )
	            throw new ARQInternalErrorException("moveToNextBinding: slot empty but hasNext was true)") ;
	        
	        final Binding b = nextBinding ;
	        nextBinding = null ;
	        return b ;
	    }
		

	    // do the real work
		protected Binding moveToNext() {
			
			Binding result = getQueueResults();
			if (result!= null) return result;
			
			while(true){
				Binding b, leftBinding;
            final Binding rightBinding;
					
				if (isFirstMapping){
					
					// PHASE 0 - get Left
					leftBinding = getLeft().next();
					bottomLeftBinding = leftBinding;
					

					// we shouldn't need to check if it hasNext(), we already did in the hasNextBinding
					matchLeftBinding(leftBinding);


					// PHASE 1- get Right
					//rightBinding = getRight().next();
					//topRightBinding = rightBinding;
					
					
					// in this case maxLeft is quite useless
					//Threshold = scorefunction.combine(bottomLeftBinding, topRightBinding, leftpureset, rightpureset, intersectionset, restset, this.getExecContext());
			
					Threshold = scorefunction.combine(bottomLeftBinding, null, leftpureset, nullpredicate, nullpredicate, thresholdpredicateset, this.getExecContext());
		         
					
					isFirstMapping = false;
				}
				
								
				if (! getLeft().hasNext() && !exhaustedInputs){
				   
				      exhaustedInputs= true;
					
						// let everything from queue
						Threshold = Score.immutableZeroScore;
				}
				else if (!exhaustedInputs){
			
					// decide how to switch between left and right input		
					b = getLeft().next();	

					setBottomBinding(b);
					
					matchLeftBinding(b);
					calculateThreshold();
						

						
				}
				else {
					if(queue.isEmpty()) return null;
				}
		
				
				result = getQueueResults();
				if (result!= null) return result;
				

					
			}

		}

		



		private void matchLeftBindingOriginal(final Binding leftBinding) {
			//insert into left hash table
			//leftHashTable.addBinding(leftBinding);
			
			final TripleMapper rightTripleMapper = new TripleMapper(leftBinding, rightTriplePattern, this.getExecContext());
			Binding rightBinding;

				
			// ALTERNATIVE SOLUTION 1 -  useful in 1:1 (or 1: small n) relationships 
			// a possibility is to add them all, don't use a left hash join table, no switching policy
			// no need to worry about the repetitions
			while(rightTripleMapper.hasNext()){
				rightBinding = rightTripleMapper.next();
				//System.out.println("matchLeftBinding: " + rightBinding +   "-" + maxLeftScore.getDouble());
				queue.add(rightBinding);
				
			}
			
		}

		

      private void matchLeftBinding(final Binding leftBinding) {

         final BGPMapper BGPMapper = new BGPMapper(rightPattern, leftBinding, this.getExecContext(), isFirstMapping);
         final List<Binding> templist = BGPMapper.findMatchingBinding(leftBinding);
         
         if(templist == null || templist.isEmpty()){
            //System.out.println("cannot produce bindings for: "+rightPattern + " -" + leftBinding);
            return;
         }
            
         for(final Binding b: templist){
            //System.out.println("matchLeftBinding: " + rightBinding +   "-" + maxLeftScore.getDouble());
            queue.add(b);
            
         }
         
      }
		

		protected Binding getQueueResults(){
			if (!queue.isEmpty()){
				if(Score.greaterThan(queue.getTopScore(this.getExecContext()), Threshold)){
					final Binding outputBinding = queue.remove();
					Binding result = outputBinding;
					// we have all the elements we need to define a final score
					if (restset.isEmpty()){
						final Var score = Var.alloc("score");
						final Node nv = outputBinding.get(score);
						if (nv == null){
							final Score s = scorefunction.evaluate(outputBinding, scorefunction.getPredicates(), restset, this.getExecContext());
							//TODO: remove ugly hack
							final Node node = Node.createLiteral(Double.toString(s.getDouble()) + "e0", null, XSDDatatype.XSDdouble);
							result = new BindingMap(outputBinding);
							result.add(score, node);
						}
					}
					
					return result;
				}
					
			}
		
			return null;
		}
		

		
		
		protected void setBottomBinding(final Binding b){
		   bottomLeftBinding = b;
			
		}
		 
		
		

		
		protected void calculateThreshold(){

			//each bottomBinding changes just once, we can half the number of calls to combine
		   Threshold = scorefunction.combine(bottomLeftBinding, topRightBinding, leftpureset, rightpureset, intersectionset, restset, this.getExecContext());

			
		}
		
		

		@Override
		public void output(final IndentedWriter out) {
	        out.print("(") ;
	        out.print(Utils.className(this)) ;
	        out.print(" ");
	        out.print("[" + leftpredicateNames + " ," + rightpredicateNames  + "]");
	        out.print(")") ;
		}



		@Override
      protected void closeSubIterator() {
			// TODO Auto-generated method stub
			
		}



		@Override
      protected void requestSubCancel() {
			// TODO Auto-generated method stub
			
		}
		
}		
		
//		
//		/////// From QueryIterTriplePattern.java
//		
//	    static class TripleMapper extends QueryIter
//	    {
//	        private final Node s ;
//	        private final Node p ;
//	        private final Node o ;
//	        private final Binding binding ;
//	        private ClosableIterator<Triple> graphIter ;
//	        private Binding slot = null ;
//	        private boolean finished = false ;
//	        private volatile boolean cancelled = false ;
//
//	        TripleMapper(final Binding binding, final Triple pattern, final ExecutionContext cxt)
//	        {
//	            super(cxt) ;
//	            this.s = substitute(pattern.getSubject(), binding) ;
//	            this.p = substitute(pattern.getPredicate(), binding) ;
//	            this.o = substitute(pattern.getObject(), binding) ;
//	            this.binding = binding ;
//	            final Node s2 = tripleNode(s) ;
//	            final Node p2 = tripleNode(p) ;
//	            final Node o2 = tripleNode(o) ;
//	            final Graph graph = cxt.getActiveGraph() ;
//	            
//	            final ExtendedIterator<Triple> iter = graph.find(s2, p2, o2) ;
//	            
//	            if ( false )
//	            {
//	                // Materialize the results now. Debugging only.
//	                final List<Triple> x = iter.toList() ;
//	                this.graphIter = WrappedIterator.create(x.iterator()) ;
//	                iter.close();
//	            }
//	            else
//	                // Stream.
//	                this.graphIter = iter ;
//	        }
//
//	        private static Node tripleNode(final Node node)
//	        {
//	            if ( node.isVariable() )
//	                return Node.ANY ;
//	            return node ;
//	        }
//
//	        private static Node substitute(final Node node, final Binding binding)
//	        {
//	            if ( Var.isVar(node) )
//	            {
//	                final Node x = binding.get(Var.alloc(node)) ;
//	                if ( x != null )
//	                    return x ;
//	            }
//	            return node ;
//	        }
//
//	        private Binding mapper(final Triple r)
//	        {
//	            final Binding results = new BindingMap(binding) ;
//
//	            if ( ! insert(s, r.getSubject(), results) )
//	                return null ; 
//	            if ( ! insert(p, r.getPredicate(), results) )
//	                return null ;
//	            if ( ! insert(o, r.getObject(), results) )
//	                return null ;
//	            return results ;
//	        }
//
//	        private static boolean insert(final Node inputNode, final Node outputNode, final Binding results)
//	        {
//	            if ( ! Var.isVar(inputNode) )
//	                return true ;
//	            
//	            final Var v = Var.alloc(inputNode) ;
//	            final Node x = results.get(v) ;
//	            if ( x != null )
//	                return outputNode.equals(x) ;
//	            
//	            results.add(v, outputNode) ;
//	            return true ;
//	        }
//	        
//	        @Override
//	        protected boolean hasNextBinding()
//	        {
//	            if ( finished ) return false ;
//	            if ( slot != null ) return true ;
//	            if ( cancelled )
//	            {
//	                graphIter.close() ;
//	                finished = true ;
//	                return false ;
//	            }
//
//	            while(graphIter.hasNext() && slot == null )
//	            {
//	                final Triple t = graphIter.next() ;
//	                slot = mapper(t) ;
//	            }
//	            if ( slot == null )
//	                finished = true ;
//	            return slot != null ;
//	        }
//
//	        @Override
//	        protected Binding moveToNextBinding()
//	        {
//	            if ( ! hasNextBinding() ) 
//	                throw new ARQInternalErrorException() ;
//	            final Binding r = slot ;
//	            slot = null ;
//	            return r ;
//	        }
//
//	        @Override
//	        protected void closeIterator()
//	        {
//	            if ( graphIter != null )
//	                NiceIterator.close(graphIter) ;
//	            graphIter = null ;
//	        }
//	        
//	        @Override
//	        protected void requestCancel()
//	        {
//	            // The QuryIteratorBase machinary will do the real work.
//	            // but we cleanly kill the ExtendedIterator.
//	            cancelled = true ;
//	        }
//	    }
//	}
