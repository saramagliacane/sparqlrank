package sparqlrank.iterators;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.graph.Graph;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.shared.PrefixMapping;
import com.hp.hpl.jena.sparql.ARQConstants;
import com.hp.hpl.jena.sparql.ARQInternalErrorException;
import com.hp.hpl.jena.sparql.core.BasicPattern;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.binding.BindingMap;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIter1;
import com.hp.hpl.jena.sparql.serializer.SerializationContext;
import com.hp.hpl.jena.sparql.util.Symbol;
import com.hp.hpl.jena.sparql.util.Utils;
import com.hp.hpl.jena.tdb.store.GraphTDBBase;

import org.openjena.atlas.io.IndentedWriter;

import sparqlrank.baseclasses.Predicate;
import sparqlrank.baseclasses.PredicateSet;
import sparqlrank.baseclasses.Score;
import sparqlrank.baseclasses.ScoreFunction;

/**
 * Physical implementation of the OpRank - MPro algorithm
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */


public class QueryIterRank extends QueryIter1 {
	
	private RankedQueue queue;
	private Predicate predicate;
	private String predicateName;
	private PredicateSet subpredicateset;
	
	private int rankedQueueSize;

	private PredicateSet restset;
	private PredicateSet restset2;
	private ScoreFunction scorefunction;
	private Score Threshold;
	
	//private SPARQLRankCache cache;
	private Boolean alreadySavedInCache = false;
		
	private Boolean useCache = false;
	//private int firstOutput = 0;

	// needed only for caching - what about taking them from the execution context?
	private Set<Var> vars;
	private String dataset = "";
	private BasicPattern subpattern;

	private Binding[] cache_array;
	private int cache_array_index;
	
	// TODO: NEED TO DEFINE JOINVARIABLE
	//private Var joinVariable = null;
	//private final Boolean useSequenceOptimization = false;
	// is ready after the first binding is output 
	//private ArrayList<Node> listOfJoinVariableValues = new ArrayList<Node>();
	
	public QueryIterRank(final QueryIterator qIter, final Predicate predicate,
			final ScoreFunction scoreFunction, final PredicateSet subpredicateset, final Set<Var> vars,  final ExecutionContext execCtx) {
		super(qIter,execCtx);
		new QueryIterRank( qIter,  predicate, scoreFunction,  subpredicateset,  vars,  null, execCtx);
	}
	
	public QueryIterRank(final QueryIterator qIter, final Predicate predicate, 
			final ScoreFunction scoreFunction, final PredicateSet subpredicateset, final Set<Var> vars, final BasicPattern subpattern,final ExecutionContext execCtx){
		
		super(qIter,execCtx);
		this.setPredicate(predicate);
		this.predicateName = predicate.toString();
		this.scorefunction = scoreFunction;
		
		// these two are needed for computing the Threshold
		// set of predicates of the sub operators
		this.subpredicateset = subpredicateset;		
		
		// the rest of the predicates contained in the global ScoreFunction (complementary)
		restset = new PredicateSet();
		restset.addAll(scorefunction.getPredicates());
		restset.removeAllPredicates(subpredicateset);
			
		// these two are needed for the RankedQueue
		// output ordering set of predicates
		final PredicateSet fullpredicateset = (PredicateSet) this.subpredicateset.clone();
		fullpredicateset.add(predicate);
		
		// the complementary set of predicates
		restset2 = (PredicateSet) restset.clone();
		restset2.removePredicate(predicate);
		
		this.queue = RankedQueue.create(fullpredicateset, restset2, scorefunction);

		this.vars = new HashSet<Var>();
		this.vars.addAll(vars);
		this.subpattern = subpattern;
		
		
		final Symbol useCacheSym = Symbol.create(ARQConstants.systemVarNS+"useCache") ;
		this.useCache = (this.subpredicateset.isEmpty() && execCtx.getContext().isTrue(useCacheSym));
		
//		final Symbol useSequenceOptimizationSym = Symbol.create(ARQConstants.systemVarNS+"useSequenceOptimization") ;
//		this.useSequenceOptimization = ( execCtx.getContext().isTrue(useSequenceOptimizationSym));
//				
//		if (useSequenceOptimization){
//		
//			// now by hand
//			for (final Var v: this.vars)
//				if (v.getName().contains("product")) 
//					this.joinVariable = v;
//			
//			if ((this.joinVariable== null)  || (!this.vars.contains(this.joinVariable)))
//				this.joinVariable = null;
//		}


		
		// we search in the cache if there are already some results
		if (this.useCache){
				
			final SPARQLRankCache cache = SPARQLRankCache.createSPARQLRankCache();
			
			final PredicateSet temp = new PredicateSet();			
			temp.add(predicate);
			
			final Graph g = execCtx.getDataset().getDefaultGraph();
			
			if (g instanceof GraphTDBBase)
				this.dataset = ((GraphTDBBase) g).getLocation().toString().replace("location:", "");
			
			//System.out.println(this.dataset);
			
			final SPARQLRankCacheKey key = new SPARQLRankCacheKey( vars, temp , dataset);
			
			cache_array = cache.get(key);
			
			//already in cache
			if((cache_array!= null) && !(cache_array.length == 0)){
				
//				for (int i = 0; i<array.length; i++){
//					queue.add(array[i]);
//				}

				// flush the queue
				Threshold = Score.immutableZeroScore;
				
//				if (useSequenceOptimization){
//					this.listOfJoinVariableValues = cache.getListOfJoinValues(key);
//				}
//				
				alreadySavedInCache = true;
				
			}

			
		}
	}


	@Override
	protected boolean hasNextBinding() {
		

	   if(alreadySavedInCache){
	      if (cache_array_index >= cache_array.length)
	           return false;
	      else
	           return true;
	   }
	        
	  // if there are no incoming bindings from sub ops and the queue is empty
	  if ( ! getInput().hasNext() && queue.isEmpty())
          return false ;
	  
	  if ( isFinished() )
          return false;
	  
      
	  return true;
     }
	
	
	

	@Override
	protected Binding moveToNextBinding() {
		
		// Main algorithm - fetch bindings until you find one whose Score 
		// is above the Threshold - the maximal-possible score of the last fetched binding 
		// calculated on the sub operator PredicateSet

		
	   if (!alreadySavedInCache && useCache){
	      addToCache(new ArrayList<Binding>());
	   }
	   
	   if(alreadySavedInCache) {
	         
	         final Binding outputBinding = cache_array[cache_array_index++];
	         Binding result = outputBinding;
	         
	         if (restset2.isEmpty()){
	            result = addScoreVariable(outputBinding, result);
	         }
	         
	         return result;
	   }
	   
      
      Binding result = getQueueResults();
		if (result!= null) return result;
		


		while(true){
	
			if (! getInput().hasNext()){
				// in order to empty the queue, we assign the Threshold the value of 0.0
				// it is assigned only once, because the next invocation of this method 
				// does not enter the loop
				Threshold = Score.immutableZeroScore;
			}
			else {
				
				final Binding b = getInput().next();
				
		
				// in order not to modify Binding we evaluate it on the fly
				// note: Threshold is evaluated with a different PredicateSet than the 
				// output PredicateSet
				Threshold = scorefunction.evaluate(b,subpredicateset, restset, this.getExecContext());
				
//				very old debugging
//				System.out.println(this+"fetched Binding : " + b + "\n Threshold: "+ Threshold.getDouble());	
				
				// add to the RankedQueue, which is ordered based on the maximal-possible 
				// score of the output PredicateSet
				queue.add(b);
				
//				//need for sequence optimization
//				if (useSequenceOptimization){
//					final Node nv = b.get(joinVariable);
//					if (!listOfJoinVariableValues.contains(nv))
//						listOfJoinVariableValues.add(nv);
//				}

			}
			
			if (queue.isEmpty()) {
				throw new ARQInternalErrorException("No next binding") ;
			}
	
			result = getQueueResults();
			if (result!= null) return result;
			
		}
	}
	
	
	protected Binding getQueueResults(){
		
		// what about taking it from the cache, which is the list of the queue?
		
		if (!queue.isEmpty()){
			if(Score.greaterThan(queue.getTopScore(this.getExecContext()), Threshold)){
				
				final Binding outputBinding = queue.remove();
				Binding result = outputBinding;
				
				// we have all the elements we need to define a final score
				if (restset2.isEmpty()){
				   result = addScoreVariable(outputBinding, result);
				}
					
//				// the first time we get a result
//				if((!alreadySavedInCache) && useCache) {
//					final List<Binding> temp = new ArrayList<Binding>();
//					temp.add(outputBinding);
//					addToCache(temp);
//				}

				
//				System.out.println("");
//				if(firstOutput<1000){
//					System.out.println(firstOutput +") Output Binding: " + this+ " :" + result);
//					firstOutput++;
//				}
				
				return result;
			}
				
		}
	
		return null;
	}
	
	
	private Binding addScoreVariable(final Binding outputBinding, Binding result){
      final Var score = Var.alloc("score");
      final Node nv = outputBinding.get(score);
      if (nv == null){
         final Score s = scorefunction.evaluate(outputBinding, scorefunction.getPredicates(), restset2, this.getExecContext());
         final Node node = Node.createLiteral(Double.toString(s.getDouble()), null, XSDDatatype.XSDdouble);
         
         // add the variable ?score to the output binding
         result = new BindingMap(outputBinding);
         result.add(score, node);
      }
      
      return result;
	}
	
	protected void addToCache(final List<Binding> additionalBindings){
		
		final SPARQLRankCache cache = SPARQLRankCache.createSPARQLRankCache();
	
		final SPARQLRankCacheKey key = new 
			SPARQLRankCacheKey(vars, queue.getPredicateSet(), dataset);
		
		// we need to extinguish the Input or the cache is not useful for the next operators	
		// usually the rank operator stops as soon as it finds a perfect score (1.0 for a single predicate)
		while ( getInput().hasNext()){
			final Binding b = getInput().next();
			queue.add(b);
			
//			//need for sequence optimization
//			if (useSequenceOptimization){
//				final Node nv = b.get(joinVariable);
//				if (!listOfJoinVariableValues.contains(nv))
//					listOfJoinVariableValues.add(nv);
//			}
		}
		
		
		cache_array = queue.returnArray(additionalBindings);		
		cache.put(key, cache_array);
		
		cache_array_index = 0;
		
//		if (useSequenceOptimization){
//			cache.putListOfJoinValues(key, listOfJoinVariableValues);
//		}

		
		alreadySavedInCache = true;

	}

	@Override
    protected void details(final IndentedWriter out, final SerializationContext sCxt)
    {
        out.print("[ "+ predicateName +"]");
    }
	
	@Override
	public String toString(final PrefixMapping pmap) {

		return Utils.className(this)+" "+ predicateName;
	}

	@Override
	public void output(final IndentedWriter out) {
	     out.print("(") ;
	     out.print(Utils.className(this)) ;
	     out.print(" ") ;
	     out.print(predicateName) ;
	     out.print(")") ;
	}
	
 
	@Override
	protected void closeSubIterator() {

	
		rankedQueueSize = queue.size();
		
		getInput().close();
		subpredicateset = null;
		queue = null;
		setPredicate(null);
		scorefunction = null;
		Threshold = null;
		restset = null;
		restset2 = null;
		vars = null;
		cache_array =null;
		
		
	}


	public void setPredicate(final Predicate predicate) {
		this.predicate = predicate;
	}


	public Predicate getPredicate() {
		return predicate;
	}

	public ScoreFunction getScoreFunction() {
		return scorefunction;
	}
	
//	public ArrayList getListOfJoinVariableValues(){
//		return listOfJoinVariableValues;
//	}
//	public void setListOfJoinVariableValues(final ArrayList l){
//		listOfJoinVariableValues = l;
//	}
//	
//	public ArrayList updateListOfJoinVariableValues(){
//		if (listOfJoinVariableValues.isEmpty() && this.useCache && !alreadySavedInCache){
//			addToCache(new ArrayList<Binding>());
//			Threshold = Score.immutableZeroScore;
//		}
//		return listOfJoinVariableValues;
//	}


	public int getRankedQueueSize(){
		return this.rankedQueueSize;
	}
	
	
	@Override
   protected void requestSubCancel() {
		// TODO Auto-generated method stub
		
	}
	
	public QueryIterator getSubIterator(){
		return this.getInput();
	}



	public BasicPattern getSubpattern() {
		return subpattern;
	}
	
	public PredicateSet getFullPredicateSet() {
	     final PredicateSet fullpredicateset = (PredicateSet) this.subpredicateset.clone();
	      fullpredicateset.add(predicate);
	      return fullpredicateset;
	}
	
	
	public PredicateSet getRestset2(){
	     // the complementary set of predicates
      restset2 = (PredicateSet) restset.clone();
      restset2.removePredicate(predicate);
      
      return restset2;
	}
	
//    // Pass iterator from one step directly into the next.
//    protected QueryIterator execute(OpSequence opSequence, QueryIterator input)
//    {
//        QueryIterator qIter = input ;
//        
//        for ( Iterator<Op> iter = opSequence.iterator() ; iter.hasNext() ; )
//        {
//            Op sub = iter.next() ;
//            qIter = executeOp(sub, qIter) ;
//        }
//        
//        return qIter ;
//    }
//	
//    protected QueryIterator execute(OpBGP opBGP, QueryIterator input)
//    {
//        BasicPattern pattern = opBGP.getPattern() ;
//        return StageBuilder.execute(pattern, input, execCxt) ;
//    }
}
