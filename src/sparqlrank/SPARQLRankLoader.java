package sparqlrank;

import sparqlrank.algebra.RankRewriterFactory;
import sparqlrank.baseclasses.ExecutionModel;
import sparqlrank.iterators.RankExecutorFactory;

/**
 * Helper class that can be used in the Joseki (or Fuseki) configuration to switch on the SPARQLRank mode instead of the standard ARQ execution.
 * Use: insert into the configuration file of Joseki the following line:
 * 	[] ja:loadClass "sparqlrank.SPARQLRankLoader" .
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class SPARQLRankLoader {
static {
	
	// TODO: remove after checking that is useless now
	ExecutionModel.createExecutionModel();
	
	// set as standard algebraic Rewriter the SPARQLRank Rewriter (which inserts OpRank, OpRankJoin, etc.)
	RankRewriterFactory.setSPARQLRankRewriter();
	
	// set as standard Execution Engine the SPARQLRank execution engine (which can handle OpRank, ..., and convert them into QueryIterRank, ...)
	RankExecutorFactory.setRankExecutor();
}
}