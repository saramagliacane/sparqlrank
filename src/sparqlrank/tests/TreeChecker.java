package sparqlrank.tests;

import sparqlrank.algebra.op.OpRank;


import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.op.Op1;
import com.hp.hpl.jena.sparql.algebra.op.Op2;
import com.hp.hpl.jena.sparql.algebra.op.OpExtend;
import com.hp.hpl.jena.sparql.algebra.op.OpOrder;

//used in several tests

public class TreeChecker{
	private int choice = 0;
	private final int checkOpOrder = 0;
	private final int checkNumRank = 2;
	private final int checkNumExtend = 3;
	
	private  int countRank = 0;
	private int limitRank = 0;
	private  int countExtend = 0;
	private int limitExtend = 0;
	
	public void setChoice (int i ){
		choice = i;
	}
	
	public void setChoice (String i ){
		if(i.equals("checkOpOrder")) choice = checkOpOrder;
		else if (i.equals("checkNumRank")) choice = checkNumRank;
		else if (i.equals("checkNumExtend")) choice = checkNumExtend;
	}
	
	public void setLimit (int i ){
		limitRank = i;
		limitExtend = i;
		countRank = 0;
		countExtend = 0;
		
	}
	
	// this is the method called
	public boolean check(Op op){
		
		// definetely stupid method
		if (choice == checkOpOrder) 
			return checkOpOrder(op);
		else if(choice == checkNumRank) return checkNumRank(op);
		else return checkNumExtend(op);
	}
	public boolean checkOpOrder(Op op){
		if (op instanceof OpRank) return false;
		else if (op instanceof OpOrder) return true;
		return false;
	}
	

	public boolean checkNumRank(Op op){
		if (op instanceof OpRank) countRank +=1;
		if (countRank >= limitRank) return true;
		else return false;
	}
	
	public boolean checkNumExtend(Op op){
		if (op instanceof OpExtend) countExtend +=1;
		if (countExtend >= limitExtend) return true;
		else return false;
	}
	
	
	public static boolean checkOpTree(Op optree, TreeChecker f){
		
		Boolean found = f.check(optree);
		
			if (optree instanceof Op1){
				Op1 op = (Op1) optree;
				Boolean temp = checkOpTree(op.getSubOp(),f);
				found = found||temp;
				
			}
			else if (optree instanceof Op2){
				Op2 op = (Op2) optree;

				Boolean left = checkOpTree(op.getLeft(),f);
				Boolean right = checkOpTree(op.getRight(),f);
				found = found||left||right;
			}
		return found;
		
	}
}
