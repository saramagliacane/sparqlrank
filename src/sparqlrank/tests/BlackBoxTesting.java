package sparqlrank.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.RDFNode;

import org.junit.After;
import org.junit.Before;

import sparqlrank.RankExecutionUtils;
import sparqlrank.TestParameters;
import sparqlrank.datasetmodificator.BSBMDataModificator;
import sparqlrank.iterators.SPARQLRankCache;

/**
 * JUnit Class that tests that the results from the SPARQL-Rank execution are the same as in the standard execution (both membership and order).
 * The testing is performed by executing a number of queries from a specified query directory and comparing the results for both
 * systems. It is the easiest way to see if things work.
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 *
 */

public class BlackBoxTesting {

	private static TestParameters param;
    
    private static int countModifiedResults;
    private static int countStandardResults;
    private static ResultSet rsStandard;
    private static ResultSet rsModified;
    
    private final static Boolean debug = false;
    private final static Boolean debugQueries = true;
    //private final static Boolean ENABLE_LOGGING = false;
    
    private static List<QuerySolution> standardBindings = new ArrayList<QuerySolution>();
    private static List<QuerySolution> modifiedBindings = new ArrayList<QuerySolution>();
	
	@Before 
	public void setUp() { 
		
			param = new TestParameters("-opt");
			param.setOutputresults(true);
			//param.setQueryDirName("queries2/");
	  }
	  
	@After
	public void tearDown(){
		rsStandard = null;
		rsModified = null;
		param = null;
		
	}
	
	@org.junit.Test 
	public void performBlackBoxTesting(){
		
	   ARQ.getContext().setTrue(ARQ.optTopNSorting);
		
	   performLoop();
		
		
//		for (String dataset: possibleDatasets){
//			
//			Model model = BSBMDataModificator.createModel(param);
//			checkResults();
//		}
		
	}
	
	
	public void performLoop(){	
		
//		if(false){
//		for(Integer numberOfOrderByVars : param.possibleNumberOfVars ){
//			
//			param.setNumberOfOrderByVars(numberOfOrderByVars);
//			
//			for (String dataset: param.possibleDatasets){
//    			
//    			param.setDataset(dataset);				
//    			Model model = BSBMDataModificator.createModel(param);
//				
//				for (int j = 0; j< param.getPower()*2; j++){
//					
//					// repeat the experiment repeat-times
//					for(int i = 0; i<param.getRepeat(); i++){
//						param.setK((int) ( Math.pow(10.0, j/2) / (2-(j%2))) + 1);
//						checkResults(model,  param);
//					}// end for repeat
//					
//				}// end for power
//				
//				model.close();
//				}// end for dataset
//
//				
//		}// end for numberOfVars		
//		
//		}//if false
		
		
		if(param.getQueryDirName() != null){
		
			for (final String dataset: param.possibleDatasets){
				
				param.setDataset(dataset);	
				final Model model = BSBMDataModificator.createModel(param);
				
			// experiments also on the set of predefined queries
				File queryDirectory;
				File[] listOfQueryFiles = {};
					
				try{
					queryDirectory = new File(param.getQueryDirName());
					listOfQueryFiles = queryDirectory.listFiles();
				}catch(final Exception e){
					// if the specified query directory does not exist 
					return;
				}
				

				Arrays.sort(listOfQueryFiles, new Comparator<File>(){
				    @Override
               public int compare(final File f1, final File f2)
				    {
				        return f1.getName().compareTo(f2.getName());
				    } });
				
				for (final File file: listOfQueryFiles){
						
						// while(true){
						// the query is not in the list 
//						if(! possibleQueries.contains(i)){i++; continue;} else{
						
					String queryName = file.getName();
						
						
					if(queryName.startsWith("query"))
						queryName = queryName.split("query")[1];
					else continue;
						
					if(queryName.endsWith(".txt"))
						queryName = queryName.split(".txt")[0];
					else continue;					
					
					
				    param.setQueryName(queryName);
//					    if(enableMainLogging)
			    	System.out.println("--- Query name " + queryName);
					    	
					final String baseString = RankExecutionUtils.readQueryFromFile(file);
					   	
					    // is in the list but is null 
					if(baseString == null) {
						System.out.println("[WARN] Query String is empty, quitting. ");
						return;
					}
						
					if(!baseString.contains("?score")){
						System.out.println("[WARN] Skipping query: " + queryName + ", because it doesn't define ?score.");
						continue;
					}
					
			      SPARQLRankCache.freeSPARQLRankCache();
						
					for (int j = param.getStartingpower(); j< param.getPower(); j++){
				    		
							// iterate on the value of K
						param.setK((int) ( Math.pow(10.0, j)));
						checkResults(model, param, baseString);


					}//for
				    	
					
				}
				model.close();
				
			}// end for dataset
			
		}
			
			

	    	
		

    	
    }
    


//public static void checkResults(Model model, TestParameters param){
//	
//
//	
//	int k = param.getK();
//	
//	param.setModifiedExecution(true);
//	String queryString = ExampleQueryCreator.createQueryString(param.getK(), param.getNumberOfOrderByVars());
//	if (queryString == null) return ;
//	
//	if(ENABLE_LOGGING)
//		System.out.println("Query num: " + param.getQueryName() + 
//				" limit: "+ param.getK()+
//				" count: " + param.getCount());
//	
//	rsModified = RankExecutionUtils.executeQueryAndGetResults(model, queryString, param);
//	
//	// take all results for standard execution
//	param.setModifiedExecution(false);
//	param.setK(-1);
//	queryString = ExampleQueryCreator.createQueryString(param.getK(), param.getNumberOfOrderByVars());
//	if (queryString == null) return ;
//	rsStandard = RankExecutionUtils.executeQueryAndGetResults(model, queryString, param);
//	
//	param.setK(k);
//	
//	assertTrue(rsModified.hasNext());
//	assertTrue(rsStandard.hasNext());
//		
//	checkResultsWorker(param.getK());
//}


public static void checkResults(final Model model, final TestParameters param, final String queryString){
	

	
	final int k = param.getK();

	String modString;
	
	if (k<=0) modString = queryString + " LIMIT 1";
	else modString =  queryString + " LIMIT " + k + "\n";

	
	param.setModifiedExecution(true);
	rsModified = RankExecutionUtils.executeQueryAndGetResults(model, modString, param);
	
	if(debugQueries) System.out.println("Modified execution:" + modString);
	
	// take all results for standard execution
	param.setModifiedExecution(false);
	rsStandard = RankExecutionUtils.executeQueryAndGetResults(model, queryString, param);
	
	if(debugQueries) System.out.println("Standard execution:" + queryString);

	assertTrue(rsModified.hasNext());
	assertTrue(rsStandard.hasNext());
		
	checkResultsWorker(param.getK());
}


public static void checkResultsWorker(final int k){
	
	countModifiedResults = 0;
	countStandardResults = 0;
	
	standardBindings.clear();
	modifiedBindings.clear();
	
	assertTrue(checkResultsWorker(-1, -1, null, null, k));
	



}


	
	
	
public static Boolean checkResultsWorker( double standardThreshold, double modifiedThreshold, 
		QuerySolution nextStandardSolution, QuerySolution nextModifiedSolution, final int k){	
	

	
	final List<QuerySolution> standardList = new ArrayList<QuerySolution>();
	final List<QuerySolution> modifiedList = new ArrayList<QuerySolution>();
	
	if(nextStandardSolution!= null) {
		standardList.add(nextStandardSolution);
		countStandardResults++;
		nextStandardSolution = null;	
	}
	if(nextModifiedSolution!= null) {
		modifiedList.add(nextModifiedSolution);
		nextModifiedSolution = null;
	}
	
	double nextStandardThreshold = standardThreshold, nextModifiedThreshold = modifiedThreshold;
	
	// solutions with the same score 
	while (rsStandard.hasNext()){
 		nextStandardSolution = rsStandard.next();
 		nextStandardThreshold = nextStandardSolution.get("score").asLiteral().getDouble();
   		if (standardThreshold == -1) standardThreshold = nextStandardThreshold;
   		
   		assertFalse(nextStandardThreshold > standardThreshold) ;
   		
		if (nextStandardThreshold < standardThreshold)
   			break;
		else{
			standardList.add(nextStandardSolution);
			countStandardResults++;
			// need to exit in the end
			nextStandardSolution = null;
		}

	}
	
	// solutions with the same score 
	while (rsModified.hasNext()){
 		nextModifiedSolution = rsModified.next();
 		nextModifiedThreshold = nextModifiedSolution.get("score").asLiteral().getDouble();
   		
   		
   	if (modifiedThreshold == -1) modifiedThreshold = nextModifiedThreshold;
         
   	
   	if (nextModifiedThreshold > modifiedThreshold){
   	   System.out.println("NEXT VALUE WRONG:" + nextModifiedThreshold +  "-current Threshold " +modifiedThreshold);
   	}
      
   		
		if (nextModifiedThreshold < modifiedThreshold)
			break;
		else
			modifiedList.add(nextModifiedSolution);
		
	}
	

	
	standardBindings.addAll(standardList);
//	assertTrue(standardBindings.size()>=k);
	modifiedBindings.addAll(modifiedList);
//	assertTrue(modifiedBindings.size()>=k);

	if(debug){
		System.out.println("Standard solution:");
		for (final QuerySolution std: standardBindings)
			System.out.println(std);
		
		
		System.out.println("Modified solution:");
		for (final QuerySolution std: modifiedBindings)
			System.out.println(std);
	}
	
	
	return checkSolutions( standardList, modifiedList , nextStandardThreshold, nextModifiedThreshold, 
			nextStandardSolution, nextModifiedSolution, k);
	
}

public static Boolean checkSolutions( final List<QuerySolution> standardList, final List<QuerySolution> modifiedList , final double nextStandardThreshold, final double nextModifiedThreshold, 
		final QuerySolution nextStandardSolution, final QuerySolution nextModifiedSolution, final int k){	
	
	Boolean resultsOk = true;
	
	// all solutions of the modified resultset are contained in the original resultset
	for (final QuerySolution mod: modifiedList){
		Boolean found = false;
		for (final QuerySolution std: standardList ){
			if(checkEqualityOfSolutions(std,mod, k)) {
				found = true;
				break;
			}
		}
		assertTrue(found);
	}
	
 	
	// the standard solutions have to be contained in the modified resultset, except if the number of modified results seen so far
	// exceeds k
	for (final QuerySolution std: standardList){
		Boolean found = false;
		for (final QuerySolution mod: modifiedList ){
			if(checkEqualityOfSolutions(std,mod, k)) {
				found = true;
				break;
			}
		}
		if(found == true) 
			countModifiedResults++;
		resultsOk = resultsOk && found;    		
	}
	
	// if we found enough results, don't bother to check resultsOk
	if ((countModifiedResults==k)||((countModifiedResults == countStandardResults ) && (nextStandardSolution == null) ))
		return true;
	// else check it
	else{
		// the thresholds are not changing-> no more inputs (ending condition)
		//assertFalse((nextStandardThreshold == standardThreshold || nextModifiedThreshold == modifiedThreshold));
		
		assertTrue(resultsOk);
		
		return checkResultsWorker(nextStandardThreshold, nextModifiedThreshold, 
					nextStandardSolution, nextModifiedSolution, k);
	}

	
	
}

public static Boolean checkEqualityOfSolutions(final QuerySolution s1, final QuerySolution s2, final int k){

	final Boolean value1 = checkEqualityWorker(s1, s2, k);
	final Boolean value2 =  checkEqualityWorker(s2,s1, k);
	
	if(false && debug && !value1) System.out.println("check: " +s1 + "\n "+ s2 + "value:" + value1);
	
	
	return value1 && value2;
}

private static Boolean checkEqualityWorker(final QuerySolution s1, final QuerySolution s2, final int k){
	Boolean check = true;
	
	final Iterator<String> i = s1.varNames();
	for ( ; i.hasNext();){
		
		if(!check) return false;
		   		
		final String varname = i.next();
		
		if (varname.equals("score")){
			final RDFNode n1 = s1.get(varname);
			final RDFNode n2 = s2.get(varname);
			
			
			//TODO o tutto double o tutto float
			final Object v1 = n1.asLiteral().getValue();
			final Object v2 = n2.asLiteral().getValue();
			
			Double temp1, temp2, result;
			if(v1 instanceof Double){	temp1 = (Double) v1; }
			else {	
			   temp1 = Double.parseDouble(Float.toString((Float) v1));	}
			
			if(v2 instanceof Double){	temp2 = (Double) v2; }
			else {	
			   temp2 = Double.parseDouble(Float.toString((Float) v2));	}
			
			//check = (temp1 == temp2);
			
			result = Math.abs(temp1 - temp2);
			check = (result<= 0.0001);


		}    			
		else if(!s2.contains(varname)) 
			return false;
		else{
			final RDFNode n1 = s1.get(varname);
			final RDFNode n2 = s2.get(varname);
			check = n1.equals(n2);
		}
		
		
	}
	return check;
	
}

}
