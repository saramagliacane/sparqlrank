package sparqlrank.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import sparqlrank.algebra.TransformExtendOrder;
import sparqlrank.algebra.TransformReorderExtend;
import sparqlrank.algebra.TransformSplitExtend;

import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.Transformer;

public class TransformExtendOrderTest extends TransformBaseTest {

	
	@Before
	public void setUp() throws Exception {
		
		super.setUp();

		for(int i = 0; i< optree.size(); i++){
			Op tempOp = optree.get(i);
			tempOp = Transformer.transform(new TransformSplitExtend(), tempOp);
			optree.remove(i);
			optree.add(i,Transformer.transform(new TransformReorderExtend(), tempOp));
		}

	
	}
	
	@After
	public void tearDown() throws Exception {
		
		super.tearDown();
	}

	@Test
	public void testTransformOpExtendOp() {
		
		Op tempOp;
		
		/*
		 * TARGET QUERY 0
		 */
		int i = 0;
		
		tempOp = optree.get(i);
		f.setChoice("checkNumExtend");
		f.setLimit(1);
		assertFalse("The op tree should not contain OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		tempOp = printAndCalculate(tempOp,i, new TransformExtendOrder());

		assertFalse("The op tree should not contain OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		
		/*
		 * TARGET QUERY 1
		 */
		i++;
		tempOp = optree.get(i);
		f.setLimit(2);
		assertTrue("The op tree should contain at least two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(3);
		assertFalse("The op tree should now not contain three OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		
		tempOp = printAndCalculate(tempOp,i, new TransformExtendOrder());
		
		f.setLimit(1);
		assertTrue("The op tree should contain at least one OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should now not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		/*
		 * TARGET QUERY 2
		 */
		i++;
		tempOp = optree.get(i);
		
		f.setLimit(2);
		assertTrue("The op tree should contain at least two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(3);
		assertFalse("The op tree should now not contain three OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		
		tempOp = printAndCalculate(tempOp,i, new TransformExtendOrder());
		
		f.setLimit(1);
		assertTrue("The op tree should contain at least one OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should now not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		/*
		 * TARGET QUERY 3
		 */
		i++;
		tempOp = optree.get(i);

		f.setLimit(2);
		assertTrue("The op tree should contain at least two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(3);
		assertFalse("The op tree should now not contain three OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		tempOp = printAndCalculate(tempOp,i, new TransformExtendOrder());
		
		f.setLimit(1);
		assertTrue("The op tree should contain at least one OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should now not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		/*
		 * TARGET QUERY 4
		 */
		i++;
		tempOp = optree.get(i);

		f.setLimit(4);
		assertTrue("The op tree should now contain at least four OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(5);
		assertFalse("The op tree should not contain five OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		
		tempOp = printAndCalculate(tempOp,i, new TransformExtendOrder());
		
		
		f.setLimit(3);
		assertTrue("The op tree should now contain at least three OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(4);
		assertFalse("The op tree should not contain four OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		
		/*
		 * TARGET QUERY 5
		 */
		i++;
		tempOp = optree.get(i);
		f.setLimit(1);
		assertTrue("The op tree should contain at least OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		tempOp = printAndCalculate(tempOp,i, new TransformExtendOrder());
		
		
		f.setLimit(1);
		assertTrue("The op tree should contain at least OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));

		
		



		
	}
}
