package sparqlrank.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import sparqlrank.algebra.TransformExtendOrder;
import sparqlrank.algebra.TransformPushExtend;
import sparqlrank.algebra.TransformReorderExtend;
import sparqlrank.algebra.TransformSplitExtend;
import sparqlrank.baseclasses.ExecutionModel;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.Syntax;
import com.hp.hpl.jena.sparql.algebra.Algebra;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.OpVars;
import com.hp.hpl.jena.sparql.algebra.Transform;
import com.hp.hpl.jena.sparql.algebra.Transformer;
import com.hp.hpl.jena.sparql.algebra.op.Op0;
import com.hp.hpl.jena.sparql.algebra.op.Op1;
import com.hp.hpl.jena.sparql.algebra.op.Op2;
import com.hp.hpl.jena.sparql.algebra.op.OpExtend;
import com.hp.hpl.jena.sparql.algebra.op.OpN;
import com.hp.hpl.jena.sparql.core.Var;

public class TransformPushExtendTest extends TransformBaseTest{

	//TODO: check better if the position in the tree is optimal
	
	@Before
	public void setUp() throws Exception {
		
		super.setUp();

		for(int i = 0; i< optree.size(); i++){
			Op tempOp = optree.get(i);
			tempOp = Transformer.transform(new TransformSplitExtend(), tempOp);
			tempOp = Transformer.transform(new TransformReorderExtend(), tempOp);
			optree.remove(i);
			optree.add(i,Transformer.transform(new TransformExtendOrder(), tempOp));
		}

	
	}

	@After
	public void tearDown() throws Exception {
		
		super.tearDown();
	}

	@Test
	public void testTransformOpExtendOp() {
		
		Op tempOp;
		
		/*
		 * TARGET QUERY 0
		 */
		int i = 0;
		
		tempOp = optree.get(i);
		f.setChoice("checkNumExtend");
		f.setLimit(1);
		assertFalse("The op tree should not contain OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		tempOp = printAndCalculate(tempOp,i, new TransformPushExtend());

		assertFalse("The op tree should not contain OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		
		/*
		 * TARGET QUERY 1
		 */
		i++;
		tempOp = optree.get(i);
		f.setLimit(1);
		assertTrue("The op tree should contain at least one OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should now not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		
		tempOp = printAndCalculate(tempOp,i, new TransformPushExtend());
		
		f.setLimit(1);
		assertTrue("The op tree should contain at least one OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should now not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		/*
		 * TARGET QUERY 2
		 */
		i++;
		tempOp = optree.get(i);
		
		f.setLimit(1);
		assertTrue("The op tree should contain at least one OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should now not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		
		tempOp = printAndCalculate(tempOp,i, new TransformPushExtend());
		
		f.setLimit(1);
		assertTrue("The op tree should contain at least one OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should now not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		/*
		 * TARGET QUERY 3
		 */
		i++;
		tempOp = optree.get(i);

		f.setLimit(1);
		assertTrue("The op tree should contain at least one OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should now not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		tempOp = printAndCalculate(tempOp,i, new TransformPushExtend());
		
		f.setLimit(1);
		assertTrue("The op tree should contain at least one OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should now not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		/*
		 * TARGET QUERY 4
		 */
		i++;
		tempOp = optree.get(i);

		f.setLimit(3);
		assertTrue("The op tree should now contain at least three OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(4);
		assertFalse("The op tree should not contain four OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		
		tempOp = printAndCalculate(tempOp,i, new TransformPushExtend());
		
		
		f.setLimit(3);
		assertTrue("The op tree should now contain at least three OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(4);
		assertFalse("The op tree should not contain four OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		
		/*
		 * TARGET QUERY 5
		 */
		i++;
		tempOp = optree.get(i);
		f.setLimit(1);
		assertTrue("The op tree should contain at least OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		tempOp = printAndCalculate(tempOp,i, new TransformPushExtend());
		
		
		f.setLimit(1);
		assertTrue("The op tree should contain at least OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));

		
	}
	



	protected Op printAndCalculate(Op tempOp, int i, TransformPushExtend t) {
		System.out.println("Transformation: " + t.getClass());
		System.out.println("BEFORE " + i);
		System.out.println(tempOp);
		optree.remove(i);
		optree.add(i, t.repeatedApply(tempOp));
		tempOp = optree.get(i);
		System.out.println("AFTER " + i);
		System.out.println(tempOp);
		System.out.println("");
		return tempOp;
	}
	


}
