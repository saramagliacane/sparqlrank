package sparqlrank.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import sparqlrank.algebra.TransformExtendOrder;
import sparqlrank.algebra.TransformPushBinary;
import sparqlrank.algebra.TransformPushExtend;
import sparqlrank.algebra.TransformReorderExtend;
import sparqlrank.algebra.TransformSplitExtend;
import sparqlrank.algebra.TransformSplitRank;
import sparqlrank.baseclasses.ExecutionModel;

import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.Transformer;

public class TransformPushBinaryTest extends TransformBaseTest{

	@Before
	public void setUp() throws Exception {
		super.setUp();
		
		for(int i = 0; i< optree.size(); i++){
			Op tempOp = optree.get(i);
			tempOp = Transformer.transform(new TransformSplitExtend(), tempOp);
			tempOp = Transformer.transform(new TransformReorderExtend(), tempOp);
			tempOp = Transformer.transform(new TransformExtendOrder(), tempOp);	
			tempOp = (new TransformPushExtend()).repeatedApply(tempOp);
	        
			optree.remove(i);
			optree.add(i,Transformer.transform(new TransformSplitRank(), tempOp));
		}
		
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void testTransformPushBinaryWorkerOpRankOpJoin() {
		
		
		Op tempOp;
		
		/*
		 * TARGET QUERY 0
		 */
		int i = 0;
		
		tempOp = optree.get(i);

		f.setChoice("checkOpOrder");
		f.setLimit(1);
		assertFalse("The op tree should not contain any OpOrder", TreeChecker.checkOpTree(tempOp, f ));
		f.setChoice("checkNumRank");
		f.setLimit(1);
		assertTrue("The op tree should contain at least one OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should not contain two OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setChoice("checkNumExtend");
		assertFalse("The op tree should not contain OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		tempOp = printAndCalculate(tempOp,i, new TransformPushBinary());

		f.setChoice("checkOpOrder");
		f.setLimit(1);
		assertFalse("The op tree should not contain any OpOrder", TreeChecker.checkOpTree(tempOp, f ));
		f.setChoice("checkNumRank");
		f.setLimit(1);
		assertTrue("The op tree should contain at least one OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should not contain two OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setChoice("checkNumExtend");
		assertFalse("The op tree should not contain OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		
		/*
		 * TARGET QUERY 1
		 */
		i++;
		tempOp = optree.get(i);

		f.setChoice("checkOpOrder");
		f.setLimit(1);
		assertFalse("The op tree should not contain any OpOrder", TreeChecker.checkOpTree(tempOp, f ));
		f.setChoice("checkNumRank");
		f.setLimit(3);
		assertTrue("The op tree should contain at least three OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(4);
		assertFalse("The op tree should not contain four OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setChoice("checkNumExtend");
		f.setLimit(1);
		assertTrue("The op tree should contain at least one OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should now not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		
		tempOp = printAndCalculate(tempOp,i, new TransformPushBinary());
		
		f.setChoice("checkOpOrder");
		f.setLimit(1);
		assertFalse("The op tree should not contain any OpOrder", TreeChecker.checkOpTree(tempOp, f ));
		f.setChoice("checkNumRank");
		f.setLimit(3);
		assertTrue("The op tree should contain at least three OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(4);
		assertFalse("The op tree should not contain four OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setChoice("checkNumExtend");
		f.setLimit(1);
		assertTrue("The op tree should contain at least one OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should now not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		/*
		 * TARGET QUERY 2
		 */
		i++;
		tempOp = optree.get(i);
		
		
		f.setChoice("checkOpOrder");
		f.setLimit(1);
		assertFalse("The op tree should not contain any OpOrder", TreeChecker.checkOpTree(tempOp, f ));
		f.setChoice("checkNumRank");
		f.setLimit(3);
		assertTrue("The op tree should contain at least three OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(4);
		assertFalse("The op tree should not contain four OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setChoice("checkNumExtend");
		f.setLimit(1);
		assertTrue("The op tree should contain at least one OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should now not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		
		tempOp = printAndCalculate(tempOp,i, new TransformPushBinary());
		
		f.setChoice("checkOpOrder");
		f.setLimit(1);
		assertFalse("The op tree should not contain any OpOrder", TreeChecker.checkOpTree(tempOp, f ));
		f.setChoice("checkNumRank");
		f.setLimit(3);
		assertTrue("The op tree should contain at least three OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(4);
		assertFalse("The op tree should not contain four OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setChoice("checkNumExtend");
		f.setLimit(1);
		assertTrue("The op tree should contain at least one OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should now not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		/*
		 * TARGET QUERY 3
		 */
		i++;
		tempOp = optree.get(i);

		f.setChoice("checkOpOrder");
		f.setLimit(1);
		assertFalse("The op tree should not contain any OpOrder", TreeChecker.checkOpTree(tempOp, f ));
		f.setChoice("checkNumRank");
		f.setLimit(3);
		assertTrue("The op tree should contain at least three OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(4);
		assertFalse("The op tree should not contain four OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setChoice("checkNumExtend");
		f.setLimit(1);
		assertTrue("The op tree should contain at least one OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should now not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		tempOp = printAndCalculate(tempOp,i, new TransformPushBinary());
		
		f.setChoice("checkOpOrder");
		f.setLimit(1);
		assertFalse("The op tree should not contain any OpOrder", TreeChecker.checkOpTree(tempOp, f ));
		f.setChoice("checkNumRank");
		f.setLimit(3);
		assertTrue("The op tree should contain at least three OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(4);
		assertFalse("The op tree should not contain four OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setChoice("checkNumExtend");
		f.setLimit(1);
		assertTrue("The op tree should contain at least one OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should now not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		/*
		 * TARGET QUERY 4
		 */
		i++;
		tempOp = optree.get(i);


		f.setChoice("checkOpOrder");
		f.setLimit(1);
		assertFalse("The op tree should not contain any OpOrder", TreeChecker.checkOpTree(tempOp, f ));
		f.setChoice("checkNumRank");
		f.setLimit(3);
		assertTrue("The op tree should contain at least three OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(4);
		assertFalse("The op tree should not contain four OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setChoice("checkNumExtend");
		f.setLimit(3);
		assertTrue("The op tree should now contain at least three OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(4);
		assertFalse("The op tree should not contain four OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		
		tempOp = printAndCalculate(tempOp,i, new TransformPushBinary());
		
		f.setChoice("checkOpOrder");
		f.setLimit(1);
		assertFalse("The op tree should not contain any OpOrder", TreeChecker.checkOpTree(tempOp, f ));
		f.setChoice("checkNumRank");
		f.setLimit(3);
		assertTrue("The op tree should contain at least three OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(4);
		assertFalse("The op tree should not contain four OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setChoice("checkNumExtend");
		f.setLimit(3);
		assertTrue("The op tree should now contain at least three OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(4);
		assertFalse("The op tree should not contain four OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		
		/*
		 * TARGET QUERY 5
		 */
		i++;
		tempOp = optree.get(i);
		
		f.setChoice("checkOpOrder");
		f.setLimit(1);
		assertFalse("The op tree should not contain any OpOrder", TreeChecker.checkOpTree(tempOp, f ));
		f.setChoice("checkNumRank");
		f.setLimit(3);
		assertTrue("The op tree should contain at least three OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(4);
		assertFalse("The op tree should not contain four OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setChoice("checkNumExtend");
		f.setLimit(1);
		assertTrue("The op tree should contain at least OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		tempOp = printAndCalculate(tempOp,i, new TransformPushBinary());
		
		f.setChoice("checkOpOrder");
		f.setLimit(1);
		assertFalse("The op tree should not contain any OpOrder", TreeChecker.checkOpTree(tempOp, f ));
		f.setChoice("checkNumRank");
		f.setLimit(3);
		assertTrue("The op tree should contain at least three OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(4);
		assertFalse("The op tree should not contain four OpRank", TreeChecker.checkOpTree(tempOp,  f));
		f.setChoice("checkNumExtend");
		f.setLimit(1);
		assertTrue("The op tree should contain at least OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));

		
		



		

	}
		
		


}
