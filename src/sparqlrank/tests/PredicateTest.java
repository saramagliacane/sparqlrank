package sparqlrank.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;

import sparqlrank.baseclasses.Predicate;
import sparqlrank.baseclasses.PredicateSet;


import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.query.SortCondition;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.binding.BindingFactory;
import com.hp.hpl.jena.sparql.expr.E_Add;
import com.hp.hpl.jena.sparql.expr.Expr;
import com.hp.hpl.jena.sparql.expr.ExprVar;
import com.hp.hpl.jena.sparql.function.FunctionEnv;
import com.hp.hpl.jena.sparql.util.Utils;


public class PredicateTest {
	
	Predicate p1, p2, p3 ,p4,  p5 , p6, p7;
	
	Expr e, e1, e2, e3, e4, e5;
	Var v1, v2, v3, v4, v5;
	SortCondition s1, s2, s3, s4, s5, s6, s7;
	List<SortCondition> conditions = new ArrayList<SortCondition>();
	PredicateSet predset1, predset2;
	
	Binding b1,	b2;
	
    @Before 
    public void setUp() { 
    	
    	v1 = Var.alloc("p1");
    	v2 = Var.alloc("p2");
    	v3 = Var.alloc("p3");
    	
    	b1 = BindingFactory.create();
    	b2 = BindingFactory.create();
    	b1.add(v1, Node.createLiteral("0.1", null, XSDDatatype.XSDdouble));
    	b1.add(v2, Node.createLiteral("0.4", null, XSDDatatype.XSDdouble));
    	b2.add(v1, Node.createLiteral("0.2", null, XSDDatatype.XSDdouble));
    	b2.add(v2, Node.createLiteral("0.7", null, XSDDatatype.XSDdouble));
    	
		e = new ExprVar(v1);
		e1 = new ExprVar(v2);
		e2 = new E_Add(e, e1);
		e3 = new E_Add(e2, e1);
		e4 = new ExprVar(v3);
		e5 = new E_Add(e1, e4);
		int direction = 1;
		s1 = new SortCondition(e, direction);
		s2 = new SortCondition(e, direction);
		s3 = new SortCondition(e1, -direction);
		s4 = new SortCondition(e2, direction);
		s5 = new SortCondition(e3, direction); 
		s6 = new SortCondition(e4, direction);
		s7 = new SortCondition(e5, direction);
		p1 = new Predicate(s1);
		p2 = new Predicate(s2);
		p3 = new Predicate(s3);
		p4 = new Predicate(s4);
		p5 = new Predicate(s5);
		p6 = new Predicate(s6);
		p7 = new Predicate(s7);
		
		conditions.add(s1);
		conditions.add(s2);
		conditions.add(s3);
		predset1 = PredicateSet.create(conditions);
		predset2 = new PredicateSet();
		predset2.add(p1);
		predset2.add(p4);
		predset2.add(p5);

		

    }

	
	@org.junit.Test 
	public void checkPredicateClass(){
		assertTrue("Two predicates with equivalent SortCondition and same direction are equal",p1.equals(p2));	
		assertFalse("Two predicates with equivalent SortCondition and opposite direction are not equal",p1.equals(p3));
		
		assertTrue( "Predicate p1 should contain v1" ,p1.getVars().contains(v1) );
		assertTrue( "Predicate p4 should contain v2", p4.getVars().contains(v2) );
		assertTrue( "Predicate p4 should contain v1", p4.getVars().contains(v1) );
		assertTrue( "Predicate p5 should contain v1", p5.getVars().contains(v1) );
		assertTrue( "Predicate p5 should contain v2", p5.getVars().contains(v2) );

		
		assertEquals("The intersection between the two predicate sets should contain one predicate",
				PredicateSet.intersection(predset1, predset2).size() , 1 );
		assertTrue("The intersection between the two predicate sets should contain p1",
				PredicateSet.intersection(predset1, predset2).contains(p1) );
		
		PredicateSet union = PredicateSet.union(predset1, predset2);
		assertTrue(union.contains(p1) );
		assertTrue(union.containsSimilar(p2) );
		assertTrue(union.containsSimilar(p3) );
		assertTrue(union.contains(p4) );
		assertTrue(union.contains(p5) );
		assertFalse((predset1.removePredicate(p1)).containsSimilar(p1) );
		
		predset1.add(p1);
		
		predset1.removeAllPredicates(predset2);
		assertEquals (predset1.size(), 1);
		assertFalse (predset1.containsSimilar(p1));
		assertFalse (predset1.containsSimilar(p2));
		assertTrue(predset1.containsSimilar(p3));
		assertFalse (predset1.containsSimilar(p4));
		assertFalse (predset1.containsSimilar(p5));
		
		assertEquals((p1.eval(b1, null)).getDouble(),0.1,0);
		assertEquals((p3.eval(b1, null)).getDouble(),0.4,0);
		assertEquals((p4.eval(b1, null)).getDouble(), 0.5,0);
		assertEquals((p5.eval(b1, null)).getDouble(), 0.9,0);
		assertEquals((p6.eval(b1, null)).getDouble(), 1.0,0);
		assertEquals((p7.eval(b1, null)).getDouble(), 1.0,0);

	}
	
	
    @After 
    public void tearDown() { 
		e = null;
		e1 = null;
		e2 = null;
		e3 = null;
		p1 = null;
		p2 = null;
		p3 = null;
		p4 = null;
		p5 = null;
    }

}
