package sparqlrank.tests;


import org.junit.Test;


import junit.framework.TestResult;
import junit.framework.TestSuite;



public class TransformTestSuite {
	
	public static TestSuite suite() {

        TestSuite suite = new TestSuite();
  
        //
        // The ShoppingCartTest we created above.
        //
        suite.addTestSuite(TransformSplitExtendTest.class);
        suite.addTestSuite( TransformExtendOrderTest.class);
        suite.addTestSuite( TransformPushExtendTest.class);
        suite.addTestSuite(TransformSplitRankTest.class);
        suite.addTestSuite(TransformPushBinaryTest.class);
        //
        // Add more tests here
        //

        return suite;
    }
	
    public static void main(String[] args) {
        junit.textui.TestRunner.run(suite());
    }


	

}
