package sparqlrank.tests;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import sparqlrank.algebra.TransformSplitExtend;


import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.Transformer;

public class TransformSplitExtendTest extends TransformBaseTest {

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void testTransformOpExtendOp() {
		
		Op tempOp;
		
		/*
		 * TARGET QUERY 0
		 */
		int i = 0;
		
		tempOp = optree.get(i);
		f.setChoice("checkNumExtend");
		f.setLimit(1);
		assertFalse("The op tree should not contain OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		tempOp = printAndCalculate(tempOp,i, new TransformSplitExtend());
		
		
		assertFalse("The op tree should not contain OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		
		/*
		 * TARGET QUERY 1
		 */
		i++;
		tempOp = optree.get(i);
		f.setLimit(1);
		assertTrue("The op tree should contain at least OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		tempOp = printAndCalculate(tempOp,i, new TransformSplitExtend());
		
		assertTrue("The op tree should contain at least two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(3);
		assertFalse("The op tree should now not contain three OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		/*
		 * TARGET QUERY 2
		 */
		i++;
		tempOp = optree.get(i);
		f.setLimit(1);
		assertTrue("The op tree should contain at least OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		tempOp = printAndCalculate(tempOp,i, new TransformSplitExtend());
		
		assertTrue("The op tree should now contain at least two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(3);
		assertFalse("The op tree should not contain three OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		/*
		 * TARGET QUERY 3
		 */
		i++;
		tempOp = optree.get(i);
		f.setLimit(1);
		assertTrue("The op tree should contain at least OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		tempOp = printAndCalculate(tempOp,i, new TransformSplitExtend());
		
		assertTrue("The op tree should now contain at least two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(3);
		assertFalse("The op tree should not contain three OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		/*
		 * TARGET QUERY 4
		 */
		i++;
		tempOp = optree.get(i);
		f.setLimit(1);
		assertTrue("The op tree should contain at least OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		tempOp = printAndCalculate(tempOp,i, new TransformSplitExtend());
		
		
		f.setLimit(4);
		assertTrue("The op tree should now contain at least four OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(5);
		assertFalse("The op tree should not contain five OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		
		/*
		 * TARGET QUERY 5
		 */
		i++;
		tempOp = optree.get(i);
		f.setLimit(1);
		assertTrue("The op tree should contain at least OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		
		tempOp = printAndCalculate(tempOp,i, new TransformSplitExtend());
		
		
		f.setLimit(1);
		assertTrue("The op tree should contain at least OpExtend", TreeChecker.checkOpTree(tempOp,  f));
		f.setLimit(2);
		assertFalse("The op tree should not contain two OpExtend", TreeChecker.checkOpTree(tempOp,  f));


	
	}
}
