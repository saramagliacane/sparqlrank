package sparqlrank.tests;


import java.util.ArrayList;
import java.util.List;

import junit.framework.TestResult;

import org.junit.After;
import org.junit.Before;

import sparqlrank.baseclasses.ExecutionModel;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.Syntax;
import com.hp.hpl.jena.sparql.algebra.Algebra;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.Transform;
import com.hp.hpl.jena.sparql.algebra.Transformer;

public class TransformBaseTest extends junit.framework.TestCase {

	protected TreeChecker f;
	
	protected List<Op> optree = new ArrayList<Op>();


	@Before
	public void setUp() throws Exception {
		
		 String targetString0 = "prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> " +
		    "prefix xsd: <http://www.w3.org/2001/XMLSchema#>"+
		    "select ?offer ?vendor ?producer" +
			"?normnumrevprod "+
			"?avgscore1"+
			"{" +
				"{?offer bsbm:vendor ?vendor ."+
				"?offer bsbm:product ?product ." +
				"?offer bsbm:price ?price }." +
				"{?product bsbm:producer  <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/dataFromProducer13/Producer13> ." +
				" <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/dataFromProducer13/Producer13> bsbm:normnumreviews ?normnumrevprod ."+
				"?product bsbm:avgscore1 ?avgscore1 .}" +


				"} ORDER BY DESC(?avgscore) LIMIT " + 10;
		
		 String targetString = "prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> " +
		    "prefix xsd: <http://www.w3.org/2001/XMLSchema#>"+
		    "select ?offer ?vendor ?producer" +
			"?normnumrevprod "+
			"?avgscore1"+
			"((10000-xsd:float(xsd:string(?price)))/10000 AS ?normprice)" +
			"((?avgscore1 + ?normnumrevprod + ?normprice) AS ?score)"+
			"{" +
				"{?offer bsbm:vendor ?vendor ."+
				"?offer bsbm:product ?product ." +
				"?offer bsbm:price ?price }." +
				"{?product bsbm:producer  <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/dataFromProducer13/Producer13> ." +
				" <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/dataFromProducer13/Producer13> bsbm:normnumreviews ?normnumrevprod ."+
				"?product bsbm:avgscore1 ?avgscore1 .}" +


				"} ORDER BY DESC(?score) LIMIT " + 10;
		
				
		 String targetString2 = "prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> " +
		    "prefix xsd: <http://www.w3.org/2001/XMLSchema#>"+
		    "select ?offer ?vendor ?producer" +
			"?normnumrevprod "+
			"?avgscore1"+
			"((10000-xsd:float(xsd:string(?price)))/10000 AS ?normprice)" +
			"(((?avgscore1/2) + (?normnumrevprod+1) + ?normprice) AS ?score)"+
			"{" +
				"{?offer bsbm:vendor ?vendor ."+
				"?offer bsbm:product ?product ." +
				"?offer bsbm:price ?price }." +
				"{?product bsbm:producer  <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/dataFromProducer13/Producer13> ." +
				" <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/dataFromProducer13/Producer13> bsbm:normnumreviews ?normnumrevprod ."+
				"?product bsbm:avgscore1 ?avgscore1 .}" +


				"} ORDER BY DESC(?score) LIMIT " + 10;
		 
		 String targetString3 = "prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> " +
		    "prefix xsd: <http://www.w3.org/2001/XMLSchema#>"+
		    "select ?offer ?vendor ?producer" +
			"?normnumrevprod "+
			"?avgscore1"+
			"((10000-xsd:float(xsd:string(?price)))/10000 AS ?normprice)" +
			"((?avgscore1 + ?normnumrevprod + ?normprice) AS ?score)"+
			"{" +
				"{?offer bsbm:vendor ?vendor} ."+
				"{?offer bsbm:product ?product} ." +
				"{?offer bsbm:price ?price }." +
				"{?product bsbm:producer  <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/dataFromProducer13/Producer13> }." +
				"{ <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/dataFromProducer13/Producer13> bsbm:normnumreviews ?normnumrevprod }."+
				"{?product bsbm:avgscore1 ?avgscore1 .}" +


				"} ORDER BY DESC(?score) LIMIT " + 10;
		 
		 String targetString4 = "prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> " +
		    "prefix xsd: <http://www.w3.org/2001/XMLSchema#>"+
		    "select ?offer ?vendor ?producer" +
			"?normnumrevprod "+
			"?avgscore1"+
			"((10000-xsd:float(xsd:string(?price)))/10000 AS ?normprice)" +
			"((?normprice + ?avgscore1) AS ?oldscore2)" +
			"((?normprice + ?avgscore1) AS ?oldscore)" +
			"((?avgscore1 + ?normnumrevprod + ?normprice) AS ?score)"+
			"{" +
				"{?offer bsbm:vendor ?vendor} ."+
				"{?product bsbm:avgscore1 ?avgscore1 .}" +
				"{?offer bsbm:product ?product} ." +
				"{?offer bsbm:price ?price }." +
				"{?product bsbm:producer  <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/dataFromProducer13/Producer13> }." +
				"{ <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/dataFromProducer13/Producer13> bsbm:normnumreviews ?normnumrevprod }."+
	


				"} ORDER BY DESC(?score) LIMIT " + 10;
		 
		 String targetString5 = "prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> " +
		    "prefix xsd: <http://www.w3.org/2001/XMLSchema#>"+
		    "select ?offer ?vendor ?producer" +
			"?normnumrevprod "+
			"?avgscore1"+
			"((10000-xsd:float(xsd:string(?price)))/10000 AS ?normprice)" +
			"{" +
				"{?offer bsbm:vendor ?vendor} ."+
				"{?offer bsbm:product ?product} ." +
				"{?offer bsbm:price ?price }." +
				"{?product bsbm:producer  <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/dataFromProducer13/Producer13> }." +
				"{ <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/dataFromProducer13/Producer13> bsbm:normnumreviews ?normnumrevprod }."+
				"{?product bsbm:avgscore1 ?avgscore1 .}" +


				"} ORDER BY DESC(?avgscore1 + ?normnumrevprod + ?normprice) LIMIT " + 10;
		 
		Query targetQuery0 = QueryFactory.create(targetString0, Syntax.syntaxSPARQL_11) ; 
		Query targetQuery = QueryFactory.create(targetString, Syntax.syntaxSPARQL_11) ;
		Query targetQuery2 = QueryFactory.create(targetString2, Syntax.syntaxSPARQL_11) ;
		Query targetQuery3 = QueryFactory.create(targetString3, Syntax.syntaxSPARQL_11) ;
		Query targetQuery4 = QueryFactory.create(targetString4, Syntax.syntaxSPARQL_11) ;
		Query targetQuery5 = QueryFactory.create(targetString5, Syntax.syntaxSPARQL_11) ;
	     	
		optree.add(0,Algebra.compile(targetQuery0) );
		optree.add(1, Algebra.compile(targetQuery) ) ;
		optree.add(2,Algebra.compile(targetQuery2) ) ;
		optree.add(3,Algebra.compile(targetQuery3) ) ;
		optree.add(4,Algebra.compile(targetQuery4) );
		optree.add(5,Algebra.compile(targetQuery5) );
		
		f = new TreeChecker();
		
		//ExecutionModel.createExecutionModel();

	}

	@After
	public void tearDown() throws Exception {
		optree = null;

	}
	
	protected Op printAndCalculate(Op tempOp, int i, Transform t){
		System.out.println("Transformation: " + t.getClass());
		System.out.println("BEFORE " + i);
		System.out.println(tempOp);
		optree.remove(i);
		optree.add(i, Transformer.transform(t, tempOp));
		tempOp = optree.get(i);
		System.out.println("AFTER " + i);
		System.out.println(tempOp);
		System.out.println("");
		return tempOp;
	}

}
