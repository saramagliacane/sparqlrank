package sparqlrank;

import java.io.BufferedWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIter;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterAssign;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterPlainWrapper;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterProject;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterSlice;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterSort;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterTopN;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIteratorBase;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIteratorWrapper;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.tdb.solver.QueryIterTDB;

import sparqlrank.baseclasses.Predicate;
import sparqlrank.baseclasses.PredicateSet;
import sparqlrank.baseclasses.Score;
import sparqlrank.baseclasses.ScoreFunction;
import sparqlrank.iterators.QueryIterRank;
import sparqlrank.iterators.QueryIterRankJoin;
import sparqlrank.iterators.QueryIterRankJoinRandom;
import sparqlrank.iterators.QueryRankSequence;

/**
* Class that enables the counting of the number of passed bindings at run-time
* Has a flag to be switched on and off dynamically (TestParameters.reporting) 
* In addition there is a final Boolean RUN that should switch it off.
* @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
*/

public aspect Reporting {
	
	private Map<QueryIterator, Integer> countCalls = new HashMap<QueryIterator, Integer>();
	private Map<QueryIterTDB, Binding> qIterTDBvars = new HashMap<QueryIterTDB,Binding>();
	private Map<QueryIterator, Long> timeForEachIterator = new HashMap<QueryIterator, Long>();
	private Map<QueryIterator, Long> startTimeForEachIterator = new HashMap<QueryIterator, Long>();
	
	private Map<QueryIterator, Integer> countPerfectScores = new HashMap<QueryIterator, Integer>();
	
	private Map<QueryIterator, HashMap<Double, Integer>> countSameScores = new HashMap<QueryIterator, HashMap <Double, Integer>>();
	
	
	static boolean RUN = false;
	final static boolean COUNTSCORES = false;
	final static boolean PRINT_STATS = true;
	final static boolean PRINT_PLAIN_WRAPPERS = false;
	
	
   pointcut enableReporting(TestParameters param) : 
      execution( void TimingDriver.setupTimingExperiments( TestParameters )) && args (param);

 
    before(TestParameters param) : enableReporting(param){
       if (param.getReporting()){
          RUN = true;          
       }
       else RUN = false;
    }

	
	
	
	pointcut tracedCalls(QueryIteratorBase qIter): if(RUN) && target(qIter) && 
		call(Binding QueryIteratorBase.moveToNextBinding());
	

	pointcut endOfMeasure(TestParameters param, Model model, String queryString, Boolean enable): if(RUN)  && 
		execution(Op RankExecutionUtils.executeQuery(Model, String , TestParameters, Boolean))&& args(model, queryString, param, enable) ;
	
//	pointcut printMeasure(TestParameters param): if(RUN)  && 
//	execution(BufferedWriter TimingDriver.writeTreeStatsToFile(TestParameters))&& 
//	args(param) ;


	after(QueryIteratorBase qIter) returning (Binding binding): tracedCalls(qIter){
		count(qIter);
		if(qIter instanceof QueryIterTDB){
			if (!qIterTDBvars.containsKey(qIter)){
				qIterTDBvars.put((QueryIterTDB) qIter, binding);
			}
		}

	}
	

	
after(TestParameters param, Model model, String queryString, Boolean enable): endOfMeasure(param, model, queryString, enable){

   if(param.getReporting()){
	if((!param.getModifiedExecution() &&  param.getCountStandard()== 0)||(param.getModifiedExecution() && param.getCountModified() == 1)){


		
			BufferedWriter bw = TimingDriver.createTreeStatsWriter(param);
			
         BufferedWriter bwdistrib = null;
         if(param.getModifiedExecution())
            bwdistrib = TimingDriver.createDistributionWriter(param);
			
			// start by registering the execution time
			try {
				bw.append("\n"+"Time: "+RankExecutionUtils.getExecutionTime() + "\n" );
			} catch (IOException e1) {	e1.printStackTrace();	}
			
			// print all the values of the iterators
			for (QueryIterator qIter: countCalls.keySet()){
				if(qIter instanceof QueryIterTDB){
					Binding b = qIterTDBvars.get(qIter);
					String varnames = "";
					Iterator<Var> iter =  b.vars();
					while (iter.hasNext() ){
						varnames = varnames + iter.next().getName()+" ";
					}

					try {bw.append(qIter + " (" + varnames + ") " + countCalls.get(qIter) +"\n");
					} catch (IOException e) {e.printStackTrace();}
					
				}
				else if ((qIter instanceof QueryIterRank)||
						 (qIter instanceof QueryIterRankJoin)|| 
				       (qIter instanceof QueryIterRankJoinRandom)||
	                 (qIter instanceof QueryRankSequence)||
						 (qIter instanceof QueryIterSlice)||
						 (qIter instanceof QueryIterTopN)||
						 (qIter instanceof QueryIterSort)||
						 (qIter instanceof QueryIterProject)||
						 (qIter instanceof QueryIterAssign)){
					//System.out.println(qIter + " "+ countCalls.get(qIter));
					try{ bw.append(qIter + " "+ countCalls.get(qIter)+"\n");
					} catch (IOException e) {e.printStackTrace();}
					
					
					if (qIter instanceof QueryIterRank){
						QueryIterRank qRank = (QueryIterRank) qIter;
						try{ bw.append("  Ranked queue size: "+ qRank.getRankedQueueSize()+"\n");
						} catch (IOException e) {e.printStackTrace();}
						
					}
					
					
				}
				else continue;
				
				if(PRINT_STATS){
					if(countPerfectScores.containsKey(qIter))
						try{ bwdistrib.append("  Perfect Scores: " + countPerfectScores.get(qIter)+"\n");
						} catch (IOException e) {e.printStackTrace();}
						
					
					if(countSameScores.containsKey(qIter))
						try{ bwdistrib.append(qIter +" \n Same Scores: " + " \n \t"+ countSameScores.get(qIter)+"\n");
						} catch (IOException e) {e.printStackTrace();}
				}	
					
					
			}//for
			
			if(PRINT_STATS){
				for (QueryIterator qIter: timeForEachIterator.keySet()){
					System.out.println(qIter + " "+ timeForEachIterator.get(qIter) + "ms");
				}
			}
			
	
			
			
		
			try{
			   bw.close();	
			   if(param.getModifiedExecution())
			      bwdistrib.close();
			   } catch (IOException e) {e.printStackTrace();}
			


		
	}// count ==0
	
	if(PRINT_STATS){	
		timeForEachIterator.clear();
		startTimeForEachIterator.clear();
	}

	countCalls.clear();
	qIterTDBvars.clear();
	
   }// reporting == true
}
	
	
	private void count(QueryIterator qIter){
	
		if (!countCalls.containsKey(qIter)) countCalls.put(qIter, 1);
		else{
			Integer value = (Integer) countCalls.get(qIter); 
			countCalls.put(qIter, ++value);
		}
	}
	
	
	
	
//	// following the creation of PlainWrappers
//	pointcut tracedQueryIterPlainWrapper(): if(RUN) && if(PRINT_PLAIN_WRAPPERS) &&
//		call(  QueryIterPlainWrapper.new(..));
//	
//	
	// used only for measuring the time
	pointcut queryIterRankCalls(QueryIterRank qIter2): if(RUN) &&  if(COUNTSCORES) && target(qIter2) && 
		execution(Binding QueryIterRank.moveToNextBinding());
//	
//	pointcut queryIterRankJoinCalls(QueryIterator qIter2): if(RUN) && if (PRINT_TIME) && target(qIter2) && 
//		execution(Binding QueryIterRankJoin.moveToNextBinding());
//
//	
//	
//	before() : tracedQueryIterPlainWrapper(){
//		//System.out.println("creation ");
//	}
//	
//	
//	before (QueryIterator qIter): queryIterRankCalls( qIter){
//		startTimeForEachIterator.put(qIter, System.currentTimeMillis());
//	}
//	
//	after (QueryIterator qIter): queryIterRankCalls( qIter){
//		long oldtime = 0;
//		if (timeForEachIterator.containsKey(qIter))
//			oldtime = timeForEachIterator.get(qIter);
//		timeForEachIterator.put(qIter, oldtime + (System.currentTimeMillis() - startTimeForEachIterator.get(qIter)));
//	}
	
	after (QueryIterRank qIter) returning (Binding binding): queryIterRankCalls( qIter){

		ScoreFunction s = qIter.getScoreFunction();
		

		Score currentScore = s.evaluate(binding, qIter.getFullPredicateSet(), qIter.getRestset2(), null);
//		//complete score
//		if ( currentScore.getDouble()== s.getPredicates().size()){
//			//System.out.println (binding);
//			int score = 0;
//			if(countPerfectScores.containsKey(qIter))
//				score = countPerfectScores.get(qIter);
//			countPerfectScores.put(qIter, ++score);
//		}
		
		
		
		HashMap <Double, Integer> tempmap;
		
		if(!countSameScores.containsKey(qIter)){

		   tempmap = new LinkedHashMap <Double, Integer> ();
			 
		}
		else{
		   tempmap = countSameScores.get(qIter);
		}
		
		if (!tempmap.containsKey(currentScore.getDouble())){
		   tempmap.put(currentScore.getDouble(), 1);
	   }
		else{
		   Integer temp = tempmap.get(currentScore.getDouble());
		   tempmap.put(currentScore.getDouble(), temp +1);
		}
		
		countSameScores.put(qIter, tempmap);
		
		
		
		
	}


}
