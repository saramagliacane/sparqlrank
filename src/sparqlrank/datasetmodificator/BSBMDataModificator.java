package sparqlrank.datasetmodificator;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFactory;
import com.hp.hpl.jena.query.ResultSetRewindable;
import com.hp.hpl.jena.query.Syntax;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.rdf.model.impl.PropertyImpl;
import com.hp.hpl.jena.tdb.TDBFactory;

import sparqlrank.TestParameters;

/**
 * Helper class for modifying the BSBM dataset in order to have normalized values and some additional parameters for ordering
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class BSBMDataModificator {

    
    public static void modifyModel(final Model model, final Boolean debugValues, final Boolean debugModel){
       
       //PHASE 0 - check if already performed
       
       final String checkQueryString = "prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> " +
              "SELECT  ?s WHERE {?s  bsbm:avgscore1 ?o}";
       
       final Query checkQuery = QueryFactory.create(checkQueryString, Syntax.syntaxSPARQL_11) ;
       final QueryExecution checkQueryExec = QueryExecutionFactory.create(checkQuery, model) ;
       final ResultSetRewindable rwResults = ResultSetFactory.makeRewindable(checkQueryExec.execSelect());
       
       if (rwResults.hasNext()){
          System.out.println("Dataset is already modifed.");
          return;
       }
       
        // PHASE 1 - add normalized average score of rating parameters for each product - xsd:decimal
       // rating i are optional, in case there isn't any rating leave unbound
       addAvgScore(model, debugValues, debugModel);
       
       // PHASE 1b - add count of rating parameters for each Product
      addNumRating(model, debugValues, debugModel);
      
      // PHASE 2 - add normalized number of reviews to each product - xsd:Integer
      // if there isn't any review count 0  
      addNumReviews(model, debugValues, debugModel);
   	
      // PHASE 3 - add normalized number of reviews per Producer
      addNumReviewsProd(model, debugValues, debugModel);
}
	
    private static void addAvgScore(final Model model, final Boolean debugValues, final Boolean debugModel){
    
    	// PHASE 1 - add normalized average score of each rating parameter for each product - xsd:decimal
        // ratingi are optional, in case there isn't any rating leave unbound
        
        String avgscoreString; 
        Query avgscoreQuery;
        QueryExecution avgscoreQexec;
        ResultSet results;
        int i;
        int counter;
        
        for (counter = 1; counter<=4; counter++){
        	
	        avgscoreString = "prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> " +
			"prefix rev: <http://purl.org/stuff/rev#>" +
			"prefix xsd: <http://www.w3.org/2001/XMLSchema#>"+
			"Select  ?product ((xsd:double(avg(?score))/10) AS ?avgscore) {" +
	        "?review bsbm:reviewFor ?product ." +
	        "?review bsbm:rating"+counter+" ?score}" +
	        "group by ?product";
	
	        avgscoreQuery = QueryFactory.create(avgscoreString, Syntax.syntaxSPARQL_11) ;
	        avgscoreQexec = QueryExecutionFactory.create(avgscoreQuery, model) ;
	    
	        try {
	          results = avgscoreQexec.execSelect() ;
	          
	          i = 0;
	                 
	          for ( ; results.hasNext() ; )
	          {
	              final QuerySolution soln = results.nextSolution() ;
	              
	              final Literal avgscore = soln.getLiteral("avgscore");
	              final double avgdouble = avgscore.getDouble();
	              final Resource product = soln.getResource("product");
	              
	   
	              //debugging print
	              if(debugValues)
	            	System.out.println((++i)+ " = "+ product.getLocalName() + " - " + avgdouble) ;	              
	
	              //add avgscore to the model
	              
	              final Statement s = model.createLiteralStatement(product,
	            		  new PropertyImpl("http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/avgscore"+counter), 
	            		  avgdouble);
	              model.add(s);
	          }
	          
	          //debug print to check if the avgscore property is in the model
	          
	          if(debugModel) printDebugModel(model, "avgscore");
	
		          
	        } finally { avgscoreQexec.close() ; }
	        
        }
    }
    
    
    private static void addNumReviews(final Model model, final Boolean debugValues, final Boolean debugModel){
    
    	// PHASE 2 - add number of reviews to each product - xsd:Integer
    	// if there isn't any review count 0
	    
	    final String numReviewsString = "prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> " +
		"prefix rev: <http://purl.org/stuff/rev#>" +
		"Select  ?product (count(?review) As ?numreviews) {" +
		"?offer bsbm:product ?product "+
        "OPTIONAL {?review bsbm:reviewFor ?product .}}"+
		 "group by ?product";

        final Query numReviewQuery = QueryFactory.create(numReviewsString, Syntax.syntaxSPARQL_11) ;
        final QueryExecution numReviewQexec = QueryExecutionFactory.create(numReviewQuery, model) ;
    
        try {
	     
         final ResultSet results = numReviewQexec.execSelect() ;
         
         int i = 0;
	     final int max = maxNumReviews(model, debugValues, debugModel);
         
         for ( ; results.hasNext() ; )
         {
              final QuerySolution soln = results.nextSolution() ;
              final Literal numreviews = soln.getLiteral("numreviews");
              final Resource product = soln.getResource("product");
              

              final double normnumrev = ((double) numreviews.getInt())/((double) max);

              
              //debugging print
              if(debugValues)
	              System.out.println((++i)+ " = "+ product.getLocalName() + " - " + normnumrev ) ;	              
             
             //add numrev to the model	              
             final Statement s1 = model.createLiteralStatement(product, 
            		 new PropertyImpl("http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/numreviews"),numreviews);
             model.add(s1);
             
             //add normnumrev to the model	
             final Statement s = model.createLiteralStatement(product, 
            		 new PropertyImpl("http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/normnumreviews"),
            		 normnumrev);
             model.add(s);

          }
          
          //debug print to check if the avgscore property is in the model
          
          if(debugModel) printDebugModel(model, "normnumreviews");
	          
        } finally { numReviewQexec.close() ; }
        
    }
    
    private static void addNumRating(final Model model, final Boolean debugValues, final Boolean debugModel){
	    
        // PHASE 1b - add count of rating parameters for each Product
    	
        String numRatingString; 
        Query numRatingQuery;
        QueryExecution numRatingQexec;
        ResultSet results;
        int i;
        int counter;
        
        for (counter = 1; counter<=4; counter++){
        	
	        numRatingString = "prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> " +
			"prefix rev: <http://purl.org/stuff/rev#>" +
			"prefix xsd: <http://www.w3.org/2001/XMLSchema#>"+
			"Select  ?product (count(?score) AS ?numrating) {" +
	        "?review bsbm:reviewFor ?product ." +
	        "?review bsbm:rating"+counter+" ?score}" +
	        "group by ?product";
	
	        numRatingQuery = QueryFactory.create( numRatingString , Syntax.syntaxSPARQL_11) ;
	        numRatingQexec = QueryExecutionFactory.create(numRatingQuery, model) ;
        	
	        try {
	     
	        	results = numRatingQexec.execSelect() ;
	        	i = 0;
	                 
	        	for ( ; results.hasNext() ; )
	        	{
	        		final QuerySolution soln = results.nextSolution() ;
	        		final Literal numrating = soln.getLiteral("numrating");
	        		final Resource product = soln.getResource("product");


	              //debugging print
	              if(debugValues)
		              System.out.println((++i)+ " = "+ product.getLocalName() + " - " + numrating ) ;	              
	             
	             //add numrev to the model	              
	             final Statement s1 = model.createLiteralStatement(product, 
	            		 new PropertyImpl("http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/numrating"+counter),numrating);
	             model.add(s1);

	        	}
          
	          //debug print to check if the property is in the model
	          
	          if(debugModel) printDebugModel(model, "numrating"+counter);
	          
	        } finally { numRatingQexec.close() ; }
        }
        
    }

    
    private static void addNumReviewsProd(final Model model, final Boolean debugValues, final Boolean debugModel){
	 	// PHASE 3 - add number of reviews per Producer
        
	    final String numReviewsProdString = "prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> " +
    		"prefix rev: <http://purl.org/stuff/rev#>" +
    		"Select ?producer (count(?review) As ?numreviews) {" +
    		"?product bsbm:producer ?producer ."+
	        "?review bsbm:reviewFor ?product }"+
    		 "group by ?producer";

	    final Query numReviewProdQuery = QueryFactory.create(numReviewsProdString, Syntax.syntaxSPARQL_11) ;
	    final QueryExecution numReviewProdQexec = QueryExecutionFactory.create(numReviewProdQuery, model) ;
        
	    try {
	    	
	        final ResultSet results = numReviewProdQexec.execSelect() ;
	        
	        int i = 0;
	        final int max = maxNumReviewsProd(model, debugValues, debugModel);
	        
	        for ( ; results.hasNext() ; )
	        {
	            final QuerySolution soln = results.nextSolution() ;
	            final Literal numreviews = soln.getLiteral("numreviews");
	            final Resource producer = soln.getResource("producer");
	            
	            final double normnumrev = ((double) numreviews.getInt())/((double) max);
	            
	            //Node node = Node.createLiteral(Double.toString(normnumrev), null, XSDDatatype.XSDdouble);
	              
	            
	            //debugging print
	            if(debugValues)
		            System.out.println((++i)+ " = "+ producer.getLocalName() + " - " + numreviews.getInt()  +" "+ normnumrev) ;	              

	             
	            //add numrev to the model	              
	             final Statement s1 = model.createLiteralStatement(producer, 
	            		 new PropertyImpl("http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/numreviews"),numreviews);
	             model.add(s1);
	             
		         //add normnumrev to the model	              
	             final Statement s = model.createLiteralStatement(producer, 
	            		 new PropertyImpl("http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/normnumreviews"),
	            		 normnumrev);
	             model.add(s);
	          }
	          
	          //debug print to check if the avgscore property is in the model
	          
	          if(debugModel) printDebugModel(model, "normnumreviews");
		          
	        } finally { numReviewProdQexec.close() ; }
	 
    }
    
    
    private static int maxNumReviews(final Model model, final Boolean debugValues, final Boolean debugModel){
	    
    	//find maximum of number of reviews
	    
	    final String maxReviewsString = "prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> " +
		"prefix rev: <http://purl.org/stuff/rev#>" +
		"Select (max(?numreviews) AS ?max){" +
		"{Select  ?product (count(?review) As ?numreviews) {" +
		"?offer bsbm:product ?product ."+
        "?review bsbm:reviewFor ?product }"+
		 "group by ?product}}";

        final Query maxReviewQuery = QueryFactory.create(maxReviewsString, Syntax.syntaxSPARQL_11) ;
        final QueryExecution maxReviewQexec = QueryExecutionFactory.create(maxReviewQuery, model) ;
    
        int max = 1; //lowest integer that avoids division by zero
        
        try {
	     
         final ResultSet results = maxReviewQexec.execSelect() ;
    
         final QuerySolution soln = results.nextSolution() ;
         final Literal numreviews = soln.getLiteral("max");
              
         max = numreviews.getInt();
              
         //debugging print
         if(debugValues)
	        System.out.println(max) ;	              

        } finally { maxReviewQexec.close() ; }
        
        return max;
    }
    
    
    private static int maxNumReviewsProd(final Model model, final Boolean debugValues, final Boolean debugModel){
	    
    	//find maximum of number of reviews for producer
	    
	    final String maxReviewsString = "prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> " +
		"prefix rev: <http://purl.org/stuff/rev#>" +
		"Select (max(?numreviews) AS ?max){" +
		"{Select ?producer (count(?review) As ?numreviews) {" +
		"?product bsbm:producer ?producer ."+
        "?review bsbm:reviewFor ?product }"+
		 "group by ?producer}}";
	    
	    final Query maxReviewQuery = QueryFactory.create(maxReviewsString, Syntax.syntaxSPARQL_11) ;
        final QueryExecution maxReviewQexec = QueryExecutionFactory.create(maxReviewQuery, model) ;
    
        int max = 1; //lowest integer that avoids division by zero
        
        try {
	     
         final ResultSet results = maxReviewQexec.execSelect() ;
    
         final QuerySolution soln = results.nextSolution() ;
         final Literal numreviews = soln.getLiteral("max");
              
         max = numreviews.getInt();
              
         //debugging print
         if(debugValues)
	        System.out.println(max) ;	              

        } finally { maxReviewQexec.close() ; }
        
        return max;
    }
    

    
    private static void printDebugModel(final Model model, final String value){
    	final StmtIterator stmtit = model.listStatements();
    	int i = 0;
    	for ( ; stmtit.hasNext() ; )
    	{
    		final Statement s = stmtit.nextStatement();
    		if (s.getPredicate().getLocalName().contains(value)) 
    			System.out.println((++i)+" "+ s);
    	}
    }
    
    
   	
    
    //TODO: shouldn't be here and the hardcoded "dataset" string is not a good idea
    
    public static Model createModel(final TestParameters param){
	    	
       final String assemblerFile = param.getDatasetDirectory() +"/dataset"+ param.getDataset()+".ttl" ;
       if(! new File(assemblerFile).exists()){
          
          // write the assembler file so it can it can work also with Joseki
          final String baseAssemblerString = "@prefix tdb:     <http://jena.hpl.hp.com/2008/tdb#> . \n@prefix rdfs:   <http://www.w3.org/2000/01/rdf-schema#> . \n@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> . \n@prefix ja:      <http://jena.hpl.hp.com/2005/11/Assembler#> . \n\n"+
                "[] ja:loadClass \"com.hp.hpl.jena.tdb.TDB\" . \n" +
                "<#dataset> rdf:type         ja:RDFDataset ; \n \t ja:defaultGraph <#graph> ; \n \t. \n\n" + 
                "<#graph> rdf:type tdb:GraphTDB ; \n \t tdb:location \"Store/dataset" + param.getDataset()+"\" ;\n \t. \n \n ";
          
          
          try{
   
             final BufferedWriter bw = new BufferedWriter(new FileWriter(assemblerFile, true)); 
             bw.write(baseAssemblerString);
             bw.close();
          } catch(final IOException e) { System.err.println("Could not create assemblers."); System.exit(-1);}
       }

       final Model model = TDBFactory.assembleModel(assemblerFile) ;
          
        //final Model model = TDBFactory.createModel("Store/dataset"+param.getDataset());
        System.out.println(model.size() + " triples loaded.");
        
        if(param.getCreateValues())
        	BSBMDataModificator.modifyModel(model, param.getDebugValues(), param.getDebugModel());	        
        
        return model;

   	}
    
    public static void modifyPossibleDatasets(final TestParameters param){
       param.setCreateValues(true);
       for (final String s: param.possibleDatasets){
          param.setDataset(s);
          final Model model = createModel(param);
          model.close();
       }
       param.setCreateValues(false);
    }
    
    public static void main(final String... args)
    {
    	final TestParameters param = new TestParameters(args);
    	param.setDataset("100K");
    	param.setCreateValues(true);
    	final Model model = createModel(param);
    	model.close();
    } 

   }
