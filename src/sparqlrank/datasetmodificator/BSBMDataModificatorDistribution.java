package sparqlrank.datasetmodificator;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSetFactory;
import com.hp.hpl.jena.query.ResultSetRewindable;
import com.hp.hpl.jena.query.Syntax;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.rdf.model.impl.PropertyImpl;
import com.hp.hpl.jena.tdb.TDBFactory;

import org.apache.commons.math.MathException;
import org.apache.commons.math.distribution.ExponentialDistributionImpl;
import org.apache.commons.math.distribution.NormalDistributionImpl;
import org.apache.commons.math.distribution.PoissonDistributionImpl;
import org.apache.commons.math.distribution.ZipfDistributionImpl;

import sparqlrank.TestParameters;

/**
 * Helper class for modifying the BSBM dataset in order to have synthetic attributes with a given distribution
 * @author Marco Balduini
 * @author Sara Magliacane &lt;sara.magliacane@mail.polimi.it&gt;
 */

public class BSBMDataModificatorDistribution {


	private static final boolean CUT_AT_DECIMAL = true;
	private static final int DECIMAL = 1;

   public static void modifyModel(final Model model, final Boolean debugValues, final Boolean debugModel, final String functionType, String distributionParameters, final String object, final String propertyName){

      
      // first check if not already present
      
      
      final String checkQueryString = "prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> " +
            "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>" +
            "prefix xsd: <http://www.w3.org/2001/XMLSchema#>"+
            "SELECT  ?" + object + " " +
            "WHERE {" +
            "?"+ object +" bsbm:" + propertyName +" ?o" +
            "}";
      
      final Query checkQuery = QueryFactory.create(checkQueryString, Syntax.syntaxSPARQL_11) ;
      final QueryExecution checkQueryExec = QueryExecutionFactory.create(checkQuery, model) ;
      final ResultSetRewindable rwResults = ResultSetFactory.makeRewindable(checkQueryExec.execSelect());
      
      if (rwResults.hasNext()){
         System.out.println("Didn't add property:" + propertyName +" because it is already in the dataset.");
         return;
      }
      
      
	   double mean = 1;
	   double  stddev = 0 ;
		
		if(functionType.equals("norm")){

			mean = Double.parseDouble(distributionParameters.substring(0, distributionParameters.indexOf("_")));
			distributionParameters = distributionParameters.substring(distributionParameters.indexOf("_") + 1, distributionParameters.length());
			stddev = Double.parseDouble(distributionParameters);
			
			//		add normalized value with Normal distribution for each product - xsd:double
			
		} else if(functionType.equals("pois")){

			 mean = Double.parseDouble(distributionParameters);
			
			//		add normalized value with Poisson distribution for each product - xsd:double
		} else if(functionType.equals("zipf")){

			mean = Double.parseDouble(distributionParameters);
			
		} else if(functionType.equals("exp")){

			mean = Double.parseDouble(distributionParameters.substring(0, distributionParameters.indexOf("_")));
			
			//		add normalized value with Exponential distribution for each product - xsd:double
			
		}

		addDistributionValue(model, debugValues, debugModel, functionType, mean, stddev, object, propertyName);
		
	}

	private static double[] returnNormalArrayOFValue(final int number, final double mean, final double stddev){

		final NormalDistributionImpl nd = new NormalDistributionImpl(mean, stddev);

		final double[] dList = new double[number];

		for(int i = 0 ; i < number ; i++){

			try {
				dList[i] = nd.sample();
			} catch (final MathException e) {
				e.printStackTrace();
			}

		}

		return dList;

	}

	private static double[] returnPoissonlArrayOFValue(final int number, final double mean){

		final PoissonDistributionImpl pd = new PoissonDistributionImpl(mean);

		final double[] dList = new double[number];

		for(int i = 0 ; i < number ; i++){

			try {
				dList[i] = pd.sample();
			} catch (final MathException e) {
				e.printStackTrace();
			}

		}

		return dList;

	}

	private static double[] returnZipfArrayOFValue(final int number, final double exp){

		final ZipfDistributionImpl zd = new ZipfDistributionImpl(number, exp);

		final double[] dList = new double[number];

		for(int i = 0 ; i < number ; i++){

			try {
				dList[i] = zd.sample();
			} catch (final MathException e) {
				e.printStackTrace();
			}

		}

		return dList;

	}

	private static double[] returnExponentialArrayOFValue(final int number, final double mean){

		final ExponentialDistributionImpl ed = new ExponentialDistributionImpl(mean);

		final double[] dList = new double[number];

		for(int i = 0 ; i < number ; i++){

			try {
				dList[i] = ed.sample();
			} catch (final MathException e) {
				e.printStackTrace();
			}

		}

		return dList;

	}

	private static double returnMax(final double[] dlist){

		double max = 0;

		for(int i = 0 ; i < dlist.length ; i++){
			if(dlist[i] > max){
				max = dlist[i];
			}
		}

		return max;

	}
	
	  private static double returnMin(final double[] dlist){

	      double min = 1000;

	      for(int i = 0 ; i < dlist.length ; i++){
	         if(dlist[i] < min){
	            min = dlist[i];
	         }
	      }

	      return min;

	   }

	private static void addDistributionValue(final Model model, final Boolean debugValues, final Boolean debugModel, final String functionType, final double mean, final double stddev, final String object, final String propertyName){

	   
	   final QueryExecution subjectQexec = getQuery(object, model);
      
		try {

		   final ResultSetRewindable rwResults = ResultSetFactory.makeRewindable(subjectQexec.execSelect());

			double [] dList = null; 
			      
			 
			 if(functionType.equals("norm")){

			    dList = returnNormalArrayOFValue(rwResults.size(), mean, stddev);
			         
	        } else if(functionType.equals("pois")){

	           dList =returnPoissonlArrayOFValue(rwResults.size(), mean);    
			  
			   } else if(functionType.equals("zipf")){

			      dList = returnZipfArrayOFValue(rwResults.size(), mean);
			      
			  } else if(functionType.equals("exp")){

			      dList = returnExponentialArrayOFValue(rwResults.size(), mean);
			         
			   }
			      
			      
			      
			      
			final double max = returnMax(dList);
	

			int i = 0;

			for ( ; rwResults.hasNext() ; )
			{
				final QuerySolution soln = rwResults.nextSolution() ;
				final Resource review = soln.getResource("?" + object);

				Double value = dList[i] / max;

				if(value < 0){
					value = 0.0;
				}
				
				
				if(CUT_AT_DECIMAL){
				   
				   final double power = Math.pow(10, DECIMAL);
				   value = value * power;
		         final int temp = value.intValue();
		         value = temp/ power;
				}



				//debugging print
				if(debugValues)
					System.out.println(review.getLocalName() + " - " + value) ;	              

				final Statement s = model.createLiteralStatement(review,
						new PropertyImpl("http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/" + propertyName), 
						value);
				model.add(s);

				i++;
			}

			//debug print to check if the normalDistributionValue property is in the model

			if(debugModel) printDebugModel(model, propertyName);


		} finally {
			subjectQexec.close() ; 
		}

	}

	

	

	
	
private static QueryExecution getQuery(final String object, final Model model){

	     String subjectString = null; 
	   
      if(object.equals("product")){

         subjectString = "prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> " +
            "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>" +
            "prefix xsd: <http://www.w3.org/2001/XMLSchema#>"+
            "SELECT  DISTINCT ?" + object + " " +
            "WHERE {" +
            "?"+ object +" rdf:type bsbm:Product " +
            "}";
      } else if(object.equals("review")){

         subjectString = "prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> " +
         "prefix rev: <http://purl.org/stuff/rev#>" +
         "prefix xsd: <http://www.w3.org/2001/XMLSchema#>"+
         "SELECT  DISTINCT ?" + object + " " +
         "WHERE {" +
         "?" + object + " bsbm:reviewFor ?product . " +
         "}";
      } else if(object.equals("producer")){

         subjectString = "prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> " +
               "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>" +
               "prefix xsd: <http://www.w3.org/2001/XMLSchema#>"+
               "SELECT  DISTINCT ?" + object + " " +
               "WHERE {" +
               "?"+ object +" rdf:type bsbm:Producer " +
               "}";
      } else if(object.equals("offer")){

         subjectString = "prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> " +
               "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>" +
               "prefix xsd: <http://www.w3.org/2001/XMLSchema#>"+
               "SELECT  DISTINCT ?" + object + " " +
               "WHERE {" +
               "?"+ object +" rdf:type bsbm:Offer " +
               "}";
      }
      
      final Query subject = QueryFactory.create(subjectString, Syntax.syntaxSPARQL_11) ;
      final QueryExecution subjectQexec = QueryExecutionFactory.create(subject, model) ;

      
      return subjectQexec;
	}

	private static void printDebugModel(final Model model, final String value){
		final StmtIterator stmtit = model.listStatements();
		int i = 0;
		for ( ; stmtit.hasNext() ; )
		{
			final Statement s = stmtit.nextStatement();
			if (s.getPredicate().getLocalName().contains(value)) 
				System.out.println((++i)+" "+ s);
		}
	}

	
	public static void createModel(final TestParameters param, final String type, final String distributionParameters, final String object, final String propertyName){


      final String assemblerFile = param.getDatasetDirectory() +"/dataset"+ param.getDataset()+".ttl" ;
    
		final Model model = TDBFactory.assembleModel(assemblerFile) ;
		System.out.println(model.size() + " triples loaded.");
		

		

	   modifyModel(model, false, false, type, distributionParameters, object, propertyName);     
		
		model.close();

	}

	public static void main(final String... args)
	{
		final TestParameters param = new TestParameters(args);
		param.setCreateValues(true);

		param.setDataset("5M");
		//distributionParameters = [distribution name]_[parameter 1]_....._[parameter n]_[property name]
		//distribution name = norm (parameter = mean, stddev), pois (parameter = mean), zipf (parameter = exponent), exp (parameter = mean)
	
		//object = product, review, producer, offer
	
		// product and offer     have normalDistr_0.5_0.16
		// 1M
		

		addDistributionValues(param);
		
	}
		
	public static void addDistributionToPossibleDatasets(final TestParameters param){   	
      param.setCreateValues(true);
      for (final String s: param.possibleDatasets){
         param.setDataset(s);
         addDistributionValues(param);
      }
      param.setCreateValues(false);
	}
	
public static void addDistributionValues(final TestParameters param){		
		
      final String object = "product";
   
   
		String type;
	   String distributionParameters;
	   String propertyName;
		

		type = "norm";
		distributionParameters = "0.5_0.16";
		

		propertyName = "normalDistr_0.5_0.16_1decimal_1";
	   createModel(param,  type, distributionParameters, object, propertyName);         

		propertyName = "normalDistr_0.5_0.16_1decimal_2";
		createModel(param,  type, distributionParameters, object, propertyName);             
		



		type = "pois";
		distributionParameters = "3.5";
	   propertyName = "poissonDistr_3.5_1decimal_1";
	   createModel(param,  type, distributionParameters, object, propertyName);         

	   propertyName = "poissonDistr_3.5_1decimal_2";
	   createModel(param,  type, distributionParameters, object, propertyName);          


      
	     
	   type = "zipf";
	   distributionParameters = "1.5";
	   propertyName = "zipfDistr_1.5_1decimal_1";
	   createModel(param,  type, distributionParameters, object, propertyName);           

	   propertyName = "zipfDistr_1.5_1decimal_2";
      createModel(param,  type, distributionParameters, object, propertyName);         
	

	} 
	
	

}
