prefix bsbm: <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/> 
prefix xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

select *
{


    { ?product bsbm:producer  ?producer . 
      ?producer bsbm:normnumreviews ?normnumrevprod . }	.

    {?product bsbm:avgscore1 ?avgscore1 }.

    {?offer bsbm:product ?product .
     ?offer bsbm:price ?price .}
    {?offer bsbm:vendor ?vendor.
     ?offer bsbm:validFrom ?from .
     ?offer bsbm:validTo ?to }

}
order by DESC(?avgscore1 + ?normnumrevprod + (10000-xsd:double(xsd:string(?price)))/10000)

